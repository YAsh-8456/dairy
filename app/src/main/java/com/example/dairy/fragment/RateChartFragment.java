package com.example.dairy.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.example.dairy.R;
import com.example.dairy.adapter.RateChatAdapter;
import com.example.dairy.databinding.FragmentBinding;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class RateChartFragment extends Fragment {


    ListView list;
    RateChatAdapter rateChatAdapter;
    FragmentBinding binding;

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private int SNFPosition;
    private List<Map<Integer, Object>> mParam2;

    public static RateChartFragment newInstance(int param1, List<Map<Integer, Object>> param2) {
        RateChartFragment fragment = new RateChartFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_PARAM1, param1);
        args.putSerializable(ARG_PARAM2, (Serializable) param2);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            SNFPosition = getArguments().getInt(ARG_PARAM1);
            mParam2 = (List<Map<Integer, Object>>) getArguments().getSerializable(ARG_PARAM2);
        }
    }

    public RateChartFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment, container, false);
        View view = binding.getRoot();

        LinearLayoutManager manager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        binding.rateChartView.setLayoutManager(manager);
        rateChatAdapter = new RateChatAdapter(getActivity(),SNFPosition);
        binding.rateChartView.setAdapter(rateChatAdapter);
        rateChatAdapter.addData(mParam2);


        return view;
    }
}

