package com.example.dairy.fragment;

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.example.dairy.R;
import com.example.dairy.adapter.MasterAdapter;
import com.example.dairy.databinding.LayoutSearchMemberBinding;
import com.example.dairy.modal.CommonResponse;
import com.example.dairy.modal.Result;
import com.example.dairy.utils.AlertMassage;
import com.example.dairy.utils.Constants;
import com.example.dairy.utils.Debug;
import com.example.dairy.utils.MemberClickInterface;
import com.example.dairy.utils.SelectMemberInterface;
import com.example.dairy.utils.Utils;
import com.example.dairy.webService.APIController;
import com.example.dairy.webService.ResponseInterface;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import org.json.JSONObject;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Response;


public class SearchMemberFragment extends BottomSheetDialogFragment implements MemberClickInterface, ResponseInterface {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String valueType;
    private String mParam2;

    private static final String TAG = "SetValueBottomDialogFragment";


    LayoutSearchMemberBinding binding;
    MasterAdapter masterAdapter;
    SelectMemberInterface selectMemberInterface;
    APIController apiController;


    public static SearchMemberFragment newInstance(String param1, String param2) {
        SearchMemberFragment fragment = new SearchMemberFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putSerializable(ARG_PARAM2,param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            valueType = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        binding = DataBindingUtil.inflate(inflater, R.layout.layout_search_member, container, false);
        View v = binding.getRoot();
        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        init();
        return v;
    }

    private void init() {

        apiController = new APIController(this);
        LinearLayoutManager manager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        binding.memberView.setLayoutManager(manager);
        masterAdapter = new MasterAdapter(getActivity(), this);
        masterAdapter.isSearchMember(true);
        binding.memberView.setAdapter(masterAdapter);

        Debug.e(TAG, "Global list =-->" + Utils.getMemberList());
        if(valueType.toUpperCase().equals("BUYER")){
            masterAdapter.addData(Utils.getBuyerMemberList());
        }else if(valueType.toUpperCase().equals("SELLER")){
            masterAdapter.addData(Utils.getSellerMemberList());
        }else {
            masterAdapter.addData(Utils.getMemberList());
        }


        binding.etMember.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                masterAdapter.filter(s.toString());
            }
            @Override
            public void afterTextChanged(Editable s) {
            }
        });

//        getMasterApiCall();

    }

    private void getMasterApiCall() {

        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("memberType", "");
            jsonObject.put("startMemberId", "");
            jsonObject.put("endMemberId", "");

            if (!Utils.isInternetConnected(getActivity())) {
                AlertMassage.MassageAlert(getActivity(), getResources().getString(R.string.message_intent_conection));
                return;
            }

            Utils.hideSoftKeyword(getActivity());
            RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), jsonObject.toString());
            Debug.e(TAG, "Member list request:- " + jsonObject.toString());

            apiController.getMasterAPI(Constants.API_TYPE.GET_MASTER,body);

        } catch (Exception e) {
            Debug.e(TAG, "Error" + e.getMessage());
        }
    }


    @Override
    public void successResponse(Constants.API_TYPE type, Object response, int code) {
//        binding.llProgressBar.setVisibility(View.GONE);
        switch (type) {
            case GET_MASTER:
                try {
                    if (code == Constants.RESULT_OK) {
                        CommonResponse getMasterResponse = ((Response<CommonResponse>) response).body();
                        if (getMasterResponse != null && getMasterResponse.getStatus()) {
                            masterAdapter.addData(getMasterResponse.getResult());
                        } else {
                            Utils.showSnackbar(getActivity(), binding.llMainLayout, getMasterResponse.getMessage());
                        }

                    } else {
                        Utils.showSnackbar(getActivity(), binding.llMainLayout, "API Error");

                    }
                } catch (Exception e) {
                    Debug.e(TAG, "Get Master List Exception" + e.getMessage());
                }
                break;

            }

    }

    @Override
    public void failureResponse(Constants.API_TYPE type, String error) {
//        binding.llProgressBar.setVisibility(View.GONE);
        Debug.e(TAG, "Fail Error =----->" + error);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            selectMemberInterface = (SelectMemberInterface) getActivity();
        } catch (ClassCastException e) {
            Debug.e(TAG, "onAttach: " + e.getMessage());
        }
    }


    @Override
    public void delete(Result result, int position) {

    }

    @Override
    public void view(Result result, int position) {
        selectMemberInterface.selectedMember(result);
        getDialog().dismiss();
    }

    @Override
    public void edit(Result result, int position) {

    }

    @Override
    public void print(Result result, int position) {
    }
}