package com.example.dairy.webService;


import com.example.dairy.utils.Constants;

public interface ResponseInterface {

    public void successResponse(Constants.API_TYPE type, Object response, int code);

    public void failureResponse(Constants.API_TYPE type, String error);
}
