package com.example.dairy.webService;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient {


//    http://217.21.78.217/
    public static final String BASE_URL =  "http://217.21.78.217/";
//    public static final String BASE_URL =  " http://45.80.152.224:8008";

    private static Retrofit retrofit = null;

    public static OkHttpClient client = new OkHttpClient.Builder().addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)).addInterceptor(new Interceptor() {
        @Override
        public okhttp3.Response intercept(Chain chain) throws IOException {
            Request original = chain.request();

            Request request = original.newBuilder()
//                    .header("Authorization", "3bb743bbd45d4eb8ae31e16b9f83c9")
//                    .header("Content-Type", "application/x-www-form-urlencoded")
//                    .header("X-Requested-With", "application/json")
                    .method(original.method(), original.body())
//                    .addHeader("Authorization", "3bb743bbd45d4eb8ae31e16b9f83c9")
                    .addHeader("Content-Type", "application/x-www-form-urlencoded")
                    .addHeader("X-Requested-With", "application/json")
                    .build();

            return chain.proceed(request);
        }
    }).callTimeout(20, TimeUnit.SECONDS).connectTimeout(20, TimeUnit.SECONDS) .writeTimeout(30, TimeUnit.SECONDS).readTimeout(30, TimeUnit.SECONDS).build();


    public static Retrofit getClient() {

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();
        }
        return retrofit;
    }

    public static Retrofit getClient(String url) {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(url)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
}
