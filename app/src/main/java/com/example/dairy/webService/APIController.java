package com.example.dairy.webService;

import androidx.annotation.NonNull;

import com.example.dairy.modal.CommonResponse;
import com.example.dairy.utils.Constants;
import com.example.dairy.utils.Debug;
import com.example.dairy.utils.PreferenceManager;

import java.util.HashMap;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;

public class APIController {

    private DairyApi mApi;
    private ResponseInterface mInterface;
    private String TAG = "APIController";

    public APIController(ResponseInterface responseInterface) {
        this.mInterface = responseInterface;
        mApi = ApiClient.getClient().create(DairyApi.class);
    }

    public void dairyLogin(final Constants.API_TYPE type, RequestBody body) {

        Call<CommonResponse> call = mApi.dairyLogin(body);
        call.enqueue(new Callback<CommonResponse>() {

            @Override
            public void onResponse(Call<CommonResponse> call, retrofit2.Response<CommonResponse> response) {
                Debug.e(TAG, "Login Response =---->" + response);
                mInterface.successResponse(type, response, response.code());
            }

            @Override
            public void onFailure(@NonNull Call<CommonResponse> call, @NonNull Throwable t) {
                Debug.e(TAG, "Login onFailure Response =---->" + t);
                mInterface.failureResponse(type, t.getMessage());
            }
        });
    }

    public void dairyRegister(final Constants.API_TYPE type, RequestBody body) {

        Call<CommonResponse> call = mApi.dairyRegister(body);
        call.enqueue(new Callback<CommonResponse>() {

            @Override
            public void onResponse(Call<CommonResponse> call, retrofit2.Response<CommonResponse> response) {
                mInterface.successResponse(type, response, response.code());
            }

            @Override
            public void onFailure(@NonNull Call<CommonResponse> call, @NonNull Throwable t) {
                mInterface.failureResponse(type, t.getMessage());
            }
        });
    }

    public void dairyEdit(final Constants.API_TYPE type, RequestBody body) {

        String dairyId = String.valueOf(PreferenceManager.getValueInt(Constants.DAIRY_ID));
        String token = PreferenceManager.getValue(Constants.DAIRY_TOKEN);
        String finalToken = "Bearer " + token;
        Call<CommonResponse> call = mApi.dairyEdit(finalToken, dairyId, body);
        call.enqueue(new Callback<CommonResponse>() {

            @Override
            public void onResponse(Call<CommonResponse> call, retrofit2.Response<CommonResponse> response) {
                mInterface.successResponse(type, response, response.code());
            }

            @Override
            public void onFailure(@NonNull Call<CommonResponse> call, @NonNull Throwable t) {
                mInterface.failureResponse(type, t.getMessage());
            }
        });
    }

    public void dairyRemove(final Constants.API_TYPE type) {

        String dairyId = String.valueOf(PreferenceManager.getValueInt(Constants.DAIRY_ID));
        String token = PreferenceManager.getValue(Constants.DAIRY_TOKEN);
        String finalToken = "Bearer " + token;
        Call<CommonResponse> call = mApi.dairyRemove(finalToken, dairyId);
        call.enqueue(new Callback<CommonResponse>() {

            @Override
            public void onResponse(Call<CommonResponse> call, retrofit2.Response<CommonResponse> response) {
                mInterface.successResponse(type, response, response.code());
            }

            @Override
            public void onFailure(@NonNull Call<CommonResponse> call, @NonNull Throwable t) {
                mInterface.failureResponse(type, t.getMessage());
            }
        });
    }


    //
    public void verifyApi(final Constants.API_TYPE type, String token, HashMap<String, String> verifyRequest) {

        String finalToken = "Bearer " + token;

        Call<CommonResponse> call = mApi.verifyApi(finalToken, verifyRequest);
        call.enqueue(new Callback<CommonResponse>() {

            @Override
            public void onResponse(Call<CommonResponse> call, retrofit2.Response<CommonResponse> response) {
                mInterface.successResponse(type, response, response.code());
            }

            @Override
            public void onFailure(@NonNull Call<CommonResponse> call, @NonNull Throwable t) {
                mInterface.failureResponse(type, t.getMessage());
            }
        });
    }


    public void addMasterAPI(final Constants.API_TYPE type, RequestBody body) {

        String token = PreferenceManager.getValue(Constants.DAIRY_TOKEN);
        String finalToken = "Bearer " + token;
        Call<CommonResponse> call = mApi.addMaster(finalToken, body);
        call.enqueue(new Callback<CommonResponse>() {

            @Override
            public void onResponse(Call<CommonResponse> call, retrofit2.Response<CommonResponse> response) {
                mInterface.successResponse(type, response, response.code());
            }

            @Override
            public void onFailure(@NonNull Call<CommonResponse> call, @NonNull Throwable t) {
                mInterface.failureResponse(type, t.getMessage());
            }
        });
    }

    public void getMasterAPI(final Constants.API_TYPE type,  RequestBody body) {

        String token = PreferenceManager.getValue(Constants.DAIRY_TOKEN);
        String dairyId = String.valueOf(PreferenceManager.getValueInt(Constants.DAIRY_ID));
        String finalToken = "Bearer " + token;
        Call<CommonResponse> call = mApi.getMaster(finalToken, dairyId, body);
        call.enqueue(new Callback<CommonResponse>() {

            @Override
            public void onResponse(Call<CommonResponse> call, retrofit2.Response<CommonResponse> response) {
                mInterface.successResponse(type, response, response.code());
            }

            @Override
            public void onFailure(@NonNull Call<CommonResponse> call, @NonNull Throwable t) {
                mInterface.failureResponse(type, t.getMessage());
            }
        });
    }

    public void editMaster(final Constants.API_TYPE type, String userID, RequestBody body) {

        String token = PreferenceManager.getValue(Constants.DAIRY_TOKEN);
        String finalToken = "Bearer " + token;
        Call<CommonResponse> call = mApi.editMaster(finalToken, userID, body);
        call.enqueue(new Callback<CommonResponse>() {

            @Override
            public void onResponse(Call<CommonResponse> call, retrofit2.Response<CommonResponse> response) {
                mInterface.successResponse(type, response, response.code());
            }

            @Override
            public void onFailure(@NonNull Call<CommonResponse> call, @NonNull Throwable t) {
                mInterface.failureResponse(type, t.getMessage());
            }
        });
    }


    public void deleteMaster(final Constants.API_TYPE type, String id) {

        String token = PreferenceManager.getValue(Constants.DAIRY_TOKEN);
        String finalToken = "Bearer " + token;
        Call<CommonResponse> call = mApi.deleteMaster(finalToken, id);
        call.enqueue(new Callback<CommonResponse>() {

            @Override
            public void onResponse(Call<CommonResponse> call, retrofit2.Response<CommonResponse> response) {
                mInterface.successResponse(type, response, response.code());
            }

            @Override
            public void onFailure(@NonNull Call<CommonResponse> call, @NonNull Throwable t) {
                mInterface.failureResponse(type, t.getMessage());
            }
        });
    }


    public void getMilkBuy(final Constants.API_TYPE type, RequestBody body) {

        String token = PreferenceManager.getValue(Constants.DAIRY_TOKEN);
        String dairyId = String.valueOf(PreferenceManager.getValueInt(Constants.DAIRY_ID));
        String finalToken = "Bearer " + token;
        Call<CommonResponse> call = mApi.getMilkBuy(finalToken, dairyId, body);
        call.enqueue(new Callback<CommonResponse>() {

            @Override
            public void onResponse(Call<CommonResponse> call, retrofit2.Response<CommonResponse> response) {
                mInterface.successResponse(type, response, response.code());
            }

            @Override
            public void onFailure(@NonNull Call<CommonResponse> call, @NonNull Throwable t) {
                mInterface.failureResponse(type, t.getMessage());
            }
        });
    }

    public void addMilkBuy(final Constants.API_TYPE type, RequestBody body) {

        String token = PreferenceManager.getValue(Constants.DAIRY_TOKEN);
        String finalToken = "Bearer " + token;
        Call<CommonResponse> call = mApi.addMilkBuy(finalToken, body);
        call.enqueue(new Callback<CommonResponse>() {

            @Override
            public void onResponse(Call<CommonResponse> call, retrofit2.Response<CommonResponse> response) {
                mInterface.successResponse(type, response, response.code());
            }

            @Override
            public void onFailure(@NonNull Call<CommonResponse> call, @NonNull Throwable t) {
                mInterface.failureResponse(type, t.getMessage());
            }
        });
    }

    public void editMilkBuy(final Constants.API_TYPE type, String id, RequestBody body) {

        String token = PreferenceManager.getValue(Constants.DAIRY_TOKEN);
        String finalToken = "Bearer " + token;
        Call<CommonResponse> call = mApi.editMilkBuy(finalToken, id, body);
        call.enqueue(new Callback<CommonResponse>() {

            @Override
            public void onResponse(Call<CommonResponse> call, retrofit2.Response<CommonResponse> response) {
                mInterface.successResponse(type, response, response.code());
            }

            @Override
            public void onFailure(@NonNull Call<CommonResponse> call, @NonNull Throwable t) {
                mInterface.failureResponse(type, t.getMessage());
            }
        });
    }

    public void deleteMilkBuy(final Constants.API_TYPE type, String id) {

        String token = PreferenceManager.getValue(Constants.DAIRY_TOKEN);
        String finalToken = "Bearer " + token;
        Call<CommonResponse> call = mApi.deleteMilkBuy(finalToken, id);
        call.enqueue(new Callback<CommonResponse>() {

            @Override
            public void onResponse(Call<CommonResponse> call, retrofit2.Response<CommonResponse> response) {
                mInterface.successResponse(type, response, response.code());
            }

            @Override
            public void onFailure(@NonNull Call<CommonResponse> call, @NonNull Throwable t) {
                mInterface.failureResponse(type, t.getMessage());
            }
        });
    }


    public void getMilkSell(final Constants.API_TYPE type, String id, RequestBody body) {

        String token = PreferenceManager.getValue(Constants.DAIRY_TOKEN);
        String dairyId = String.valueOf(PreferenceManager.getValueInt(Constants.DAIRY_ID));
        String finalToken = "Bearer " + token;
        Call<CommonResponse> call = mApi.getMilkSell(finalToken, dairyId, body);
        call.enqueue(new Callback<CommonResponse>() {

            @Override
            public void onResponse(Call<CommonResponse> call, retrofit2.Response<CommonResponse> response) {
                mInterface.successResponse(type, response, response.code());
            }

            @Override
            public void onFailure(@NonNull Call<CommonResponse> call, @NonNull Throwable t) {
                mInterface.failureResponse(type, t.getMessage());
            }
        });
    }

    public void addMilkSell(final Constants.API_TYPE type, RequestBody body) {

        String token = PreferenceManager.getValue(Constants.DAIRY_TOKEN);
        String finalToken = "Bearer " + token;
        Call<CommonResponse> call = mApi.addMilkSell(finalToken, body);
        call.enqueue(new Callback<CommonResponse>() {

            @Override
            public void onResponse(Call<CommonResponse> call, retrofit2.Response<CommonResponse> response) {
                mInterface.successResponse(type, response, response.code());
            }

            @Override
            public void onFailure(@NonNull Call<CommonResponse> call, @NonNull Throwable t) {
                mInterface.failureResponse(type, t.getMessage());
            }
        });
    }


    public void editMilkSell(final Constants.API_TYPE type, String id, RequestBody body) {

        String token = PreferenceManager.getValue(Constants.DAIRY_TOKEN);
        String finalToken = "Bearer " + token;
        Call<CommonResponse> call = mApi.editMilkSell(finalToken, id, body);
        call.enqueue(new Callback<CommonResponse>() {

            @Override
            public void onResponse(Call<CommonResponse> call, retrofit2.Response<CommonResponse> response) {
                mInterface.successResponse(type, response, response.code());
            }

            @Override
            public void onFailure(@NonNull Call<CommonResponse> call, @NonNull Throwable t) {
                mInterface.failureResponse(type, t.getMessage());
            }
        });
    }

    public void deleteMilkSell(final Constants.API_TYPE type, String id) {

        String token = PreferenceManager.getValue(Constants.DAIRY_TOKEN);
        String finalToken = "Bearer " + token;
        Debug.e(TAG, "Delete ID =--->" + id);
        Call<CommonResponse> call = mApi.deleteMilkSell(finalToken, id);
        call.enqueue(new Callback<CommonResponse>() {

            @Override
            public void onResponse(Call<CommonResponse> call, retrofit2.Response<CommonResponse> response) {
                mInterface.successResponse(type, response, response.code());
            }

            @Override
            public void onFailure(@NonNull Call<CommonResponse> call, @NonNull Throwable t) {
                mInterface.failureResponse(type, t.getMessage());
            }
        });
    }


    public void getAdvancePayment(final Constants.API_TYPE type, RequestBody body) {

        String token = PreferenceManager.getValue(Constants.DAIRY_TOKEN);
        String dairyId = String.valueOf(PreferenceManager.getValueInt(Constants.DAIRY_ID));
        String finalToken = "Bearer " + token;
        Call<CommonResponse> call = mApi.getAdvancePayment(finalToken, dairyId, body);
        call.enqueue(new Callback<CommonResponse>() {

            @Override
            public void onResponse(Call<CommonResponse> call, retrofit2.Response<CommonResponse> response) {
                mInterface.successResponse(type, response, response.code());
            }

            @Override
            public void onFailure(@NonNull Call<CommonResponse> call, @NonNull Throwable t) {
                mInterface.failureResponse(type, t.getMessage());
            }
        });
    }

    public void addAdvancePayment(final Constants.API_TYPE type, RequestBody body) {

        String token = PreferenceManager.getValue(Constants.DAIRY_TOKEN);
        String finalToken = "Bearer " + token;
        Call<CommonResponse> call = mApi.addAdvancePayment(finalToken, body);
        call.enqueue(new Callback<CommonResponse>() {

            @Override
            public void onResponse(Call<CommonResponse> call, retrofit2.Response<CommonResponse> response) {
                mInterface.successResponse(type, response, response.code());
            }

            @Override
            public void onFailure(@NonNull Call<CommonResponse> call, @NonNull Throwable t) {
                mInterface.failureResponse(type, t.getMessage());
            }
        });
    }

    public void editAdvancePayment(final Constants.API_TYPE type, String id, RequestBody body) {

        String token = PreferenceManager.getValue(Constants.DAIRY_TOKEN);
        String finalToken = "Bearer " + token;
        Call<CommonResponse> call = mApi.editAdvancePayment(finalToken, id, body);
        call.enqueue(new Callback<CommonResponse>() {

            @Override
            public void onResponse(Call<CommonResponse> call, retrofit2.Response<CommonResponse> response) {
                mInterface.successResponse(type, response, response.code());
            }

            @Override
            public void onFailure(@NonNull Call<CommonResponse> call, @NonNull Throwable t) {
                mInterface.failureResponse(type, t.getMessage());
            }
        });
    }

    public void deleteAdvancePayment(final Constants.API_TYPE type, String id) {

        String token = PreferenceManager.getValue(Constants.DAIRY_TOKEN);
        String finalToken = "Bearer " + token;
        Call<CommonResponse> call = mApi.deleteAdvancePayment(finalToken, id);
        call.enqueue(new Callback<CommonResponse>() {

            @Override
            public void onResponse(Call<CommonResponse> call, retrofit2.Response<CommonResponse> response) {
                mInterface.successResponse(type, response, response.code());
            }

            @Override
            public void onFailure(@NonNull Call<CommonResponse> call, @NonNull Throwable t) {
                mInterface.failureResponse(type, t.getMessage());
            }
        });
    }


    public void getMilkPurchaseReport(final Constants.API_TYPE type, RequestBody requestBody) {

        String token = PreferenceManager.getValue(Constants.DAIRY_TOKEN);
        String dairyId = String.valueOf(PreferenceManager.getValueInt(Constants.DAIRY_ID));
        String finalToken = "Bearer " + token;
        Call<CommonResponse> call = mApi.getMilkPurchaseReport(finalToken, dairyId, requestBody);
        call.enqueue(new Callback<CommonResponse>() {

            @Override
            public void onResponse(Call<CommonResponse> call, retrofit2.Response<CommonResponse> response) {
                mInterface.successResponse(type, response, response.code());
            }

            @Override
            public void onFailure(@NonNull Call<CommonResponse> call, @NonNull Throwable t) {
                mInterface.failureResponse(type, t.getMessage());
            }
        });
    }


    public void getMilkSaleReport(final Constants.API_TYPE type, RequestBody requestBody) {

        String token = PreferenceManager.getValue(Constants.DAIRY_TOKEN);
        String dairyId = String.valueOf(PreferenceManager.getValueInt(Constants.DAIRY_ID));
        String finalToken = "Bearer " + token;
        Call<CommonResponse> call = mApi.getMilkSaleReport(finalToken, dairyId, requestBody);
        call.enqueue(new Callback<CommonResponse>() {

            @Override
            public void onResponse(Call<CommonResponse> call, retrofit2.Response<CommonResponse> response) {
                mInterface.successResponse(type, response, response.code());
            }

            @Override
            public void onFailure(@NonNull Call<CommonResponse> call, @NonNull Throwable t) {
                mInterface.failureResponse(type, t.getMessage());
            }
        });
    }

    public void getLegerReport(final Constants.API_TYPE type, RequestBody requestBody) {

        String token = PreferenceManager.getValue(Constants.DAIRY_TOKEN);
        String dairyId = String.valueOf(PreferenceManager.getValueInt(Constants.DAIRY_ID));
        String finalToken = "Bearer " + token;
        Call<CommonResponse> call = mApi.getLegerReport(finalToken,dairyId, requestBody);
        call.enqueue(new Callback<CommonResponse>() {

            @Override
            public void onResponse(Call<CommonResponse> call, retrofit2.Response<CommonResponse> response) {
                mInterface.successResponse(type, response, response.code());
            }

            @Override
            public void onFailure(@NonNull Call<CommonResponse> call, @NonNull Throwable t) {
                mInterface.failureResponse(type, t.getMessage());
            }
        });
    }

    public void getPaymentReport(final Constants.API_TYPE type, RequestBody requestBody) {

        String token = PreferenceManager.getValue(Constants.DAIRY_TOKEN);
        String dairyId = String.valueOf(PreferenceManager.getValueInt(Constants.DAIRY_ID));
        String finalToken = "Bearer " + token;
        Call<CommonResponse> call = mApi.getPaymentReport(finalToken, dairyId,requestBody);
        call.enqueue(new Callback<CommonResponse>() {

            @Override
            public void onResponse(Call<CommonResponse> call, retrofit2.Response<CommonResponse> response) {
                mInterface.successResponse(type, response, response.code());
            }

            @Override
            public void onFailure(@NonNull Call<CommonResponse> call, @NonNull Throwable t) {
                mInterface.failureResponse(type, t.getMessage());
            }
        });
    }

    public void getMemberList(final Constants.API_TYPE type, RequestBody requestBody) {

        String token = PreferenceManager.getValue(Constants.DAIRY_TOKEN);
        String dairyId = String.valueOf(PreferenceManager.getValueInt(Constants.DAIRY_ID));
        String finalToken = "Bearer " + token;
        Call<CommonResponse> call = mApi.getMemberList(finalToken,dairyId, requestBody);
        call.enqueue(new Callback<CommonResponse>() {

            @Override
            public void onResponse(Call<CommonResponse> call, retrofit2.Response<CommonResponse> response) {
                mInterface.successResponse(type, response, response.code());
            }

            @Override
            public void onFailure(@NonNull Call<CommonResponse> call, @NonNull Throwable t) {
                mInterface.failureResponse(type, t.getMessage());
            }
        });
    }


//
//    public void forgotPasswordApi(final Constants.API_TYPE type, String forgotPasswordRequest) {
//
//        String token = PreferenceManager.getValue(Constants.DAIRY_TOKEN);
//        String finalToken = "Bearer " + token;
//        Call<CommonResponse> call = mApi.forgotPasswordApi(forgotPasswordRequest);
//        call.enqueue(new Callback<CommonResponse>() {
//
//            @Override
//            public void onResponse(Call<CommonResponse> call, retrofit2.Response<CommonResponse> response) {
//                mInterface.successResponse(type, response, response.code());
//            }
//
//            @Override
//            public void onFailure(@NonNull Call<CommonResponse> call, @NonNull Throwable t) {
//                mInterface.failureResponse(type, t.getMessage());
//            }
//        });
//    }
//
//    public void recoverPasswordApi(final Constants.API_TYPE type,  HashMap<String, String> recoverParameter) {
//
//        String token = PreferenceManager.getValue(Constants.DAIRY_TOKEN);
//        String finalToken = "Bearer " + token;
//
//        Call<CategoriesResponse> call = mApi.recoverPasswordApi(finalToken, recoverParameter);
//        call.enqueue(new Callback<CategoriesResponse>() {
//
//            @Override
//            public void onResponse(Call<CategoriesResponse> call, retrofit2.Response<CategoriesResponse> response) {
//                mInterface.successResponse(type, response, response.code());
//            }
//
//            @Override
//            public void onFailure(@NonNull Call<CategoriesResponse> call, @NonNull Throwable t) {
//                mInterface.failureResponse(type, t.getMessage());
//            }
//        });
//    }
//
//    public void categoriesApi(final Constants.API_TYPE type) {
//
//
//        Call<CategoriesResponse> call = mApi.categoriesApi();
//        call.enqueue(new Callback<CategoriesResponse>() {
//
//            @Override
//            public void onResponse(Call<CategoriesResponse> call, retrofit2.Response<CategoriesResponse> response) {
//                mInterface.successResponse(type, response, response.code());
//            }
//
//            @Override
//            public void onFailure(@NonNull Call<CategoriesResponse> call, @NonNull Throwable t) {
//                mInterface.failureResponse(type, t.getMessage());
//            }
//        });
//    }
//
//    public void homeSliderApi(final Constants.API_TYPE type) {
//
//
//        Call<CategoriesResponse> call = mApi.homeSliderApi();
//        call.enqueue(new Callback<CategoriesResponse>() {
//
//            @Override
//            public void onResponse(Call<CategoriesResponse> call, retrofit2.Response<CategoriesResponse> response) {
//                mInterface.successResponse(type, response, response.code());
//            }
//
//            @Override
//            public void onFailure(@NonNull Call<CategoriesResponse> call, @NonNull Throwable t) {
//                mInterface.failureResponse(type, t.getMessage());
//            }
//        });
//    }
//
//    public void subCategoriesApi(final Constants.API_TYPE type, String id) {
//
//
//        Call<CategoriesResponse> call = mApi.subCategoriesApi(id);
//        call.enqueue(new Callback<CategoriesResponse>() {
//
//            @Override
//            public void onResponse(Call<CategoriesResponse> call, retrofit2.Response<CategoriesResponse> response) {
//                mInterface.successResponse(type, response, response.code());
//            }
//
//            @Override
//            public void onFailure(@NonNull Call<CategoriesResponse> call, @NonNull Throwable t) {
//                mInterface.failureResponse(type, t.getMessage());
//            }
//        });
//    }
//
//    public void productApi(final Constants.API_TYPE type, String id) {
//
//
//        Call<ProductResponse> call = mApi.productApi(id);
//        call.enqueue(new Callback<ProductResponse>() {
//
//            @Override
//            public void onResponse(Call<ProductResponse> call, retrofit2.Response<ProductResponse> response) {
//                mInterface.successResponse(type, response, response.code());
//            }
//
//            @Override
//            public void onFailure(@NonNull Call<ProductResponse> call, @NonNull Throwable t) {
//                mInterface.failureResponse(type, t.getMessage());
//            }
//        });
//    }
//
//    public void productDetailsApi(final Constants.API_TYPE type, String id) {
//
//
//        Call<ProductDetailsResponse> call = mApi.productDetailsApi(id);
//        call.enqueue(new Callback<ProductDetailsResponse>() {
//
//            @Override
//            public void onResponse(Call<ProductDetailsResponse> call, retrofit2.Response<ProductDetailsResponse> response) {
//                mInterface.successResponse(type, response, response.code());
//            }
//
//            @Override
//            public void onFailure(@NonNull Call<ProductDetailsResponse> call, @NonNull Throwable t) {
//                mInterface.failureResponse(type, t.getMessage());
//            }
//        });
//    }
//
//
//    public void ConfirmOrderAPI(final Constants.API_TYPE type, HashMap<String, String> hashMap) {
//
//        String token = PreferenceManager.getValue(Constants.DAIRY_TOKEN);
//        String finalToken = "Bearer " + token;
//
//        Call<ConfirmOrderResponse> call = mApi.confirmOrderApi(finalToken, hashMap);
//        call.enqueue(new Callback<ConfirmOrderResponse>() {
//
//            @Override
//            public void onResponse(Call<ConfirmOrderResponse> call, retrofit2.Response<ConfirmOrderResponse> response) {
//                mInterface.successResponse(type, response, response.code());
//            }
//
//            @Override
//            public void onFailure(@NonNull Call<ConfirmOrderResponse> call, @NonNull Throwable t) {
//                mInterface.failureResponse(type, t.getMessage());
//            }
//        });
//    }
//
//
//
//    public void notificationApi(final Constants.API_TYPE type) {
//
//        String token = PreferenceManager.getValue(Constants.DAIRY_TOKEN);
//        String finalToken = "Bearer " + token;
//
//        Call<NotificationResponse> call = mApi.notificationApi(finalToken);
//        call.enqueue(new Callback<NotificationResponse>() {
//
//            @Override
//            public void onResponse(Call<NotificationResponse> call, retrofit2.Response<NotificationResponse> response) {
//                mInterface.successResponse(type, response, response.code());
//            }
//
//            @Override
//            public void onFailure(@NonNull Call<NotificationResponse> call, @NonNull Throwable t) {
//                mInterface.failureResponse(type, t.getMessage());
//            }
//        });
//    }
//
//    public void readNotificationApi(final Constants.API_TYPE type,int notificationId) {
//
//        String token = PreferenceManager.getValue(Constants.DAIRY_TOKEN);
//        String finalToken = "Bearer " + token;
//
//        Call<CommonResponse> call = mApi.readNotification(finalToken,notificationId);
//        call.enqueue(new Callback<CommonResponse>() {
//
//            @Override
//            public void onResponse(Call<CommonResponse> call, retrofit2.Response<CommonResponse> response) {
//                mInterface.successResponse(type, response, response.code());
//            }
//
//            @Override
//            public void onFailure(@NonNull Call<CommonResponse> call, @NonNull Throwable t) {
//                mInterface.failureResponse(type, t.getMessage());
//            }
//        });
//    }
//
//
//    public void deleteNotificationApi(final Constants.API_TYPE type, int notificationId) {
//
//        String token = PreferenceManager.getValue(Constants.DAIRY_TOKEN);
//        String finalToken = "Bearer " + token;
//
//        Call<CommonResponse> call = mApi.deleteNotification(finalToken, notificationId);
//        call.enqueue(new Callback<CommonResponse>() {
//
//            @Override
//            public void onResponse(Call<CommonResponse> call, retrofit2.Response<CommonResponse> response) {
//                mInterface.successResponse(type, response, response.code());
//            }
//
//            @Override
//            public void onFailure(@NonNull Call<CommonResponse> call, @NonNull Throwable t) {
//                mInterface.failureResponse(type, t.getMessage());
//            }
//        });
//    }
//
//
//    public void cartApi(final Constants.API_TYPE type, HashMap<String, String> hashMap) {
//
//
//        Call<CommonResponse> call = mApi.cartApi(hashMap);
//        call.enqueue(new Callback<CommonResponse>() {
//
//            @Override
//            public void onResponse(Call<CommonResponse> call, retrofit2.Response<CommonResponse> response) {
//                mInterface.successResponse(type, response, response.code());
//            }
//
//            @Override
//            public void onFailure(@NonNull Call<CommonResponse> call, @NonNull Throwable t) {
//                mInterface.failureResponse(type, t.getMessage());
//            }
//        });
//    }
//
//
//    public void addCartApi(final Constants.API_TYPE type, HashMap<String, String> hashMap) {
//
//        String token = PreferenceManager.getValue(Constants.DAIRY_TOKEN);
//        String finalToken = "Bearer " + token;
//
//        Call<CommonResponse> call = mApi.addCartApi(hashMap);
//        call.enqueue(new Callback<CommonResponse>() {
//
//            @Override
//            public void onResponse(Call<CommonResponse> call, retrofit2.Response<CommonResponse> response) {
//                mInterface.successResponse(type, response, response.code());
//            }
//
//            @Override
//            public void onFailure(@NonNull Call<CommonResponse> call, @NonNull Throwable t) {
//                mInterface.failureResponse(type, t.getMessage());
//            }
//        });
//    }
//
//
//    public void userInfoApi(final Constants.API_TYPE type, String token) {
//
//        String finalToken = "Bearer " + token;
//        Call<CommonResponse> call = mApi.userInfoApi(finalToken);
//        call.enqueue(new Callback<CommonResponse>() {
//
//            @Override
//            public void onResponse(Call<CommonResponse> call, retrofit2.Response<CommonResponse> response) {
//                Debug.e(TAG, "Login Response =---->" + response);
//                mInterface.successResponse(type, response, response.code());
//            }
//
//            @Override
//            public void onFailure(@NonNull Call<CommonResponse> call, @NonNull Throwable t) {
//                Debug.e(TAG, "Login onFailure Response =---->" + t);
//                mInterface.failureResponse(type, t.getMessage());
//            }
//        });
//    }
//
//    public void changePasswordApi(final Constants.API_TYPE type, HashMap<String, String> changePasswordRequest) {
//
//        String token = PreferenceManager.getValue(Constants.DAIRY_TOKEN);
//        String finalToken = "Bearer " + token;
//        Call<CommonResponse> call = mApi.changePasswordApi(finalToken, changePasswordRequest);
//        call.enqueue(new Callback<CommonResponse>() {
//
//            @Override
//            public void onResponse(Call<CommonResponse> call, retrofit2.Response<CommonResponse> response) {
//                mInterface.successResponse(type, response, response.code());
//            }
//
//            @Override
//            public void onFailure(@NonNull Call<CommonResponse> call, @NonNull Throwable t) {
//                mInterface.failureResponse(type, t.getMessage());
//            }
//        });
//    }
//
//
//    public void editProfileApi(final Constants.API_TYPE type, HashMap<String, String> changePasswordRequest) {
//
//        String token = PreferenceManager.getValue(Constants.DAIRY_TOKEN);
//        String finalToken = "Bearer " + token;
//        Call<CommonResponse> call = mApi.editProfileApi(finalToken, changePasswordRequest);
//        call.enqueue(new Callback<CommonResponse>() {
//
//            @Override
//            public void onResponse(Call<CommonResponse> call, retrofit2.Response<CommonResponse> response) {
//                mInterface.successResponse(type, response, response.code());
//            }
//
//            @Override
//            public void onFailure(@NonNull Call<CommonResponse> call, @NonNull Throwable t) {
//                mInterface.failureResponse(type, t.getMessage());
//            }
//        });
//    }
//
//    public void newInquiryApi(final Constants.API_TYPE type, HashMap<String, String> hashMap) {
//
//        Call<CommonResponse> call = mApi.newInquiryApi(hashMap);
//        call.enqueue(new Callback<CommonResponse>() {
//
//            @Override
//            public void onResponse(Call<CommonResponse> call, retrofit2.Response<CommonResponse> response) {
//                mInterface.successResponse(type, response, response.code());
//            }
//
//            @Override
//            public void onFailure(@NonNull Call<CommonResponse> call, @NonNull Throwable t) {
//                mInterface.failureResponse(type, t.getMessage());
//            }
//        });
//    }
//
//    public void orderListApi(final Constants.API_TYPE type) {
//
//        String token = PreferenceManager.getValue(Constants.DAIRY_TOKEN);
//        String finalToken = "Bearer " + token;
//
//        Call<ConfirmOrderResponse> call = mApi.orderListApi(finalToken);
//        call.enqueue(new Callback<ConfirmOrderResponse>() {
//
//            @Override
//            public void onResponse(Call<ConfirmOrderResponse> call, retrofit2.Response<ConfirmOrderResponse> response) {
//                mInterface.successResponse(type, response, response.code());
//            }
//
//            @Override
//            public void onFailure(@NonNull Call<ConfirmOrderResponse> call, @NonNull Throwable t) {
//                mInterface.failureResponse(type, t.getMessage());
//            }
//        });
//
//    }
//
//
//    public void settingApi(final Constants.API_TYPE type) {
//
//        String token = PreferenceManager.getValue(Constants.DAIRY_TOKEN);
//        String finalToken = "Bearer " + token;
//
//        Call<SettingResponse> call = mApi.settingApi(finalToken);
//        call.enqueue(new Callback<SettingResponse>() {
//
//            @Override
//            public void onResponse(Call<SettingResponse> call, retrofit2.Response<SettingResponse> response) {
//                mInterface.successResponse(type, response, response.code());
//            }
//
//            @Override
//            public void onFailure(@NonNull Call<SettingResponse> call, @NonNull Throwable t) {
//                mInterface.failureResponse(type, t.getMessage());
//            }
//        });
//
//    }
//
//    public void logOutApi(final Constants.API_TYPE type) {
//
//        String token = PreferenceManager.getValue(Constants.DAIRY_TOKEN);
//        String finalToken = "Bearer " + token;
//
//        Call<CommonResponse> call = mApi.logOutApi(finalToken);
//        call.enqueue(new Callback<CommonResponse>() {
//
//            @Override
//            public void onResponse(Call<CommonResponse> call, retrofit2.Response<CommonResponse> response) {
//                mInterface.successResponse(type, response, response.code());
//            }
//
//            @Override
//            public void onFailure(@NonNull Call<CommonResponse> call, @NonNull Throwable t) {
//                mInterface.failureResponse(type, t.getMessage());
//            }
//        });
//    }

}