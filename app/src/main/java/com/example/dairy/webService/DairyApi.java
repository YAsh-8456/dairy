package com.example.dairy.webService;


import com.example.dairy.modal.CommonResponse;

import java.util.HashMap;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.FieldMap;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;


public interface DairyApi {


    // DAIRY LOGIN ------------------------------------------------------------------- >
    @POST("signin")
    Call<CommonResponse> dairyLogin(@Body RequestBody body);

    // DAIRY  ------------------------------------------------------------------------ >
    @POST("dairy")
    Call<CommonResponse> dairyRegister(@Body RequestBody body);

    @PUT("dairy/{id}")
    Call<CommonResponse> dairyEdit(@Header("Authorization") String userAuth, @Path("id") String id, @Body RequestBody body);

    @DELETE("dairy/{id}")
    Call<CommonResponse> dairyRemove(@Header("Authorization") String userAuth, @Path("id") String id);

    //
    @POST("verify")
    Call<CommonResponse> verifyApi(@Header("Authorization") String userAuth, @FieldMap HashMap<String, String> verifyParameter);

    // MASTER ENTRY ------------------------------------------------------------------------ >
    @POST("customer")
    Call<CommonResponse> addMaster(@Header("Authorization") String userAuth, @Body RequestBody body);

    @POST("customer/search")
    Call<CommonResponse> getMaster(@Header("Authorization") String finalToken, @Query("dairyid") String dairyid, @Body RequestBody body);

    @PUT("customer/{id}")
    Call<CommonResponse> editMaster(@Header("Authorization") String userAuth, @Path("id") String id, @Body RequestBody body);

    @DELETE("customer/{id}")
    Call<CommonResponse> deleteMaster(@Header("Authorization") String userAuth, @Path("id") String id);


    //  MIll SEll  ------------------------------------------------------------------------ >
    //@Multipart
    // @FormUrlEncoded
    @POST("local-sale")
    Call<CommonResponse> addMilkSell(@Header("Authorization") String userAuth, @Body RequestBody body);

    @POST("local-sale/search")
    Call<CommonResponse> getMilkSell(@Header("Authorization") String finalToken, @Query("dairyid") String dairyid, @Body RequestBody body);

    //  @FormUrlEncoded
    // @Multipart
    @PUT("local-sale/{id}")
    Call<CommonResponse> editMilkSell(@Header("Authorization") String userAuth, @Path("id") String id, @Body RequestBody body);

    @DELETE("local-sale/{id}")
    Call<CommonResponse> deleteMilkSell(@Header("Authorization") String userAuth, @Path("id") String id);


    //  MIll BUY ------------------------------------------------------------------------ >
    @POST("milk-collection")
    Call<CommonResponse> addMilkBuy(@Header("Authorization") String userAuth, @Body RequestBody body);

    @POST("milk-collection/search")
    Call<CommonResponse> getMilkBuy(@Header("Authorization") String finalToken, @Query("dairyid") String dairyid, @Body RequestBody body);

    @PUT("milk-collection/{id}")
    Call<CommonResponse> editMilkBuy(@Header("Authorization") String userAuth, @Path("id") String id, @Body RequestBody body);

    @DELETE("milk-collection/{id}")
    Call<CommonResponse> deleteMilkBuy(@Header("Authorization") String userAuth, @Path("id") String id);


    //  MIll ADVANCE PAYMENT ------------------------------------------------------------------------ >
    @POST("payment")
    Call<CommonResponse> addAdvancePayment(@Header("Authorization") String userAuth, @Body RequestBody body);

    @POST("payment/search")
    Call<CommonResponse> getAdvancePayment(@Header("Authorization") String finalToken, @Query("dairyid") String dairyid, @Body RequestBody body);

    @PUT("payment/{id}")
    Call<CommonResponse> editAdvancePayment(@Header("Authorization") String userAuth, @Path("id") String id, @Body RequestBody body);

    @DELETE("payment/{id}")
    Call<CommonResponse> deleteAdvancePayment(@Header("Authorization") String userAuth, @Path("id") String id);


    //  MIll REPORTS PAYMENT ------------------------------------------------------------------------ >
    @POST("milk-collection/ledger")
    Call<CommonResponse> getLegerReport(@Header("Authorization") String userAuth,@Query("dairyid") String dairyid, @Body RequestBody body);

    @POST("milk-collection")
    Call<CommonResponse> getMilkPurchaseReport(@Header("Authorization") String userAuth, @Query("dairyid") String dairyid, @Body RequestBody body);

    @POST("local-sale")
    Call<CommonResponse> getMilkSaleReport(@Header("Authorization") String userAuth,@Query("dairyid") String dairyid, @Body RequestBody body);


    @POST("payment")
    Call<CommonResponse> getPaymentReport(@Header("Authorization") String userAuth, @Query("dairyid") String dairyid, @Body RequestBody body);

    @POST("customer")
    Call<CommonResponse> getMemberList(@Header("Authorization") String userAuth, @Query("dairyid") String dairyid, @Body RequestBody body);




//
//    // Forgot Password
//    @FormUrlEncoded
//    @POST("forgotpassword")
//    Call<CommonResponse> forgotPasswordApi(@Field("email") String phone_no);
//
//    @FormUrlEncoded
//    @POST("recoverpassword")
//    Call<CategoriesResponse> recoverPasswordApi(@Header("Authorization") String userAuth, @FieldMap HashMap<String, String> recoverParameter);
//
//
//    // Home screen ---------------------------------------------------------------------
//    @GET("getcategories")
//    Call<CategoriesResponse> categoriesApi();
//
//    @GET("gethomeslider")
//    Call<CategoriesResponse> homeSliderApi();
//
//
//    // Product list screen ---------------------------------------------------------------------
//    @GET("getsubcategories/{id}")
//    Call<CategoriesResponse> subCategoriesApi(@Path("id") String id);
//
//
//    @GET("getcategoryproducts/{id}")
//    Call<ProductResponse> productApi(@Path("id") String id);
//
//    // Product details
//    @GET("getproductinfo/{id}")
//    Call<ProductDetailsResponse> productDetailsApi(@Path("id") String id);
//
//
//    // Notification  details
//    @GET("notification/list")
//    Call<NotificationResponse> notificationApi(@Header("Authorization") String userAuth);
//
//    @GET("notification/read")
//    Call<CommonResponse> readNotification(@Header("Authorization") String userAuth, @Field("ids") int ids);
//
//    @FormUrlEncoded
//    @POST("notification/delete")
//    Call<CommonResponse> deleteNotification(@Header("Authorization") String userAuth, @Field("ids") int ids);
//
//    // Cart details
//    @FormUrlEncoded
//    @POST("")
//    Call<CommonResponse> cartApi(@FieldMap HashMap<String, String> hashMap);
//
//    // Cart details
//    @FormUrlEncoded
//    @POST("")
//    Call<CommonResponse> addCartApi(@FieldMap HashMap<String, String> hashMap);
//
//
//    //     profile screen api call list ---------------------------------------------------------------------
//    // get user details
//    @GET("getuser")
//    Call<CommonResponse> userInfoApi(@Header("Authorization") String userAuth);
//
//    // Inquiry API call
//    @FormUrlEncoded
//    @POST("inquirystore")
//    Call<CommonResponse> newInquiryApi(@FieldMap HashMap<String, String> hashMap);
//
//    // Change Password
//    @FormUrlEncoded
//    @POST("changepassword")
//    Call<CommonResponse> changePasswordApi(@Header("Authorization") String userAuth, @FieldMap HashMap<String, String> create);
//
//    // Edit profile
//    @FormUrlEncoded
//    @POST("editaccountinfo")
//    Call<CommonResponse> editProfileApi(@Header("Authorization") String userAuth, @FieldMap HashMap<String, String> create);
//
//
//    // get txn token
//    @GET("initiateTransaction")
//    Call<TxtResponse> getTxtToken(@Query("mid") String mid, @Query("orderId") String orderId);
//
//
//    @FormUrlEncoded
//    @POST("order/storeorder")
//    Call<ConfirmOrderResponse> confirmOrderApi(@Header("Authorization") String userAuth, @FieldMap HashMap<String, String> hashMap);
//
//
//    @GET("order/getuserorderlist")
//    Call<ConfirmOrderResponse> orderListApi(@Header("Authorization") String userAuth);
//
//
//
//    // Calculation API call
//    @GET("getsettings")
//    Call<SettingResponse> settingApi(@Header("Authorization") String userAuth);
//
//    // Calculation API call
//    @GET("logout")
//    Call<CommonResponse> logOutApi(@Header("Authorization") String userAuth);
}
