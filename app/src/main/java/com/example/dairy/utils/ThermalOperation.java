package com.example.dairy.utils;

import static android.content.ContentValues.TAG;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.dantsu.escposprinter.textparser.PrinterTextParserImg;
import com.example.dairy.R;
import com.example.dairy.modal.Result;
import com.example.dairy.utils.async.AsyncBluetoothEscPosPrint;
import com.example.dairy.utils.async.AsyncEscPosPrinter;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class ThermalOperation {


    public Activity context;
    public static final int PERMISSION_BLUETOOTH = 1;
    private static final boolean dummyPrint = false;
    private String dairyName = "";
    private String dairyContact = "";

    public ThermalOperation(Activity context) {
        this.context = context;
        dairyName = PreferenceManager.getValue(Constants.DAIRY_NAME);
        dairyContact = PreferenceManager.getValue(Constants.USER_MO_NUMBER);
    }


    public boolean readyTOPrintSlip() {
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.BLUETOOTH) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(context, new String[]{Manifest.permission.BLUETOOTH}, PERMISSION_BLUETOOTH);
            return false;
        } else {

            return true;
        }
    }

    private void checkValidation() {
        if (Locale.getDefault().getLanguage().toUpperCase().equals("HI")) {
            Toast.makeText(context, context.getResources().getString(R.string.hindi_is_not_support), Toast.LENGTH_SHORT).show();
            return;
        }

        if (!readyTOPrintSlip()) {
            Toast.makeText(context, context.getResources().getString(R.string.printer_is_not_connected), Toast.LENGTH_SHORT).show();
            return;
        }
    }

    public void printSingleBuy(Result result) {
        checkValidation();
        new AsyncBluetoothEscPosPrint(context).execute(getAsyncEscPosPrinter(getMilkBuySlip(result)));
    }

    public void printBitmap(Bitmap result) {
        new AsyncBluetoothEscPosPrint(context).execute(getAsyncEscPosPrinter(getBitmapSlip(result)));
    }

    public void printSingleSell(Result result) {
        checkValidation();
        new AsyncBluetoothEscPosPrint(context).execute(getAsyncEscPosPrinter(getMilkSellSlip(result)));
    }

    public void printSingleAdvancePayment(Result result) {
        checkValidation();
        new AsyncBluetoothEscPosPrint(context).execute(getAsyncEscPosPrinter(getMilkAdvancePaymentSlip(result)));
    }


    public void printProducerList(List<Result> resultList) {
        checkValidation();
        new AsyncBluetoothEscPosPrint(context).execute(getAsyncEscPosPrinter(getProducerSlip(resultList)));
    }

    public void printLedgerList(List<Result> resultList, String startDate, String endDate) {
        if (Locale.getDefault().getLanguage().toUpperCase().equals("HI")) {
            Toast.makeText(context, context.getResources().getString(R.string.hindi_is_not_support), Toast.LENGTH_SHORT).show();
            return;
        }
        new AsyncBluetoothEscPosPrint(context).execute(getAsyncEscPosPrinter(getLedgerSlip(resultList, startDate, endDate)));
    }


    public void printPurchaseList(List<Result> resultList, String shiftData, String milkData) {
        if (Locale.getDefault().getLanguage().toUpperCase().equals("HI")) {
            Toast.makeText(context, context.getResources().getString(R.string.hindi_is_not_support), Toast.LENGTH_SHORT).show();
            return;
        }
        new AsyncBluetoothEscPosPrint(context).execute(getAsyncEscPosPrinter(getPurchaseList(resultList, shiftData, milkData)));
    }

    public void printSellList(List<Result> resultList, String shiftData, String milkData) {
        if (Locale.getDefault().getLanguage().toUpperCase().equals("HI")) {
            Toast.makeText(context, context.getResources().getString(R.string.hindi_is_not_support), Toast.LENGTH_SHORT).show();
            return;
        }
        new AsyncBluetoothEscPosPrint(context).execute(getAsyncEscPosPrinter(getSellList(resultList, shiftData, milkData)));
    }

    public void printAdvancePaymentList(List<Result> resultList) {
        if (Locale.getDefault().getLanguage().toUpperCase().equals("HI")) {
            Toast.makeText(context, context.getResources().getString(R.string.hindi_is_not_support), Toast.LENGTH_SHORT).show();
            return;
        }
        new AsyncBluetoothEscPosPrint(context).execute(getAsyncEscPosPrinter(getAdvancePaymentList(resultList)));
    }

    public String getPurchaseList(List<Result> resultList, String shiftData, String milkData) {

        String shift = shiftData.toUpperCase();
        String milk = milkData.toUpperCase();

        SimpleDateFormat format = new SimpleDateFormat("'on' yyyy-MM-dd 'at' HH:mm:ss");
        String printData =
                "[C]<u><b><font size='small'>" + dairyName + "</b></u> \n" +
                        "[C]<font size='small'>" + format.format(new Date()) + "</font>\n" +
                        "[C]<b>" + dairyContact + "</b>\n" +
                        "[C]<b>" + context.getResources().getString(R.string.purchase_milk) + "</b>\n" +
                        "[C]=================\n" +
                        "[L]<b><font size='small'>" + context.getResources().getString(R.string.shift) + " :- </b> " + shift + " \n" +
                        "[L]<b><font size='small'>" + context.getResources().getString(R.string.milk) + " :- </b> " + milk + " \n" +
                        "[C]=============================\n" +
                        "[L]<b><font size='small'>" +
                        context.getResources().getString(R.string.code).toUpperCase() + "  " +
                        context.getResources().getString(R.string.fat).toUpperCase() + "  " +
                        context.getResources().getString(R.string.snf).toUpperCase() + "  " +
                        context.getResources().getString(R.string.liter).toUpperCase() + "  " +
                        "[R]" + context.getResources().getString(R.string.amount).toUpperCase() +
                        "</b>  \n" +
                        "[C]=============================\n";

        String producerList = "";
        float totalAmount = 0f;
        for (int i = 0; i < resultList.size(); i++) {

            Result member = resultList.get(i);
            String code = "01";
            String liter = "3.0";
            String fat = "6.6";
            String snf = "7.0";
            String amount = "123.00";

            if (!dummyPrint) {
                code = member.getCustomer().getMemberCode();
                liter = member.getLiter();
                fat = member.getFat();
                snf = member.getSnf();
                amount = member.getAmount();
            }

            totalAmount += Float.parseFloat(amount);
            producerList = producerList + " " +
                    code + "  " +
                    fat + "  " +
                    snf + "   " +
                    liter + "   " +
                    "[R]" + amount + " </b>  \n";
        }

        printData = printData + producerList;
        printData += "[C]===============================\n" +
//                "[L]<b><font size='small'>" + context.getResources().getString(R.string.deduction).toUpperCase() + ":  </b>" + "0" + "\n" +
                "[L]<b><font size='small'>" + context.getResources().getString(R.string.totalAmount).toUpperCase() + ":  </b>" + totalAmount + "\n";

        return printData;
    }

    public String getSellList(List<Result> resultList, String shiftData, String milkData) {

        String shift = shiftData.toUpperCase();
        String milk = milkData.toUpperCase();

        SimpleDateFormat format = new SimpleDateFormat("'on' yyyy-MM-dd 'at' HH:mm:ss");
        String printData =
                "[C]<u><b><font size='small'>" + dairyName + "</b></u> \n" +
                        "[C]<font size='small'>" + format.format(new Date()) + "</font>\n" +
                        "[C]<b>" + dairyContact + "</b>\n" +
                        "[C]<b>" + context.getResources().getString(R.string.milk_sell) + "</b>\n" +
                        "[C]=================\n" +
                        "[L]<b><font size='small'>" + context.getResources().getString(R.string.shift) + " :- </b> " + shift + " \n" +
                        "[L]<b><font size='small'>" + context.getResources().getString(R.string.milk) + " :- </b> " + milk + " \n" +
                        "[C]=============================\n" +
                        "[L]<b><font size='small'>" +
                        context.getResources().getString(R.string.date_d).toUpperCase() + "  " +
                        context.getResources().getString(R.string.shift_s).toUpperCase() + "  " +
                        context.getResources().getString(R.string.liter).toUpperCase() + "  " +
                        context.getResources().getString(R.string.liter_rate).toUpperCase() + "  " +
                        "[R]" + context.getResources().getString(R.string.amount).toUpperCase() +
                        "</b>  \n" +
                        "[C]=============================\n";

        String producerList = "";
        float totalAmount = 0f;
        for (int i = 0; i < resultList.size(); i++) {
            Result member = resultList.get(i);
            String date = "01";
            String memberCode = "01";
            String liter = "3.0";
            String literRate = "25.0";
            String amount = "125.00";

            if (!dummyPrint) {
                date = Utils.onlyPrintDate(member.getSaleDate());
                memberCode = member.getCustomer().getMemberCode();
                liter = member.getLiter();
                literRate = member.getRate();
                amount = member.getTotalAmount();

            }

            totalAmount += Float.parseFloat(amount);
            producerList = producerList +
                    date + " " +
                    memberCode + "   " +
                    liter + "      " +
                    literRate + "   " +
                    "[R]" + amount + " </b>  \n";
        }

        printData = printData + producerList;
        printData += "[C]===============================\n" +
//                "[L]<b><font size='small'>" + context.getResources().getString(R.string.deduction).toUpperCase() + ":  </b>" + "0" + "\n" +
                "[L]<b><font size='small'>" + context.getResources().getString(R.string.totalAmount).toUpperCase() + ":  </b>" + totalAmount + "\n";

        return printData;
    }

    public String getAdvancePaymentList(List<Result> resultList) {


        SimpleDateFormat format = new SimpleDateFormat("'on' yyyy-MM-dd 'at' HH:mm:ss");
        String printData =
                "[C]<u><b><font size='small'>" + dairyName + "</b></u> \n" +
                        "[C]<font size='small'>" + format.format(new Date()) + "</font>\n" +
                        "[C]<b>" + dairyContact + "</b>\n" +
                        "[C]<b>" + context.getResources().getString(R.string.advance_payment).toUpperCase() + "</b>\n" +
                        "[C]=================\n" +
//                        "[L]<b><font size='small'>" + context.getResources().getString(R.string.shift) + " :- </b> " + shift + " \n" +
//                        "[L]<b><font size='small'>" + context.getResources().getString(R.string.milk) + " :- </b> " + milk + " \n" +
                        "[C]=============================\n" +
                        "[L]<b><font size='small'>" +
                        context.getResources().getString(R.string.date_d).toUpperCase() + "  " +
                        context.getResources().getString(R.string.member_id).toUpperCase() + "  " +
                        "[R]" + context.getResources().getString(R.string.amount).toUpperCase() +
                        "</b>  \n" +
                        "[C]=============================\n";

        String producerList = "";
        float totalAmount = 0f;
        for (int i = 0; i < resultList.size(); i++) {
            Result member = resultList.get(i);
            String date = "01";
            String memberCode = "01";
            String amount = "125.00";

            if (!dummyPrint) {
                date = Utils.onlyPrintDate(member.getPaymentDate());
                memberCode = member.getCustomer().getMemberCode();
                amount = member.getAmount();
            }

            totalAmount += Float.parseFloat(amount);
            producerList = producerList +
                    date + "    " +
                    memberCode + "   " +
                    "[R]" + amount + " </b>  \n";
        }

        printData = printData + producerList;
        printData += "[C]===============================\n" +
//                "[L]<b><font size='small'>" + context.getResources().getString(R.string.deduction).toUpperCase() + ":  </b>" + "0" + "\n" +
                "[L]<b><font size='small'>" + context.getResources().getString(R.string.totalAmount).toUpperCase() + ":  </b>" + totalAmount + "\n";

        return printData;
    }

    public String getLedgerSlip(List<Result> resultList, String startDate, String endDate) {

        SimpleDateFormat format = new SimpleDateFormat("'on' yyyy-MM-dd 'at' HH:mm:ss");
        String printData =
                "<u><b><font size='normal'> " + context.getResources().getString(R.string.farm) + "</b></u> \n" +
                        "<b>" + context.getResources().getString(R.string.producerLedger) + "</b>\n" +
                        "<font size='normal'>" + startDate + "To" + endDate + "</font>\n" +
                        "[C]<b>" + Constants.MOBILE_NUMBER + "</b>\n" +
                        "[C]---------------------------------\n" +
                        "[L]<b><font size='small'>" +
                        context.getResources().getString(R.string.date_d).toUpperCase() + "  " +
                        context.getResources().getString(R.string.shift_s).toUpperCase() + "  " +
                        context.getResources().getString(R.string.liter).toUpperCase() + "  " +
                        context.getResources().getString(R.string.fat).toUpperCase() + "  " +
                        context.getResources().getString(R.string.snf).toUpperCase() + "  " +
                        "[R]" + context.getResources().getString(R.string.amount).toUpperCase() +
                        "</b>  \n" +
                        "[C]----------------------------------\n";

        String producerList = "";
        float totalAmount = 0f;
        float totalLiter = 0f;
        for (int i = 0; i < resultList.size(); i++) {
            Result member = new Result();
            String date = "01";
            String shift = "E";
            String liter = "3.0";
            String fat = "6.6";
            String snf = "7.0";
            String amount = "123.00";

            if (!dummyPrint) {
                date = Utils.onlyPrintDate(member.getAddDate());
                shift = member.getTimeslot().toUpperCase().equals(Constants.MORNING.toUpperCase()) ? "M" : "E";
                liter = member.getLiter();
                fat = member.getFat();
                snf = member.getSnf();
                amount = member.getAmount();

            }


            totalAmount += Float.parseFloat(amount);
            totalLiter += Float.parseFloat(liter);
            producerList = producerList +
                    date + " " +
                    shift + "   " +
                    liter + "   " +
                    fat + "   " +
                    snf + "  " +
                    "[R]" + amount + " </b>  \n";
        }

        producerList = producerList +
                resultList.size() + " " +
                "  " + "   " +
                totalLiter + "   " +
                "  " + "   " +
                "  " + "  " +
                "[R]" + totalAmount + " </b>  \n";
        printData = printData + producerList;
        printData += "[C]----------------------------------\n" +
                "[L]<b><font size='small'>" + context.getResources().getString(R.string.deduction).toUpperCase() + ":  </b>" + "0" + "\n" +
                "[L]<b><font size='small'>" + context.getResources().getString(R.string.totalAmount).toUpperCase() + ":  </b>" + totalAmount + "\n";

        return printData;
    }

    public String getProducerSlip(List<Result> resultList) {

        SimpleDateFormat format = new SimpleDateFormat("'on' yyyy-MM-dd 'at' HH:mm:ss");
        String printData =
                "[C]<u><b><font size='small'>" + dairyName + "</b></u> \n" +
                        "[C]<font size='small'>" + format.format(new Date()) + "</font>\n" +
                        "[C]<b>" + dairyContact + "</b>\n" +
                        "[C]<b>" + context.getResources().getString(R.string.producerName) + "</b>\n" +
                        "[C]=============================\n" +
                        "[L]<b><font size='small'>" + context.getResources().getString(R.string.code) + " " + context.getResources().getString(R.string.user_type).toUpperCase() + "[C]" + context.getResources().getString(R.string.producerName).toUpperCase() + " </b>  \n" +
                        "[C]=============================\n";

        String producerList = "";
        for (int i = 0; i < resultList.size(); i++) {
            Result member = resultList.get(i);
            String userCode = member.getMemberCode().length() > 3 ? member.getMemberCode().substring(0, 3) : member.getMemberCode();
            String userName = member.getCustomerName().length() > 15 ? member.getCustomerName().toUpperCase().substring(0, 15) : member.getCustomerName();
            String userType = member.getMemberType().toUpperCase().equals(Constants.ALL) ? "Bo" :
                    member.getMemberType().toUpperCase().equals(Constants.SELLER) ? "S" :
                            member.getMemberType().toUpperCase().equals(Constants.BUYER) ? "B" : "-";
            producerList = producerList + "[L]<b><font size='small'>" + " " + userCode + "     " + userType + "[C]" + userName + " </b>  \n";
        }

        String TotalMember = String.valueOf(resultList.size());
        printData = printData + producerList;
        printData += "[C]===============================\n" +
                "[L]<b><font size='small'>" + context.getResources().getString(R.string.totalProducer).toUpperCase() + "[C]" + TotalMember + " </b>  \n";

        return printData;
    }

    public String getMilkAdvancePaymentSlip(Result result) {

        String shift = "EVENING";
        String customerNumber = "11";
        String customerName = "GULAB PATALIYA";
        String paymentDate = Utils.changeResponseDateFormat(String.valueOf(new Date()));
        String totalAmount = "580";

        if (!dummyPrint) {
            shift = result.getTimeslot().toUpperCase();
            customerNumber = result.getCustomer().getMemberCode();
            customerName = result.getCustomer().getCustomerName();
            paymentDate = result.getPaymentDate();
            totalAmount = result.getAmount();
        }


        SimpleDateFormat format = new SimpleDateFormat("'on' yyyy-MM-dd 'at' HH:mm:ss");
        String printData =
                "[C]<u><b><font size='small'>" + dairyName + "</b></u> \n" +
                        "[C]<font size='small'>" + format.format(new Date()) + "</font>\n" +
                        "[C]<b>" + dairyContact + "</b>\n" +
                        "[C]=================\n" +
                        "[L]<b><font size='small'>" + context.getResources().getString(R.string.shift) + " :- </b> " + shift + " \n" +
                        "[L]<b><font size='small'>" + context.getResources().getString(R.string.paymentdate) + " :- </b> " + paymentDate + " \n" +
                        "[L]<b><font size='small'>" + context.getResources().getString(R.string.customer_number) + " :- </b> " + customerNumber + " \n" +
                        "[L]<b><font size='small'>" + context.getResources().getString(R.string.customer_name) + " :- </b> " + customerName + " \n" +
                        "[C]=================\n" +
                        "[L]<b>" + context.getResources().getString(R.string.total) + " :- </b>[R]" + totalAmount + " \n";

        return printData;
    }

    public String getMilkSellSlip(Result result) {

        String shift = "EVENING";
        String milk = "BUFFALO";
        String customerNumber = "11";
        String customerName = "GULAB PATALIYA";

        String qty = "10";
        String literAmount = "58";
        String totalAmount = "580";

        if (!dummyPrint) {
            shift = result.getTimeslot().toUpperCase();
            milk = result.getAnimalType().toUpperCase();
            customerNumber = result.getCustomer().getMemberCode();
            customerName = result.getCustomer().getCustomerName();

            qty = result.getLiter();
            literAmount = result.getRate();
            totalAmount = result.getTotalAmount();
        }


        SimpleDateFormat format = new SimpleDateFormat("'on' yyyy-MM-dd 'at' HH:mm:ss");
        String printData =
                "[C]<u><b><font size='small'>" + dairyName + "</b></u> \n" +
                        "[C]<font size='small'>" + format.format(new Date()) + "</font>\n" +
                        "[C]<b>" + dairyContact + "</b>\n" +
                        "[C]=================\n" +
                        "[L]<b><font size='small'>" + context.getResources().getString(R.string.shift) + " :- </b> " + shift + " \n" +
                        "[L]<b><font size='small'>" + context.getResources().getString(R.string.milk) + " :- </b> " + milk + " \n" +
                        "[L]<b><font size='small'>" + context.getResources().getString(R.string.customer_number) + " :- </b> " + customerNumber + " \n" +
                        "[L]<b><font size='small'>" + context.getResources().getString(R.string.customer_name) + " :- </b> " + customerName + " \n" +
                        "[C]=================\n" +
                        "[L]<b>" + context.getResources().getString(R.string.qty) + " :- </b>[R]" + qty + " \n" +
                        "[L]<b>" + context.getResources().getString(R.string.liter_rate).toUpperCase() + "% :- </b>[R]" + literAmount + " \n" +
                        "[L]<b>" + context.getResources().getString(R.string.total) + " :- </b>[R]" + totalAmount + " \n" +
                        "[C]**" + context.getResources().getString(R.string.thank_you) + "**\n";

        return printData;
    }

    public String getMilkBuySlip(Result result) {

        String shift = "EVENING";
        String milk = "BUFFALO";
        String customerNumber = "11";
        String customerName = "GULAB PATALIYA";

        String qty = "10";
        String fat = "8.5";
        String snf = "9.0";
        String clr = "26.6";
        String watter = "10.55";
        String total = "620.00";

        String fatPD = "8.5";
        String literPD = "9.0";
        String govtPD = "2.00";
        String totalWithPD = "940.55";
        String rateLiter = "60.00";
        String rateWatterPDLiter = "90.00";


        if (!dummyPrint) {
            shift = result.getTimeslot().toUpperCase();
            milk = result.getAnimalType().toUpperCase();
            customerNumber = result.getCustomer().getMemberCode();
            customerName = result.getCustomer().getCustomerName();

            qty = result.getLiter();
            fat = result.getFat();
            snf = result.getSnf();
            clr = "0.00";
            watter = "0.00";
            total = result.getAmount();

            fatPD = result.getFatPd();
            literPD = result.getLiterPd();
            govtPD = result.getGovPd();
            totalWithPD = "0.00";
            rateLiter = result.getRate();
            rateWatterPDLiter = "0.00";

            if (Utils.isStringInteger(result.getLiter())) {
                Debug.e(TAG, "Liter Rate =--->" + String.valueOf((float) Integer.parseInt(result.getLiter())));
//            result.setTotalRateWithPd(result.getTotalAmount());
                if (result.getTotalRateWithPd() != null && Utils.isNotEmpty(result.getTotalRateWithPd())) {
                    rateWatterPDLiter = String.valueOf(Float.parseFloat(result.getTotalRateWithPd()) / (float) Integer.parseInt(result.getLiter()));
                    totalWithPD = result.getTotalRateWithPd();
                }
            }
        }


        SimpleDateFormat format = new SimpleDateFormat("'on' yyyy-MM-dd 'at' HH:mm:ss");
        String printData =
                "[C]<u><b><font size='small'>" + dairyName + "</b></u> \n" +
                        "[C]<font size='small'>" + format.format(new Date()) + "</font>\n" +
                        "[C]<b>" + dairyContact + "</b>\n" +
                        "[C]=================\n" +
                        "[L]<b><font size='small'>" + context.getResources().getString(R.string.shift) + " :- </b> " + shift + " \n" +
                        "[L]<b><font size='small'>" + context.getResources().getString(R.string.milk) + " :- </b> " + milk + " \n" +
                        "[L]<b><font size='small'>" + context.getResources().getString(R.string.customer_number) + " :- </b> " + customerNumber + " \n" +
                        "[L]<b><font size='small'>" + context.getResources().getString(R.string.customer_name) + " :- </b> " + customerName + " \n" +
                        "[C]=================\n" +
                        "[L]<b>" + context.getResources().getString(R.string.qty) + " :- </b>[R]" + qty + " \n" +
                        (PreferenceManager.getValueBoolean(Constants.SNF_IN_SLIP) ? "[L]<b>" + context.getResources().getString(R.string.fat) + "% :- </b>[R]" + fat + " \n" : "") +
                        (PreferenceManager.getValueBoolean(Constants.SNF_IN_SLIP) ? "[L]<b>" + context.getResources().getString(R.string.snf) + "% :- </b>[R]" + snf + " \n" : "") +
                        "[L]<b>" + context.getResources().getString(R.string.snf_clr) + "% :- </b>[R]" + clr + " \n" +
                        "[L]<b>" + context.getResources().getString(R.string.total) + " :- </b>[R]" + total + " \n" +
                        (PreferenceManager.getValueBoolean(Constants.WATER_IN_SLIP) ? "[L]<b>" + context.getResources().getString(R.string.water) + " :- </b>[R]" + watter + " \n" : "") +
                        (PreferenceManager.getValueBoolean(Constants.FAT_PD) ? "[L]<b>" + context.getResources().getString(R.string.fat_pd) + " :- </b>[R]" + fatPD + " \n" : "") +
                        (PreferenceManager.getValueBoolean(Constants.LITER_PD) ? "[L]<b>" + context.getResources().getString(R.string.liter_pd) + " :- </b>[R]" + literPD + " \n" : "") +
                        (PreferenceManager.getValueBoolean(Constants.GOVT_PD) ? "[L]<b>" + context.getResources().getString(R.string.govt_pd) + " :- </b>[R]" + govtPD + " \n" : "") +
                        "[L]<b>" + context.getResources().getString(R.string.total_rate_with_pd).toUpperCase() + " :- </b>[R]" + totalWithPD + " \n" +
                        (PreferenceManager.getValueBoolean(Constants.PER_LITER_RATE_IN_SLIP) ? "[L]<b>" + context.getResources().getString(R.string.rate_liter) + " :- </b>[R]" + rateLiter + " \n" : "") +
                        (PreferenceManager.getValueBoolean(Constants.PER_LITER_PD_IN_SLIP) ? "[L]<b>" + context.getResources().getString(R.string.rate_w_pd_liter) + " :- </b>[R]" + rateWatterPDLiter : "") +
                        "\n" + "[C]=================\n" +
                        "[L]<b>" + PreferenceManager.getValue(Constants.SLIP_MESSAGE) + "</b>" +
                        " \n";

        return printData;
    }

    public String getBitmapSlip(Bitmap result) {
        AsyncEscPosPrinter printer = new AsyncEscPosPrinter(null, 203, 48f, 32);
        String printData = PrinterTextParserImg.bitmapToHexadecimalString(printer, result);
        return "<img>" + printData + "</img>";
    }


    @SuppressLint("SimpleDateFormat")
    public AsyncEscPosPrinter getAsyncEscPosPrinter(String printData) {

        AsyncEscPosPrinter printer = new AsyncEscPosPrinter(null, 203, 48f, 30);
        return printer.setTextToPrint(printData);
    }
}
