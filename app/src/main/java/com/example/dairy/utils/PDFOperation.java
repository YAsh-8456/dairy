package com.example.dairy.utils;

import static com.example.dairy.permission.PermissionsActivity.PERMISSION_REQUEST_CODE;
import static com.example.dairy.permission.PermissionsChecker.REQUIRED_PERMISSION;
import static com.example.dairy.utils.LogUtils.LOGE;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import com.example.dairy.modal.Result;
import com.example.dairy.permission.PermissionsActivity;
import com.example.dairy.permission.PermissionsChecker;
import com.itextpdf.kernel.colors.Color;
import com.itextpdf.kernel.colors.DeviceRgb;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfDocumentInfo;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.kernel.pdf.canvas.draw.DottedLine;
import com.itextpdf.kernel.pdf.canvas.draw.SolidLine;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.borders.Border;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.LineSeparator;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.element.Text;
import com.itextpdf.layout.font.FontProvider;
import com.itextpdf.layout.font.FontSet;
import com.itextpdf.layout.property.TextAlignment;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class PDFOperation {

    Activity mContext;
    PermissionsChecker checker;
    private static final boolean dummyPrint = true;

    public PDFOperation(Activity context) {
        this.mContext = context;
        checker = new PermissionsChecker(context);
    }


    public void createPdfMemberList(boolean isPrinted, List<Result> masterList) {

        Long tsLong = System.currentTimeMillis() / 1000;
        String ts = tsLong.toString();
        String dest;

        if (!isPrinted) {
            dest = FileUtils.getAppPath(mContext) + "memberList_" + ts + ".pdf";
        } else {
            dest = FileUtils.getAppPath(mContext) + "temp" + ".pdf";
        }


        if (new File(dest).exists()) {
            new File(dest).delete();
        }

        try {
            /**
             * Creating Document
             */
            PdfWriter pdfWriter = new PdfWriter(new FileOutputStream(dest));
            PdfDocument pdfDocument = new PdfDocument(pdfWriter);
            PdfDocumentInfo info = pdfDocument.getDocumentInfo();

            info.setTitle("Example of iText7 by Pratik Butani");
            info.setAuthor("Pratik Butani");
            info.setSubject("iText7 PDF Demo");
            info.setKeywords("iText, PDF, Pratik Butani");
            info.setCreator("A simple tutorial example");

            Document document = new Document(pdfDocument, PageSize.A4, true);

            /***
             * Variables for further use....
             */
            Color mColorAccent = new DeviceRgb(153, 204, 255);
            Color mColorBlack = new DeviceRgb(0, 0, 0);
            float mHeadingFontSize = 20.0f;
            float mValueFontSize = 26.0f;

            /**
             * How to USE FONT....
             */
            PdfFont font = PdfFontFactory.createFont("assets/fonts/brandon_medium.otf", "UTF-8", true);

            final FontSet set = new FontSet();
            set.addFont("assets/fonts/brandon_medium.otf");
            set.addFont("assets/fonts/noto_sans_tamil_regular.ttf");
            document.setFontProvider(new FontProvider(set));
            // LINE SEPARATOR
            LineSeparator lineSeparator = new LineSeparator(new SolidLine());
            lineSeparator.setStrokeColor(new DeviceRgb(0, 0, 68));

            LineSeparator dotLineSeparator = new LineSeparator(new DottedLine());
            lineSeparator.setStrokeColor(new DeviceRgb(0, 0, 68));

            // Title Order Details...
            // Adding Title....


            Text mOrderDetailsTitleChunk = new Text("Milk Producer Co. Pvt. Ltd").setFont(font).setFontSize(20.0f).setFontColor(mColorBlack);
            Paragraph mOrderDetailsTitleParagraph = new Paragraph(mOrderDetailsTitleChunk)
                    .setTextAlignment(TextAlignment.CENTER);
            document.add(mOrderDetailsTitleParagraph);

            Text memberList = new Text("MemberList").setFont(font).setFontSize(16.0f).setFontColor(mColorBlack);
            Paragraph memberListParagraph = new Paragraph(memberList)
                    .setTextAlignment(TextAlignment.CENTER);
            document.add(memberListParagraph);

            Text date = new Text(new Date().toString()).setFont(font).setFontSize(14.0f).setFontColor(mColorBlack);
            Paragraph dateParagraph = new Paragraph(date)
                    .setTextAlignment(TextAlignment.CENTER);
            document.add(dateParagraph);


            float[] pointColumnWidths = {30F, 30F, 50F, 90F, 60F, 90F, 40F, 50F, 70F};
            Table table = new Table(pointColumnWidths);

            document.add(new Paragraph(""));
            document.add(dotLineSeparator);

            // Adding cells to the table
            table.addCell(setTableHeader("Sr. No."));
            table.addCell(setTableHeader("Code"));
            table.addCell(setTableHeader("Type"));
            table.addCell(setTableHeader("Name"));
            table.addCell(setTableHeader("Contact No."));
            table.addCell(setTableHeader("Address"));
            table.addCell(setTableHeader("Bank\nName"));
            table.addCell(setTableHeader("Account No."));
            table.addCell(setTableHeader("Join Date"));

            document.add(table);
            document.add(dotLineSeparator);
            document.add(new Paragraph(""));


            for (int i = 0; i < masterList.size(); i++) {

                Result memberData = masterList.get(i);
                Table tableData = new Table(pointColumnWidths);

                if (dummyPrint) {
                    // Adding cells to the table
                    tableData.addCell(setTableData("11"));
                    tableData.addCell(setTableData("01"));
                    tableData.addCell(setTableData("Buyer"));
                    tableData.addCell(setTableData("MoHan"));
                    tableData.addCell(setTableData("9879865412"));
                    tableData.addCell(setTableData("Surat"));
                    tableData.addCell(setTableData("SBI"));
                    tableData.addCell(setTableData("3554856897404"));
                    tableData.addCell(setTableData("20-06-2020"));
                    document.add(tableData);

                } else {
                    // Adding cells to the table
                    tableData.addCell(setTableData("11"));
                    tableData.addCell(setTableData(memberData.getMemberCode()));
                    tableData.addCell(setTableData(memberData.getMemberType().toUpperCase()));
                    tableData.addCell(setTableData(memberData.getCustomerName().toUpperCase()));
                    tableData.addCell(setTableData(memberData.getPhoneNumber().toUpperCase()));
                    tableData.addCell(setTableData(memberData.getAddress().toUpperCase()));
                    tableData.addCell(setTableData(memberData.getBankName().toUpperCase()));
                    tableData.addCell(setTableData(memberData.getAccountNumber().toUpperCase()));
                    tableData.addCell(setTableData("20-06-2020"));
                    document.add(tableData);
                }


            }
            document.add(new Paragraph(""));
            document.add(dotLineSeparator);

            document.close();

            if (isPrinted) {
                openPDF(dest);
            } else {
                Toast.makeText(mContext, "Created... :)", Toast.LENGTH_SHORT).show();

            }


        } catch (IOException e) {
            LOGE("createPdf: Error " + e.getLocalizedMessage());
        } catch (ActivityNotFoundException ae) {
            Toast.makeText(mContext, "No application found to open this file.", Toast.LENGTH_SHORT).show();
        }
    }


    public void createPdfForPurchaseList(boolean isPrinted, List<Result> purchaseList) {

        if (!checkPermissionGranted()) {
            return;
        }

        Long tsLong = System.currentTimeMillis() / 1000;
        String ts = tsLong.toString();
        String dest;

        if (!isPrinted) {
            dest = FileUtils.getAppPath(mContext) + "purchase_" + ts + ".pdf";
        } else {
            dest = FileUtils.getAppPath(mContext) + "temp" + ".pdf";
        }

        if (new File(dest).exists()) {
            new File(dest).delete();
        }

        try {
            /**
             * Creating Document
             */
            PdfWriter pdfWriter = new PdfWriter(new FileOutputStream(dest));
            PdfDocument pdfDocument = new PdfDocument(pdfWriter);
            PdfDocumentInfo info = pdfDocument.getDocumentInfo();

            info.setTitle("Example of iText7 by Pratik Butani");
            info.setAuthor("Pratik Butani");
            info.setSubject("iText7 PDF Demo");
            info.setKeywords("iText, PDF, Pratik Butani");
            info.setCreator("A simple tutorial example");

            Document document = new Document(pdfDocument, PageSize.A4, true);

            /***
             * Variables for further use....
             */
            Color mColorAccent = new DeviceRgb(153, 204, 255);
            Color mColorBlack = new DeviceRgb(0, 0, 0);
            float mHeadingFontSize = 20.0f;
            float mValueFontSize = 26.0f;

            /**
             * How to USE FONT....
             */
            PdfFont font = PdfFontFactory.createFont("assets/fonts/brandon_medium.otf", "UTF-8", true);

            final FontSet set = new FontSet();
            set.addFont("assets/fonts/brandon_medium.otf");
            set.addFont("assets/fonts/noto_sans_tamil_regular.ttf");
            document.setFontProvider(new FontProvider(set));
            // LINE SEPARATOR
            LineSeparator lineSeparator = new LineSeparator(new SolidLine());
            lineSeparator.setStrokeColor(new DeviceRgb(0, 0, 68));

            LineSeparator dotLineSeparator = new LineSeparator(new DottedLine());
            lineSeparator.setStrokeColor(new DeviceRgb(0, 0, 68));

            // Title Order Details...
            // Adding Title....


            Text mOrderDetailsTitleChunk = new Text("Milk Producer Co. Pvt. Ltd").setFont(font).setFontSize(20.0f).setFontColor(mColorBlack);
            Paragraph mOrderDetailsTitleParagraph = new Paragraph(mOrderDetailsTitleChunk)
                    .setTextAlignment(TextAlignment.CENTER);
            document.add(mOrderDetailsTitleParagraph);

            Text memberList = new Text("PurchaseList").setFont(font).setFontSize(16.0f).setFontColor(mColorBlack);
            Paragraph memberListParagraph = new Paragraph(memberList)
                    .setTextAlignment(TextAlignment.CENTER);
            document.add(memberListParagraph);


            String date = "";
            SimpleDateFormat spf = new SimpleDateFormat("dd-MM-yyyy");
            date = spf.format(new Date());
            String timeSlot = "Morning";
            String animalName = "Buffalo-Cow";
            String timeInformationData = "Date:-" + date + "  " + "Shift:-" + timeSlot + "  " + "Milk:-" + animalName;

            Text timeInformation = new Text(timeInformationData).setFont(font).setFontSize(14.0f).setFontColor(mColorBlack);
            Paragraph dateParagraph = new Paragraph(timeInformation)
                    .setTextAlignment(TextAlignment.CENTER);
            document.add(dateParagraph);


            float[] pointColumnWidths = {30F, 30F, 130F, 70F, 50F, 30F, 30F, 30F, 50F, 50F};
            Table table = new Table(pointColumnWidths);

            document.add(new Paragraph(""));
            document.add(dotLineSeparator);

            // Adding cells to the table
            table.addCell(setTableHeader("Sr. No."));
            table.addCell(setTableHeader("Code"));
            table.addCell(setTableHeader("Name"));
            table.addCell(setTableHeader("Date"));
            table.addCell(setTableHeader("Animal\nType"));
            table.addCell(setTableHeader("Liter"));
            table.addCell(setTableHeader("fat"));
            table.addCell(setTableHeader("snf"));
            table.addCell(setTableHeader("Liter Rate"));
            table.addCell(setTableHeader("Amount"));

            document.add(table);
            document.add(dotLineSeparator);
            document.add(new Paragraph(""));


            for (int i = 0; i < purchaseList.size(); i++) {

                Table tableData = new Table(pointColumnWidths);
                Result purchaseRecord = purchaseList.get(i);

                if (dummyPrint) {
                    // Adding cells to the table
                    tableData.addCell(setTableData("" + "01"));
                    tableData.addCell(setTableData("011"));
                    tableData.addCell(setTableData("Ram"));
                    tableData.addCell(setTableData("12-12-2021"));
                    tableData.addCell(setTableData("Cow"));
                    tableData.addCell(setTableData("5"));
                    tableData.addCell(setTableData("5.5"));
                    tableData.addCell(setTableData("6.7"));
                    tableData.addCell(setTableData("33"));
                    tableData.addCell(setTableData("165"));
                    document.add(tableData);
                } else {
                    // Adding cells to the table
                    tableData.addCell(setTableData("" + purchaseRecord.getId()));
                    tableData.addCell(setTableData(purchaseRecord.getCustomer().getMemberCode()));
                    tableData.addCell(setTableData(purchaseRecord.getCustomer().getCustomerName()));
                    tableData.addCell(setTableData(purchaseRecord.getAddDate()));
                    tableData.addCell(setTableData(purchaseRecord.getAnimalType()));
                    tableData.addCell(setTableData(purchaseRecord.getLiter()));
                    tableData.addCell(setTableData(purchaseRecord.getFat()));
                    tableData.addCell(setTableData(purchaseRecord.getSnf()));
                    tableData.addCell(setTableData(purchaseRecord.getRate()));
                    tableData.addCell(setTableData(purchaseRecord.getAmount()));
                    document.add(tableData);
                }


            }
            document.add(new Paragraph(""));
            document.add(dotLineSeparator);

            document.close();

            if (isPrinted) {
                openPDF(dest);
            } else {
                Toast.makeText(mContext, "Created... :)", Toast.LENGTH_SHORT).show();
            }


        } catch (IOException e) {
            LOGE("createPdf: Error " + e.getLocalizedMessage());
        } catch (ActivityNotFoundException ae) {
            Toast.makeText(mContext, "No application found to open this file.", Toast.LENGTH_SHORT).show();
        }
    }

    public void createPdfForLagerList(boolean isPrinted, List<Result> purchaseList) {

        if (!checkPermissionGranted()) {
            return;
        }

        Long tsLong = System.currentTimeMillis() / 1000;
        String ts = tsLong.toString();
        String dest;

        if (!isPrinted) {
            dest = FileUtils.getAppPath(mContext) + "localSale_" + ts + ".pdf";
        } else {
            dest = FileUtils.getAppPath(mContext) + "temp" + ".pdf";
        }

        if (new File(dest).exists()) {
            new File(dest).delete();
        }

        try {
            /**
             * Creating Document
             */
            PdfWriter pdfWriter = new PdfWriter(new FileOutputStream(dest));
            PdfDocument pdfDocument = new PdfDocument(pdfWriter);
            PdfDocumentInfo info = pdfDocument.getDocumentInfo();

            info.setTitle("Example of iText7 by Pratik Butani");
            info.setAuthor("Pratik Butani");
            info.setSubject("iText7 PDF Demo");
            info.setKeywords("iText, PDF, Pratik Butani");
            info.setCreator("A simple tutorial example");

            Document document = new Document(pdfDocument, PageSize.A4, true);

            /***
             * Variables for further use....
             */
            Color mColorAccent = new DeviceRgb(153, 204, 255);
            Color mColorBlack = new DeviceRgb(0, 0, 0);
            float mHeadingFontSize = 20.0f;
            float mValueFontSize = 26.0f;

            /**
             * How to USE FONT....
             */
            PdfFont font = PdfFontFactory.createFont("assets/fonts/brandon_medium.otf", "UTF-8", true);

            final FontSet set = new FontSet();
            set.addFont("assets/fonts/brandon_medium.otf");
            set.addFont("assets/fonts/noto_sans_tamil_regular.ttf");
            document.setFontProvider(new FontProvider(set));
            // LINE SEPARATOR
            LineSeparator lineSeparator = new LineSeparator(new SolidLine());
            lineSeparator.setStrokeColor(new DeviceRgb(0, 0, 68));

            LineSeparator dotLineSeparator = new LineSeparator(new DottedLine());
            lineSeparator.setStrokeColor(new DeviceRgb(0, 0, 68));

            // Title Order Details...
            // Adding Title....


            Text mOrderDetailsTitleChunk = new Text("Milk Producer Co. Pvt. Ltd").setFont(font).setFontSize(20.0f).setFontColor(mColorBlack);
            Paragraph mOrderDetailsTitleParagraph = new Paragraph(mOrderDetailsTitleChunk)
                    .setTextAlignment(TextAlignment.CENTER);
            document.add(mOrderDetailsTitleParagraph);

            Text memberList = new Text("LocalSell").setFont(font).setFontSize(16.0f).setFontColor(mColorBlack);
            Paragraph memberListParagraph = new Paragraph(memberList)
                    .setTextAlignment(TextAlignment.CENTER);
            document.add(memberListParagraph);


            String date = "";
            SimpleDateFormat spf = new SimpleDateFormat("dd-MM-yyyy");
            date = spf.format(new Date());
            String timeSlot = "Morning";
            String animalName = "Buffalo-Cow";
            String timeInformationData = "Date:-" + date + "  " + "Shift:-" + timeSlot + "  " + "Milk:-" + animalName;

            Text timeInformation = new Text(timeInformationData).setFont(font).setFontSize(14.0f).setFontColor(mColorBlack);
            Paragraph dateParagraph = new Paragraph(timeInformation)
                    .setTextAlignment(TextAlignment.CENTER);
            document.add(dateParagraph);


            float[] pointColumnWidths = {30F, 30F, 150F, 50F, 50F, 30F, 30F, 30F, 50F, 50F};
            Table table = new Table(pointColumnWidths);

            document.add(new Paragraph(""));
            document.add(dotLineSeparator);

            // Adding cells to the table
            table.addCell(setTableHeader("Sr. No."));
            table.addCell(setTableHeader("Code"));
            table.addCell(setTableHeader("Name"));
            table.addCell(setTableHeader("Date"));
            table.addCell(setTableHeader("Animal\nType"));
            table.addCell(setTableHeader("Liter"));
            table.addCell(setTableHeader("fat"));
            table.addCell(setTableHeader("snf"));
            table.addCell(setTableHeader("Liter Rate"));
            table.addCell(setTableHeader("Amount"));

            document.add(table);
            document.add(dotLineSeparator);
            document.add(new Paragraph(""));


            for (int i = 0; i < purchaseList.size(); i++) {

                Table tableData = new Table(pointColumnWidths);

                Result purchaseRecord = purchaseList.get(i);

                if (dummyPrint) {
//                                  Adding cells to the table
                    tableData.addCell(setTableData("01"));
                    tableData.addCell(setTableData("11"));
                    tableData.addCell(setTableData("PrakashBhai"));
                    tableData.addCell(setTableData("31-01-21"));
                    tableData.addCell(setTableData("Cow"));
                    tableData.addCell(setTableData("5"));
                    tableData.addCell(setTableData("8.5"));
                    tableData.addCell(setTableData("6.5"));
                    tableData.addCell(setTableData("56"));
                    tableData.addCell(setTableData("280"));

                } else {
                    // Adding cells to the table
                    tableData.addCell(setTableData("" + purchaseRecord.getId()));
                    tableData.addCell(setTableData(purchaseRecord.getCustomer().getMemberCode()));
                    tableData.addCell(setTableData(purchaseRecord.getCustomer().getCustomerName()));
                    tableData.addCell(setTableData(purchaseRecord.getAddDate()));
                    tableData.addCell(setTableData(purchaseRecord.getAnimalType()));
                    tableData.addCell(setTableData(purchaseRecord.getLiter()));
                    tableData.addCell(setTableData(purchaseRecord.getFat()));
                    tableData.addCell(setTableData(purchaseRecord.getSnf()));
                    tableData.addCell(setTableData(purchaseRecord.getRate()));
                    tableData.addCell(setTableData(purchaseRecord.getAmount()));
                }
                document.add(tableData);


            }
            document.add(new Paragraph(""));
            document.add(dotLineSeparator);

            document.close();

            if (isPrinted) {
                openPDF(dest);
            } else {
                Toast.makeText(mContext, "Created... :)", Toast.LENGTH_SHORT).show();
            }


        } catch (IOException e) {
            LOGE("createPdf: Error " + e.getLocalizedMessage());
        } catch (ActivityNotFoundException ae) {
            Toast.makeText(mContext, "No application found to open this file.", Toast.LENGTH_SHORT).show();
        }
    }


    public void createPdfForLocalSellList(boolean isPrinted, List<Result> list) {

        if (!checkPermissionGranted()) {
            return;
        }

        Long tsLong = System.currentTimeMillis() / 1000;
        String ts = tsLong.toString();
        String dest;

        if (!isPrinted) {
            dest = FileUtils.getAppPath(mContext) + "localSale_" + ts + ".pdf";
        } else {
            dest = FileUtils.getAppPath(mContext) + "temp" + ".pdf";
        }

        if (new File(dest).exists()) {
            new File(dest).delete();
        }

        try {
            /**
             * Creating Document
             */
            PdfWriter pdfWriter = new PdfWriter(new FileOutputStream(dest));
            PdfDocument pdfDocument = new PdfDocument(pdfWriter);
            PdfDocumentInfo info = pdfDocument.getDocumentInfo();

            info.setTitle("Example of iText7 by Pratik Butani");
            info.setAuthor("Pratik Butani");
            info.setSubject("iText7 PDF Demo");
            info.setKeywords("iText, PDF, Pratik Butani");
            info.setCreator("A simple tutorial example");

            Document document = new Document(pdfDocument, PageSize.A4, true);

            /***
             * Variables for further use....
             */
            Color mColorAccent = new DeviceRgb(153, 204, 255);
            Color mColorBlack = new DeviceRgb(0, 0, 0);
            float mHeadingFontSize = 20.0f;
            float mValueFontSize = 26.0f;

            /**
             * How to USE FONT....
             */
            PdfFont font = PdfFontFactory.createFont("assets/fonts/brandon_medium.otf", "UTF-8", true);

            final FontSet set = new FontSet();
            set.addFont("assets/fonts/brandon_medium.otf");
            set.addFont("assets/fonts/noto_sans_tamil_regular.ttf");
            document.setFontProvider(new FontProvider(set));
            // LINE SEPARATOR
            LineSeparator lineSeparator = new LineSeparator(new SolidLine());
            lineSeparator.setStrokeColor(new DeviceRgb(0, 0, 68));

            LineSeparator dotLineSeparator = new LineSeparator(new DottedLine());
            lineSeparator.setStrokeColor(new DeviceRgb(0, 0, 68));

            // Title Order Details...
            // Adding Title....


            Text mOrderDetailsTitleChunk = new Text("Milk Producer Co. Pvt. Ltd").setFont(font).setFontSize(20.0f).setFontColor(mColorBlack);
            Paragraph mOrderDetailsTitleParagraph = new Paragraph(mOrderDetailsTitleChunk)
                    .setTextAlignment(TextAlignment.CENTER);
            document.add(mOrderDetailsTitleParagraph);

            Text memberList = new Text("LocalSell").setFont(font).setFontSize(16.0f).setFontColor(mColorBlack);
            Paragraph memberListParagraph = new Paragraph(memberList)
                    .setTextAlignment(TextAlignment.CENTER);
            document.add(memberListParagraph);


            String date = "";
            SimpleDateFormat spf = new SimpleDateFormat("dd-MM-yyyy");
            date = spf.format(new Date());
            String timeSlot = "Morning";
            String animalName = "Buffalo-Cow";
            String timeInformationData = "Date:-" + date + "  " + "Shift:-" + timeSlot + "  " + "Milk:-" + animalName;

            Text timeInformation = new Text(timeInformationData).setFont(font).setFontSize(14.0f).setFontColor(mColorBlack);
            Paragraph dateParagraph = new Paragraph(timeInformation)
                    .setTextAlignment(TextAlignment.CENTER);
            document.add(dateParagraph);


            float[] pointColumnWidths = {50F, 50F, 130F, 70F, 70F, 50F, 50F};
            Table table = new Table(pointColumnWidths);

            document.add(new Paragraph(""));
            document.add(dotLineSeparator);

            // Adding cells to the table
            table.addCell(setTableHeader("Sr. No."));
            table.addCell(setTableHeader("Code"));
            table.addCell(setTableHeader("Name"));
            table.addCell(setTableHeader("Date"));
            table.addCell(setTableHeader("AnimalType"));
            table.addCell(setTableHeader("Qty."));
            table.addCell(setTableHeader("Amount"));

            document.add(table);
            document.add(dotLineSeparator);
            document.add(new Paragraph(""));


            for (int i = 0; i < list.size(); i++) {

                Table tableData = new Table(pointColumnWidths);

                Result sellerData = list.get(i);

                if (dummyPrint) {
                    // Adding cells to the table
                    tableData.addCell(setTableData("01"));
                    tableData.addCell(setTableData("05"));
                    tableData.addCell(setTableData("Ram"));
                    tableData.addCell(setTableData("21-12-2021"));
                    tableData.addCell(setTableData("Cow"));
                    tableData.addCell(setTableData("5"));
                    tableData.addCell(setTableData("200"));
                    document.add(tableData);
                } else {
                    // Adding cells to the table
                    tableData.addCell(setTableData("" + sellerData.getId()));
                    tableData.addCell(setTableData(sellerData.getCustomer().getMemberCode()));
                    tableData.addCell(setTableData(sellerData.getCustomer().getCustomerName()));
                    tableData.addCell(setTableData(sellerData.getSaleDate()));
                    tableData.addCell(setTableData(sellerData.getAnimalType()));
                    tableData.addCell(setTableData(sellerData.getLiter()));
                    tableData.addCell(setTableData(sellerData.getTotalAmount()));
                    document.add(tableData);
                }


            }
            document.add(new Paragraph(""));
            document.add(dotLineSeparator);

            document.close();

            if (isPrinted) {
                openPDF(dest);
            } else {
                Toast.makeText(mContext, "Created... :)", Toast.LENGTH_SHORT).show();
            }


        } catch (IOException e) {
            LOGE("createPdf: Error " + e.getLocalizedMessage());
        } catch (ActivityNotFoundException ae) {
            Toast.makeText(mContext, "No application found to open this file.", Toast.LENGTH_SHORT).show();
        }
    }


    public void createPdfForAdvancePaymentList(boolean isPrinted, List<Result> list) {

        if (!checkPermissionGranted()) {
            return;
        }

        Long tsLong = System.currentTimeMillis() / 1000;
        String ts = tsLong.toString();
        String dest;

        if (!isPrinted) {
            dest = FileUtils.getAppPath(mContext) + "localSale_" + ts + ".pdf";
        } else {
            dest = FileUtils.getAppPath(mContext) + "temp" + ".pdf";
        }

        if (new File(dest).exists()) {
            new File(dest).delete();
        }

        try {
            /**
             * Creating Document
             */
            PdfWriter pdfWriter = new PdfWriter(new FileOutputStream(dest));
            PdfDocument pdfDocument = new PdfDocument(pdfWriter);
            PdfDocumentInfo info = pdfDocument.getDocumentInfo();

            info.setTitle("Example of iText7 by Pratik Butani");
            info.setAuthor("Pratik Butani");
            info.setSubject("iText7 PDF Demo");
            info.setKeywords("iText, PDF, Pratik Butani");
            info.setCreator("A simple tutorial example");

            Document document = new Document(pdfDocument, PageSize.A4, true);

            /***
             * Variables for further use....
             */
            Color mColorAccent = new DeviceRgb(153, 204, 255);
            Color mColorBlack = new DeviceRgb(0, 0, 0);
            float mHeadingFontSize = 20.0f;
            float mValueFontSize = 26.0f;

            /**
             * How to USE FONT....
             */
            PdfFont font = PdfFontFactory.createFont("assets/fonts/brandon_medium.otf", "UTF-8", true);

            final FontSet set = new FontSet();
            set.addFont("assets/fonts/brandon_medium.otf");
            set.addFont("assets/fonts/noto_sans_tamil_regular.ttf");
            document.setFontProvider(new FontProvider(set));
            // LINE SEPARATOR
            LineSeparator lineSeparator = new LineSeparator(new SolidLine());
            lineSeparator.setStrokeColor(new DeviceRgb(0, 0, 68));

            LineSeparator dotLineSeparator = new LineSeparator(new DottedLine());
            lineSeparator.setStrokeColor(new DeviceRgb(0, 0, 68));

            // Title Order Details...
            // Adding Title....


            Text mOrderDetailsTitleChunk = new Text("Milk Producer Co. Pvt. Ltd").setFont(font).setFontSize(20.0f).setFontColor(mColorBlack);
            Paragraph mOrderDetailsTitleParagraph = new Paragraph(mOrderDetailsTitleChunk)
                    .setTextAlignment(TextAlignment.CENTER);
            document.add(mOrderDetailsTitleParagraph);

            Text memberList = new Text("AdvancePayment").setFont(font).setFontSize(16.0f).setFontColor(mColorBlack);
            Paragraph memberListParagraph = new Paragraph(memberList)
                    .setTextAlignment(TextAlignment.CENTER);
            document.add(memberListParagraph);


            String date = "";
            SimpleDateFormat spf = new SimpleDateFormat("dd-MM-yyyy");
            date = spf.format(new Date());
            String timeSlot = "Morning";
            String animalName = "Buffalo-Cow";
            String timeInformationData = "Date:-" + date + "  " + "Shift:-" + timeSlot + "  " + "Milk:-" + animalName;

//            Text timeInformation = new Text(timeInformationData).setFont(font).setFontSize(14.0f).setFontColor(mColorBlack);
//            Paragraph dateParagraph = new Paragraph(timeInformation)
//                    .setTextAlignment(TextAlignment.CENTER);
//            document.add(dateParagraph);


            float[] pointColumnWidths = {50F, 50F, 130F, 70F, 70F, 50F};
            Table table = new Table(pointColumnWidths);

            document.add(new Paragraph(""));
            document.add(dotLineSeparator);

            // Adding cells to the table
            table.addCell(setTableHeader("Sr. No."));
            table.addCell(setTableHeader("Code"));
            table.addCell(setTableHeader("Name"));
            table.addCell(setTableHeader("Date"));
            table.addCell(setTableHeader("PaymentDate"));
            table.addCell(setTableHeader("Amount"));

            document.add(table);
            document.add(dotLineSeparator);
            document.add(new Paragraph(""));


            for (int i = 0; i < list.size(); i++) {

                Table tableData = new Table(pointColumnWidths);

                Result sellerData = list.get(i);

                if(dummyPrint){
                    // Adding cells to the table
                    tableData.addCell(setTableData("01"));
                    tableData.addCell(setTableData("05"));
                    tableData.addCell(setTableData("Ram"));
                    tableData.addCell(setTableData("21-12-2021"));
                    tableData.addCell(setTableData("29-12-2021"));
                    tableData.addCell(setTableData("2000"));
                    document.add(tableData);
                }else {
                    // Adding cells to the table
                    tableData.addCell(setTableData("" + sellerData.getId()));
                    tableData.addCell(setTableData(sellerData.getCustomer().getMemberCode()));
                    tableData.addCell(setTableData(sellerData.getCustomer().getCustomerName()));
                    tableData.addCell(setTableData(sellerData.getSaleDate()));
                    tableData.addCell(setTableData(sellerData.getPaymentDate()));
                    tableData.addCell(setTableData(sellerData.getTotalAmount()));
                    document.add(tableData);
                }


            }
            document.add(new Paragraph(""));
            document.add(dotLineSeparator);

            document.close();

            if (isPrinted) {
                openPDF(dest);
            } else {
                Toast.makeText(mContext, "Created... :)", Toast.LENGTH_SHORT).show();
            }


        } catch (IOException e) {
            LOGE("createPdf: Error " + e.getLocalizedMessage());
        } catch (ActivityNotFoundException ae) {
            Toast.makeText(mContext, "No application found to open this file.", Toast.LENGTH_SHORT).show();
        }
    }


    private static Cell setTableHeader(String text) {
        Color mColorBlack = new DeviceRgb(0, 0, 0);
        Paragraph nameParagraph = new Paragraph(text).setFontSize(12.0f).setFontColor(mColorBlack).setBold()
                .setTextAlignment(TextAlignment.CENTER);
        Cell cell = new Cell();
        cell.add(nameParagraph);
        cell.setBorder(Border.NO_BORDER);
        return cell;
    }

    private static Cell setTableData(String text) {
        Color mColorBlack = new DeviceRgb(0, 0, 0);
        Paragraph nameParagraph = new Paragraph(text).setFontSize(10.0f).setFontColor(mColorBlack)
                .setTextAlignment(TextAlignment.CENTER);
        Cell cell = new Cell();
        cell.add(nameParagraph);
        cell.setBorder(Border.NO_BORDER);
        return cell;
    }


    public boolean checkPermissionGranted() {
        if (checker.lacksPermissions(REQUIRED_PERMISSION)) {
            PermissionsActivity.startActivityForResult(mContext, PERMISSION_REQUEST_CODE, REQUIRED_PERMISSION);
            return false;
        }
        return true;
    }


    public void openPDF(final String dest) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                try {
                    FileUtils.openFile(mContext, new File(dest));
                } catch (Exception e) {
                    Log.d("TAG", "run: Error" + e.getMessage());
                }
            }
        }, 1000);
    }
}
