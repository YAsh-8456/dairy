package com.example.dairy.utils;

import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

import com.example.dairy.activity.MyAppBaseActivity;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.util.UUID;

public class ManagePrint extends MyAppBaseActivity implements Runnable {

    private  BluetoothSocket mBluetoothSocket;
    private  final String TAG = "ManagePrint";
    static BluetoothAdapter mBluetoothAdapter;
    static private UUID applicationUUID = UUID
            .fromString("00001101-0000-1000-8000-00805F9B34FB");
    private ProgressDialog mBluetoothConnectProgressDialog;
     BluetoothDevice mBluetoothDevice;

    public void printSlip() {

        try {
            mBluetoothSocket = mBluetoothDevice
                    .createRfcommSocketToServiceRecord(applicationUUID);
            mBluetoothAdapter.cancelDiscovery();
            mBluetoothSocket.connect();
//            mHandler.sendEmptyMessage(0);
        } catch (IOException eConnectException) {
            Log.d(TAG, "CouldNotConnectToSocket", eConnectException);
//            closeSocket(mBluetoothSocket);
            return;
        }

        Thread t = new Thread() {
            public void run() {
                try {
                    OutputStream os = mBluetoothSocket
                            .getOutputStream();
                    String BILL = "";

                    BILL = "                   XXXX MART    \n"
                            + "                   XX.AA.BB.CC.     \n " +
                            "                 NO 25 ABC ABCDE    \n" +
                            "                  XXXXX YYYYYY      \n" +
                            "                   MMM 590019091      \n";
                    BILL = BILL
                            + "-----------------------------------------------\n";


                    BILL = BILL + String.format("%1$-10s %2$10s %3$13s %4$10s", "Item", "Qty", "Rate", "Totel");
                    BILL = BILL + "\n";
                    BILL = BILL
                            + "-----------------------------------------------";
                    BILL = BILL + "\n " + String.format("%1$-10s %2$10s %3$11s %4$10s", "item-001", "5", "10", "50.00");
                    BILL = BILL + "\n " + String.format("%1$-10s %2$10s %3$11s %4$10s", "item-002", "10", "5", "50.00");
                    BILL = BILL + "\n " + String.format("%1$-10s %2$10s %3$11s %4$10s", "item-003", "20", "10", "200.00");
                    BILL = BILL + "\n " + String.format("%1$-10s %2$10s %3$11s %4$10s", "item-004", "50", "10", "500.00");

                    BILL = BILL
                            + "\n-----------------------------------------------";
                    BILL = BILL + "\n\n ";

                    BILL = BILL + "                   Total Qty:" + "      " + "85" + "\n";
                    BILL = BILL + "                   Total Value:" + "     " + "700.00" + "\n";

                    BILL = BILL
                            + "-----------------------------------------------\n";
                    BILL = BILL + "\n\n ";


                    BILL =
                            "M.P.C.S PVT. LTD\n" +
                                    "DATE : 07.12.2020  TIME: 03.57 PM\n" +
                                    "SHIFT : EVENING\n" +
                                    "MILK : BUFFALO\n" +
                                    "C. NUMBER : 11\n" +
                                    "NAME : GULAB PATALIYA\n" +
                                    "QTY : 10\n" +
                                    "FAT %  : 8.5\n" +
                                    "SNF % : 9.0\n" +
                                    "CLR % : 26.6\n" +
                                    "WATTER : 10.55\n" +
                                    "TOTAL : 620.00\n" +
                                    "FAT PD  : 8.5\n" +
                                    "LITER PD :   20\n" +
                                    "GOVT PD : 3.50\n" +
                                    "TOTAL  : 940.00\n" +
                                    "RATE /LITER : 62.00\n" +
                                    "RATE/W/PD/LITER : 94\n" +
                                    "9829015097\n";

                    os.write(BILL.getBytes());
                    //This is printer specific code you can comment ==== > Start

                    // Setting height
                    int gs = 29;
                    os.write(intToByteArray(gs));
                    int h = 104;
                    os.write(intToByteArray(h));
                    int n = 162;
                    os.write(intToByteArray(n));

                    // Setting Width
                    int gs_width = 29;
                    os.write(intToByteArray(gs_width));
                    int w = 119;
                    os.write(intToByteArray(w));
                    int n_width = 2;
                    os.write(intToByteArray(n_width));


                } catch (Exception e) {
                    Log.e("MainActivity", "Exe ", e);
                }
            }
        };
        t.start();
    }

    public static byte intToByteArray(int value) {
        byte[] b = ByteBuffer.allocate(4).putInt(value).array();

        for (int k = 0; k < b.length; k++) {
            System.out.println("Selva  [" + k + "] = " + "0x"
                    + UnicodeFormatter.byteToHex(b[k]));
        }

        return b[3];
    }


    @Override
    public void run() {
        try {
            mBluetoothSocket = mBluetoothDevice
                    .createRfcommSocketToServiceRecord(applicationUUID);
            mBluetoothAdapter.cancelDiscovery();
            mBluetoothSocket.connect();
            mHandler.sendEmptyMessage(0);
        } catch (IOException eConnectException) {
            Log.d(TAG, "CouldNotConnectToSocket", eConnectException);
            closeSocket(mBluetoothSocket);
            return;
        }
    }

    private void closeSocket(BluetoothSocket nOpenSocket) {
        try {
            nOpenSocket.close();
            Log.d(TAG, "SocketClosed");
        } catch (IOException ex) {
            Log.d(TAG, "CouldNotCloseSocket");
        }
    }

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            mBluetoothConnectProgressDialog.dismiss();
            Toast.makeText(getBaseContext(), "DeviceConnected", Toast.LENGTH_SHORT).show();
        }
    };
}
