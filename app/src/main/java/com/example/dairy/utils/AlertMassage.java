package com.example.dairy.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.ViewGroup;


public class AlertMassage {

    public static AlertDialog alert;

    public static void showProgress(final Context activity) {

//        final AlertDialog.Builder builder = new AlertDialog.Builder(activity, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
//        View dialoglayout = LayoutInflater.from(activity).inflate(R.layout.layout_loader, null);
//        builder.setInverseBackgroundForced(true);
//        builder.setView(dialoglayout);
//        alert = builder.setCancelable(false).create();
//
//        ImageView loader = dialoglayout.findViewById(R.id.loader);
//        ConstraintLayout loaderLayout = dialoglayout.findViewById(R.id.loaderLayout);
//
//        Glide.with(activity)
//                .asGif()
//                .load(R.drawable.preloader30)
//                .into(loader);
//
//        loaderLayout.setVisibility(View.VISIBLE);


       /* if (alert != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            alert.getWindow().setLayout(width, height);

        }*/


        if (!((Activity) activity).isFinishing()) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            alert.getWindow().setLayout(width, height);
            alert.show();
        }

    }


    private void showDialog(Activity activity) {

//        final View dialogView = View.inflate(activity, R.layout.layout_bottom_sheet, null);
//
//        final Dialog dialog = new Dialog(activity, R.style.MyAlertDialogStyle);
//        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//        dialog.setContentView(dialogView);
//
//        LinearLayout llMainBottomSheetLayout = dialog.findViewById(R.id.llMainBottomSheetLayout);
//        llMainBottomSheetLayout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                dialog.dismiss();
//            }
//        });
//
//        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
//            @Override
//            public void onShow(DialogInterface dialogInterface) {
//            }
//        });
//
//        dialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
//            @Override
//            public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
//                if (i == KeyEvent.KEYCODE_BACK) {
//
//                    return true;
//                }
//
//                return false;
//            }
//        });
//
//
//        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
//
//        dialog.show();
    }



    public static void MassageAlert(Activity activity, String Massage) {

        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle("Alert");
        builder.setMessage(Massage);

        // add a button
        builder.setPositiveButton("OK", null);

        // create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public static void logOutMassageAlert(Activity activity, String Massage, DialogInterface.OnClickListener yesClick) {

        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle("Alert");
        builder.setMessage(Massage);

        // add a button
        builder.setPositiveButton("Yes", yesClick);
        builder.setNegativeButton("No", null );


        // create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }
}
