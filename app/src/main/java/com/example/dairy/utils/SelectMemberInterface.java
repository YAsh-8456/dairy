package com.example.dairy.utils;

import com.example.dairy.modal.Result;

public interface SelectMemberInterface {

    void selectedMember(Result result);
}
