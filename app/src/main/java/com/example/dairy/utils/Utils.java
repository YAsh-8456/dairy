package com.example.dairy.utils;

import static com.example.dairy.utils.Constants.DAIRY_ADDRESS;
import static com.example.dairy.utils.Constants.DAIRY_ID;
import static com.example.dairy.utils.Constants.DAIRY_NAME;
import static com.example.dairy.utils.Constants.DAIRY_TOKEN;
import static com.example.dairy.utils.Constants.IS_LOGIN;
import static com.example.dairy.utils.Constants.SLIP_MESSAGE;
import static com.example.dairy.utils.Constants.SLIP_MESSAGE_HINDI;
import static com.example.dairy.utils.Constants.USER_MO_NUMBER;
import static com.example.dairy.utils.Constants.USER_NAME;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Looper;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.content.ContextCompat;

import com.bumptech.glide.request.RequestOptions;
import com.example.dairy.R;
import com.example.dairy.activity.MainActivity;
import com.example.dairy.modal.Categories;
import com.example.dairy.modal.CommonResponse;
import com.example.dairy.modal.Result;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class Utils {

    public static final String TAG = "Utils";
    public static List<Result> memberList = new ArrayList<>();


    public static List<Result> getMemberList() {
        return memberList;
    }

    public static void setMemberList(List<Result> list) {
        memberList.clear();
        memberList.addAll(list);
    }

    public static List<Result> getBuyerMemberList() {
        List<Result> list = getMemberList();
        List<Result> buyerList = new ArrayList<>();
        for (Result result : list) {
            if (!result.getMemberType().toUpperCase().equals("seller".toUpperCase())) {
                buyerList.add(result);
            }
        }
        return buyerList;

    }

    public static List<Result> getSellerMemberList() {
        List<Result> list = getMemberList();
        List<Result> sellerList = new ArrayList<>();
        for (Result result : list) {
            if (!result.getMemberType().toUpperCase().equals("buyer".toUpperCase())) {
                sellerList.add(result);
            }
        }
        return sellerList;

    }

    public static List<Result> getSellerMemberList(List<Result> allMemberList) {
        List<Result> list = allMemberList;
        List<Result> sellerList = new ArrayList<>();
        for (Result result : list) {
            if (!result.getMemberType().toUpperCase().equals("buyer".toUpperCase())) {
                sellerList.add(result);
            }
        }
        return sellerList;

    }

    public static void addNewMemberInList(Result memberRecord) {
        memberList.add(memberRecord);
    }

    public static boolean isLogin() {
        return PreferenceManager.getValueBoolean(Constants.IS_LOGIN);
    }

    public static String arrangeFloatValue(Float value) {
        return new DecimalFormat("##.##").format(value);
    }

    public static float arrangeFloatValueReturnFloat(Float value) {
        Debug.e(TAG, "arrangeFloatValueReturnFloat =-->" + Float.parseFloat(new DecimalFormat("##.##").format(value)));
        return Float.parseFloat(new DecimalFormat("##.##").format(value));
    }

    public static int arrangeFloatSingleValue(Float value) {
        int finalValue = Integer.valueOf(new DecimalFormat("##").format(value * 10));
        return finalValue;
    }

//    public static void createNointernetView(final Activity activity, final ConstraintLayout real) {
//
//        final ConstraintLayout.LayoutParams params = new ConstraintLayout.LayoutParams(C.pxWidth, C.pxHeight);
//        final noInternet noIntr = new noInternet(activity);
//        real.addView(noIntr, params);
//        noIntr.initiate();
//
//        noIntr.setInternetListener(new noInternet.internetListener() {
//            @Override
//            public void onTryAgain() {
//                if (isInternetConnected(activity)) {
//                    real.removeView(noIntr);
//                } else {
//                    Toast.makeText(activity, "Internet Error", Toast.LENGTH_SHORT).show();
//                }
//            }
//        });
//    }

    private Dialog progressDialog = null;
    private ProgressBar progressBar;

//    public void showPopupProgressSpinner(Activity activity, Boolean isShowing) {
//        if (isShowing) {
//            progressDialog = new Dialog(activity);
//            progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//            progressDialog.setContentView(R.layout.popup_progressbar);
//            progressDialog.setCancelable(false);
//            progressDialog.getWindow().setBackgroundDrawable(
//                    new ColorDrawable(Color.TRANSPARENT));
//
//            progressBar = (ProgressBar) progressDialog
//                    .findViewById(R.id.progressBar1);
//            progressBar.getIndeterminateDrawable().setColorFilter(
//                    Color.parseColor("#ff6700"),
//                    android.graphics.PorterDuff.Mode.MULTIPLY);
//            progressDialog.show();
//        } else if (!isShowing) {
//            progressDialog.dismiss();
//        }
//    }

    public static boolean isStringInteger(String number) {
        try {
            Integer.parseInt(number);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    public static boolean isStringFloat(String number) {
        try {
            Float.parseFloat(number);
        } catch (Exception e) {
            return false;
        }
        return true;
    }


    public static Float getStringToFloat(String number) {

        if (!Utils.isStringFloat(number)) {
            return arrangeFloatValueReturnFloat((float) Integer.parseInt(number));
        } else {
            return arrangeFloatValueReturnFloat(Float.valueOf(number));
        }
    }

    public static boolean getStringToInteger(String number) {
        try {
            Float.parseFloat(number);
        } catch (Exception e) {
            return false;
        }
        return true;
    }


    public static boolean isInternetConnected(Context mContext) {

        try {
            ConnectivityManager connect = null;
            if (mContext == null)
                return true;
            connect = (ConnectivityManager) mContext
                    .getSystemService(Context.CONNECTIVITY_SERVICE);

            if (connect != null) {
                NetworkInfo resultMobile = connect
                        .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

                NetworkInfo resultWifi = connect
                        .getNetworkInfo(ConnectivityManager.TYPE_WIFI);

                if ((resultMobile != null && resultMobile
                        .isConnectedOrConnecting())
                        || (resultWifi != null && resultWifi
                        .isConnectedOrConnecting())) {
                    return true;
                } else {
                    return false;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }


    public static void setDefaultData(Activity activity, CreateChartInterface createChartInterface) {

        PreferenceManager.putFloatValue(Constants.COW_FAT_LIMIT, Constants.cowFatLimit);

        if (PreferenceManager.getValueInt(Constants.COW_LOCAL_SALE_RATE) <= 0) {
            PreferenceManager.putValueInt(Constants.COW_LOCAL_SALE_RATE, Constants.COW_LOCAL_SALE_PRICE);
        }
        if (PreferenceManager.getValueInt(Constants.BUFFALO_LOCAL_SALE_RATE) <= 0) {
            PreferenceManager.putValueInt(Constants.BUFFALO_LOCAL_SALE_RATE, Constants.BUFFALO_LOCAL_SALE_PRICE);
        }


        if (Utils.isNotEmpty(PreferenceManager.getValue(Constants.BUFFALO_CHART_DATA)) && Utils.isNotEmpty(PreferenceManager.getValue(Constants.COW_CHART_DATA))) {
            // already there rate chart in app then without update more main screen  ----------------------------------------------- >
            Intent openMainScreen = new Intent(activity, MainActivity.class);
            activity.startActivity(openMainScreen);
            activity.finish();
        } else {
            // create rate chart  ----------------------------------------------- >
            Utils.createChartData(createChartInterface, activity, Constants.buffaloStartFAT, Constants.buffaloEndFAT, Constants.buffaloStartSNF, Constants.buffaloEndSNF, Constants.fatRateData, Constants.snfRateData, Constants.commissionData, Constants.dedicationData, true);
        }
    }

    public static void saveDairyDetails(String token, Result userResponse) {
        Debug.e("Utils", userResponse.toString());
        PreferenceManager.putValueBoolean(IS_LOGIN, true);
        PreferenceManager.putValue(DAIRY_TOKEN, token);
        PreferenceManager.putValueInt(DAIRY_ID, userResponse.getId());
        PreferenceManager.putValue(USER_NAME, userResponse.getUserName());
        PreferenceManager.putValue(USER_MO_NUMBER, userResponse.getPhoneNumber());
        PreferenceManager.putValue(DAIRY_NAME, userResponse.getDairyName());
        PreferenceManager.putValue(DAIRY_ADDRESS, userResponse.getDairyAddress());

        String tempContactEnglishUs = Constants.contactUsEnglish + userResponse.getPhoneNumber();
        String tempContactHindiUs = Constants.contactUsHindi + userResponse.getPhoneNumber();
        PreferenceManager.putValue(SLIP_MESSAGE, tempContactEnglishUs);
        PreferenceManager.putValue(SLIP_MESSAGE_HINDI, tempContactHindiUs);
    }


    public static void logoutValueRemove() {
        PreferenceManager.removeValue(IS_LOGIN);
        PreferenceManager.removeValue(DAIRY_TOKEN);
        PreferenceManager.removeValue(DAIRY_ID);
        PreferenceManager.removeValue(USER_NAME);
        PreferenceManager.removeValue(USER_MO_NUMBER);
        PreferenceManager.removeValue(DAIRY_NAME);
        PreferenceManager.removeValue(DAIRY_ADDRESS);
        PreferenceManager.removeValue(SLIP_MESSAGE);
        PreferenceManager.removeValue(SLIP_MESSAGE_HINDI);
        Debug.e(TAG, "Clear user value");
    }

    public static boolean isValidEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }

    public static boolean isValidPassword(final String password) {

        Pattern pattern;
        Matcher matcher;
//        final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[A-Z])(?=.*[@#$%^&+=!])(?=\\S+$).{4,}$";
        final String PASSWORD_PATTERN = "^(?=.*[@#$%^&+=!])(?=\\S+$).{4,}$";
        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(password);
        return matcher.matches();
    }

    public static void showSnackbar(Context context, View view, String message) {
        Snackbar snackbar = Snackbar
                .make(view, message, Snackbar.LENGTH_LONG);
        View snackbarView = snackbar.getView();
        snackbarView.setBackgroundColor(ContextCompat.getColor(context, R.color.black));
        TextView textView = (TextView) snackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
        textView.setTextColor(Color.WHITE);
        snackbar.show();
    }

    public static RequestOptions noImage() {
        return new RequestOptions()
                .centerCrop()
                .placeholder(R.drawable.no_image)
                .error(R.drawable.no_image);
    }

    public static void showToast(Context mContext, String message) {
        Toast toast = Toast.makeText(mContext, message, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }

    public static void showSoftKeyword(Activity activity) {

        if (activity.getCurrentFocus() != null) {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            View v = ((Activity) activity).getCurrentFocus();
            if (v == null)
                return;
            imm.showSoftInput(v, InputMethodManager.SHOW_IMPLICIT);
        }
    }

    public static void showSoftKeyword(Activity activity, View view) {

        if (activity.getCurrentFocus() != null) {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
        }
    }

    public static void showKeyboard(Context context) {
        ((InputMethodManager) (context).getSystemService(Context.INPUT_METHOD_SERVICE)).toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
    }

    public static void hideKeyboard(Context context) {
        try {
            ((Activity) context).getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
            if ((((Activity) context).getCurrentFocus() != null) && (((Activity) context).getCurrentFocus().getWindowToken() != null)) {
                ((InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(((Activity) context).getCurrentFocus().getWindowToken(), 0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void hideSoftKeyword(Activity activity) {
        try {
            if (activity.getCurrentFocus() != null) {
                InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(activity.INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static boolean isEmpty(String str) {

        if (!TextUtils.isEmpty(str))
            return TextUtils.isEmpty(str.trim());
        return TextUtils.isEmpty(str);

    }

    public static boolean isNotEmpty(String str) {
        return !isEmpty(str);
    }


    public static String convertListToString(List<Map<Integer, Object>> list) {
        Gson gson1 = new Gson();
        return gson1.toJson(list);

    }

    public static CommonResponse convertStringToList(String s) {
        Type list = new TypeToken<CommonResponse>() {
        }.getType();
        Gson gson = new Gson();
        return gson.fromJson(s, list);
    }


    public static String convertChartListToString(List<Map<Integer, Object>> list) {
        Gson gson1 = new Gson();
        return gson1.toJson(list);

    }

    public static List<Map<Integer, Object>> convertStringToChartList(String s) {
        Type list = new TypeToken<List<Map<Integer, Object>>>() {
        }.getType();
        Gson gson = new Gson();
        return gson.fromJson(s, list);
    }

    public static Typeface setBoldText(Activity activity) {

        Typeface face = Typeface.createFromAsset(activity.getAssets(),
                "proximanova_bold.ttf");

        return face;
    }

    public static Typeface setRagularText(Activity activity) {

        Typeface face = Typeface.createFromAsset(activity.getAssets(),
                "proximanova_reg.ttf");

        return face;
    }

    public static Typeface setSemiBoldText(Activity activity) {

        Typeface face = Typeface.createFromAsset(activity.getAssets(),
                "proximanova_sbold.ttf");

        return face;
    }

    public static String finalDateFormat(String date, boolean isMorning) {
        Debug.e(TAG, "First value =---->" + date);
        SimpleDateFormat input = new SimpleDateFormat("dd-MM-yyyy");
        SimpleDateFormat output = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            Debug.e(TAG, "First time value =---->" + input.parse(date));
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(input.parse(date));
            Debug.e(TAG, "WithOut add Time  =---->" + output.format(calendar.getTime()));
            if (isMorning) {
                calendar.set(Calendar.HOUR_OF_DAY, 8);
            } else {
                calendar.set(Calendar.HOUR_OF_DAY, 20);
            }
            Debug.e(TAG, "Time =---->" + output.format(calendar.getTime()));
            return output.format(calendar.getTime());    // format output
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }


    public static String finalAdvancePaymentDateFormat(String date) {
        Debug.e(TAG, "First value =---->" + date);
        SimpleDateFormat output = new SimpleDateFormat("dd-MM-yy");
        SimpleDateFormat input = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(input.parse(date));
            Debug.e(TAG, "Time =---->" + output.format(calendar.getTime()));
            return output.format(calendar.getTime());    // format output
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }


    public static String finalDateFormat(String date) {

        // Date format dd/MM/yyyy
        SimpleDateFormat input = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat output = new SimpleDateFormat("yyyy-MM-dd");
        try {
            return output.format(input.parse(date));    // format output
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }


    public static String changeResponseDateFormat(String date) {

        // Date format dd/MM/yyyy
        SimpleDateFormat input = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat output = new SimpleDateFormat("dd/MM/yyyy");
        try {
            return output.format(input.parse(date));    // format output
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String onlyPrintDate(String date) {

        // Date format dd/MM/yyyy
        SimpleDateFormat input = new SimpleDateFormat("dd-MM-yyyy");
        SimpleDateFormat output = new SimpleDateFormat("dd");
        try {
            return output.format(input.parse(date));    // format output
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }


    public static final SimpleDateFormat BIRTHDAY_FORMAT_PARSER = new SimpleDateFormat("dd/MM/yyyy");
    public static final String DASH_STRING = "-";

    public static Calendar parseDateString(String date) {
        Calendar calendar = Calendar.getInstance();
        BIRTHDAY_FORMAT_PARSER.setLenient(false);
        try {
            calendar.setTime(BIRTHDAY_FORMAT_PARSER.parse(date));
        } catch (ParseException e) {
        }
        return calendar;
    }

    public static boolean isValidBirthday(String birthday) {
        Calendar calendar = parseDateString(birthday);
        int year = calendar.get(Calendar.YEAR);
        int thisYear = Calendar.getInstance().get(Calendar.YEAR);
        return year >= 1900 && year < thisYear;
    }


    private static boolean appInstalledOrNot(String uri, Activity activity) {
        PackageManager pm = activity.getPackageManager();
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
        }

        return false;
    }

    public static List<Categories> getCategoriesList(Context context) {

        List<Categories> list = new ArrayList<>();

        Categories record1 = new Categories(R.drawable.milk_buy, context.getResources().getString(R.string.categories_milk_buy), Constants.CATEGORIES_CLICK_TYPE.MILK_BUY);
        Categories record2 = new Categories(R.drawable.milk, context.getResources().getString(R.string.categories_milk_sell), Constants.CATEGORIES_CLICK_TYPE.MILK_SELL);
        Categories record3 = new Categories(R.drawable.master_entry, context.getResources().getString(R.string.categories_master), Constants.CATEGORIES_CLICK_TYPE.MASTER_ENTRY);
        Categories record4 = new Categories(R.drawable.rate_chart, context.getResources().getString(R.string.categories_milk_rate_chart), Constants.CATEGORIES_CLICK_TYPE.CHART);
        Categories record5 = new Categories(R.drawable.report, context.getResources().getString(R.string.categories_milk_report), Constants.CATEGORIES_CLICK_TYPE.REPORT);
        Categories record6 = new Categories(R.drawable.advance_payment, context.getResources().getString(R.string.categories_milk_advance_payment), Constants.CATEGORIES_CLICK_TYPE.ADVANCE_PAYMENT);
        Categories record7 = new Categories(R.drawable.settings, context.getResources().getString(R.string.categories_milk_setting), Constants.CATEGORIES_CLICK_TYPE.SETTING);
        Categories record8 = new Categories(R.drawable.ic_baseline_contact_mail_24, context.getResources().getString(R.string.categories_milk_setting), Constants.CATEGORIES_CLICK_TYPE.CONTACT_MAIL);
        Categories record9 = new Categories(R.drawable.logout, context.getResources().getString(R.string.categories_milk_logout), Constants.CATEGORIES_CLICK_TYPE.LOGOUT);

        list.add(record1);
        list.add(record2);
        list.add(record6);

        list.add(record3);
        list.add(record4);
        list.add(record5);

        list.add(record7);
//        list.add(record8);
        list.add(record9);

        return list;
    }

    public static List<Integer> getSliderImagesList(MainActivity main) {
        List<Integer> list = new ArrayList<>();

        list.add(R.drawable.demo_banner_01);
        list.add(R.drawable.demo_banner_02);
        list.add(R.drawable.demo_banner_01);
        list.add(R.drawable.demo_banner_02);
        list.add(R.drawable.demo_banner_01);


        return list;

    }


    public static void createChartData(CreateChartInterface createChartInterface, final Activity activity, float fromFAT,
                                       float toFAT, float fromSNF, float toSNF, final float fatRateData, final float snfRateData, final float commissionData, final float dedicationData, final boolean isBuffalo) {
        List<Map<Integer, Object>> list = new ArrayList<>();
        if (Looper.myLooper() == null) {
            Looper.prepare();
        }
        try {
            final int startFAT = Utils.arrangeFloatSingleValue(Float.valueOf(fromFAT));
            final int endFAT = Utils.arrangeFloatSingleValue(Float.valueOf(toFAT));

            final int startSNF = Utils.arrangeFloatSingleValue(Float.valueOf(fromSNF));
            final int endSNF = Utils.arrangeFloatSingleValue(Float.valueOf(toSNF));

            Map<Integer, Object> headerMap = new HashMap<>();
            headerMap.put(0, "");

            for (int snfCount = startSNF, j = 1; snfCount <= endSNF; snfCount += 1, j++) {
                float snfValue = snfCount / 10.0f;
                headerMap.put(j, String.valueOf(snfValue));
            }
            list.add(headerMap);

            for (int count = startFAT, i = 0; count <= endFAT; count += 1, i++) {
                float fatValue = count / 10.0f;
                Debug.e(TAG, "New FAT Value =------>" + fatValue + " " + i);
                Map<Integer, Object> headerMap1 = new HashMap<>();
                headerMap1.put(0, String.valueOf(fatValue));

                for (int snfCount = startSNF, j = 1; snfCount <= endSNF; snfCount += 1, j++) {
                    float snfValue = snfCount / 10.0f;
                    Debug.e(TAG, "New SNF value =------>" + snfValue);
                    float _FAT = fatValue;
                    float _SNF = snfValue;
                    Float finalPrice = manageMilkCalculation(_FAT, _SNF, fatRateData, snfRateData, commissionData, dedicationData);
                    headerMap1.put(j, String.valueOf(finalPrice));
                }

                list.add(headerMap1);

            }

            createChartInterface.createdChart(isBuffalo, list);
            Toast.makeText(activity, "Successfully create your chart", Toast.LENGTH_SHORT).show();

        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(activity, "Error", Toast.LENGTH_SHORT).show();
        }

        Looper.loop();
    }


    public static void addPD(String FAT_PD, String literPD, String govtPD) {

        PreferenceManager.putValue(Constants.FAT_PD, FAT_PD);
        PreferenceManager.putValue(Constants.LITER_PD, literPD);
        PreferenceManager.putValue(Constants.GOVT_PD, govtPD);


    }

    public static void removePD(String FAT_PD, String literPD, String govtPD) {

        PreferenceManager.putValue(Constants.FAT_PD, "");
        PreferenceManager.putValue(Constants.LITER_PD, "");
        PreferenceManager.putValue(Constants.GOVT_PD, "");


    }

    public static float manageMilkCalculation(float fat, float snf, float fatRateData, float snfRateData, float commissionData, float dedicationData) {

        float RT = fat * fatRateData / 100;
        float LR = snf * snfRateData / 100;
        float literRate = RT + LR;
        float literRateWithCommission = literRate + commissionData - dedicationData;

        return Float.parseFloat(arrangeFloatValue(literRateWithCommission));
    }

    public static Result checkIDValidOrNot(String id) {

        List<Result> list = getMemberList();
        for (Result result : list) {
            if (result.getMemberCode().equals(id) && !result.getMemberType().toUpperCase().equals("seller".toUpperCase())) {
                return result;
            }
        }

        return null;
    }

    public static Result checkIDValidOrNotSell(String id) {

        List<Result> list = getMemberList();
        for (Result result : list) {
            if (result.getMemberCode().equals(id) && !result.getMemberType().toUpperCase().equals("buyer".toUpperCase())) {
                return result;
            }
        }

        return null;
    }


    public static boolean isMorning() {
        Calendar c = Calendar.getInstance();
        int timeOfDay = c.get(Calendar.HOUR_OF_DAY);

        if (timeOfDay >= 0 && timeOfDay < 12) {
            return true;
        } else if (timeOfDay >= 12 && timeOfDay < 16) {
            return true;
        } else if (timeOfDay >= 16 && timeOfDay < 21) {
            return false;
        } else if (timeOfDay >= 21 && timeOfDay < 24) {
            return false;
        }

        return false;
    }


    public static String getTimeSlot(Activity activity, String timeSlot) {
        return timeSlot.equals(activity.getResources().getString(R.string.morning)) ? Constants.MORNING
                : Constants.EVENING;
    }

    public static String getPetName(Activity activity, String timeSlot) {
        return timeSlot.equals(activity.getResources().getString(R.string.morning)) ? Constants.MORNING
                : Constants.EVENING;
    }

    public static boolean selectedLanguageIsHindi() {
        if (isEmpty(PreferenceManager.getValue(Constants.SELECTED_LANGUAGE))) {
            if (PreferenceManager.getValue(Constants.SELECTED_LANGUAGE).toUpperCase().equals("HI")) {
                return true;
            }
            return false;
        }

        return false;
    }

}
