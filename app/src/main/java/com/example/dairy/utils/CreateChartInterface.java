package com.example.dairy.utils;

import java.util.List;
import java.util.Map;

public interface CreateChartInterface {

    void createdChart(boolean isBuffalo, List<Map<Integer, Object>> data);

    void createdCowChart(boolean isBuffalo, List<Map<Integer, Object>> data);

    void createdBuffaloChart(boolean isBuffalo, List<Map<Integer, Object>> data);
}
