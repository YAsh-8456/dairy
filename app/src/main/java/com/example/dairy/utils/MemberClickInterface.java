package com.example.dairy.utils;

import com.example.dairy.modal.Result;

public interface MemberClickInterface {

    public void delete(Result result, int position);

    public void view(Result result, int position);

    public void edit(Result result, int position);

    public void print(Result result, int position);
}