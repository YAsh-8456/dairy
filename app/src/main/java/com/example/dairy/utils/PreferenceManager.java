package com.example.dairy.utils;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;


public class PreferenceManager extends Application {

    public static SharedPreferences preferences;
    public static SharedPreferences.Editor prefEditor;


    @Override
    public void onCreate() {
        super.onCreate();
        preferences = getSharedPreferences("DAIRY", MODE_PRIVATE);
        prefEditor = preferences.edit();
        prefEditor.commit();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
//        MultiDex.install(this);
    }

    public static void removePreferenceData() {
        prefEditor.clear();
        prefEditor.commit();
    }


    public static String getValue(String name) {
        return preferences.getString(name, null);
    }

    public static void putValue(String name, String value) {
        prefEditor.putString(name, value);
        prefEditor.commit();
    }


    public static Float getFloatValue(String name) {
        return preferences.getFloat(name,0.0f);
    }

    public static void putFloatValue(String name, Float value) {
        prefEditor.putFloat(name, value);
        prefEditor.commit();
    }

    // integer value

    public static int getValueInt(String name) {
        return preferences.getInt(name, 0);
    }

    public static void putValueInt(String name, int value) {
        prefEditor.putInt(name, value);
        prefEditor.commit();
    }

    // boolean value

    public static boolean getValueBoolean(String name) {
        return preferences.getBoolean(name, false);
    }

    public static void putValueBoolean(String name, Boolean value) {
        prefEditor.putBoolean(name, value);
        prefEditor.commit();
    }

    public static void removeValue(String key){
        prefEditor.remove(key);
        prefEditor.commit();
    }

    public static void clearData(){
        prefEditor.clear();
        prefEditor.apply();
    }
}