package com.example.dairy.utils;

public class Constants {

    public static final int RESULT_OK = 200;
    public static final int ERROR_AUTHENTICATON = 400;
    public static final int ERROR_UPROCESSABLE = 422;
    public static final int SERVER_ERROR = 500;

    // On Result Activity
    public static final int LOGIN_REQURST_CODE = 105;

    // Forth Discovery screen date
    public static final int YEAR_SCREEN = 0;
    public static final int DAY_SCREEN = 1;
    public static final int MONTH_SCREEN = 2;
    public static final int ROOM_SCREEN = 4;
    public static final int PERSONS_SCREEN = 3;


    // Milk Calculation variable
    public static final float cowStartFAT = 4.5f;
    public static final float cowEndFAT = 7.0f;
    public static final float buffaloStartFAT = 5.5f;
    public static final float buffaloEndFAT = 9.0f;
    public static final float cowStartSNF = 5.5f;
    public static final float cowEndSNF = 8.5f;
    public static final float buffaloStartSNF = 5.5f;
    public static final float buffaloEndSNF = 8.5f;
    public static final float fatRateData = 200.0f;
    public static final float snfRateData = 180.0f;
    public static final float commissionData = 5.00f;
    public static final float dedicationData = 0.02f;
    public static final String ENGLISH = "EN";
    public static final String HINDI = "HI";


    public static final float cowFatLimit = 5.0f;


    public static final String contactUsHindi = "संपर्क करें";
    public static final String contactUsEnglish = "Contact Us";

    public enum API_TYPE {

        DAIRY_LOGIN,
        DAIRY_REGISTER,
        DAIRY_EDIT,
        DAIRY_DELETE,
        VERIFY,

        ADD_MASTER,
        GET_MASTER,
        EDIT_MASTER,
        DELETE_MASTER,

        GET_MILK_BUY,
        ADD_MILK_BUY,
        EDIT_MILK_BUY,
        REMOVE_MILK_BUY,

        GET_MILK_SELL,
        ADD_MILK_SELL,
        EDIT_MILK_SELL,
        REMOVE_MILK_SELL,

        GET_ADVANCE_PAYMENT,
        ADD_ADVANCE_PAYMENT,
        EDIT_ADVANCE_PAYMENT,
        REMOVE_ADVANCE_PAYMENT,

        GET_PURCHASE_REPORT,
        GET_PAYMENT_REPORT,
        GET_LOCAL_SALE_REPORT,
        GET_LEDGER_REPORT,
        GET_MEMBER_REPORT,

        SETTING,
        LOGOUT,

    }


    public enum GET_VALUE_TYPE {
        GET_LITER_PRICE,
        GET_TOTAL_PRICE,
        GET_TOTAL_PRICE_WITH_PD
    }

    public enum CATEGORIES_CLICK_TYPE {

        MILK_BUY,
        MILK_SELL,
        MASTER_ENTRY,
        ADVANCE_PAYMENT,
        CHART,
        SETTING,
        CONTACT_MAIL,
        REPORT,
        LOGOUT,


    }


    public enum PRINT_TYPE {

        // Single Record slip
        MILK_BUY,
        MILK_SELL,
        ADVANCE_PAYMENT,

        // Multiple Record slip
        MASTER_LIST,
        PRODUCER_LIST,
        LEDGER_LIST,
        PURCHASE_LIST,
        SELL_LIST,
        ADVANCE_PAYMENT_LIST,

        // PDF
        MEMBER_LIST_PDF,
        PURCHASE_LIST_PDF,
        LAGER_LIST_PDF,
        LOCAL_SELL_LIST_PDF,
        ADVANCE_PAYMENT_LIST_PDF,


    }

    public static final String PRINT_ENABLE = "PRINT_ENABLE";
    public static final String SMS_ENABLE = "SMS_ENABLE";
    public static final String CALCULATE_BY_FAT = "CALCULATE_BY_FAT";


    public static final String COW = "Cow";   // Constants value for pass API
    public static final String BUFFALO = "Buffalo";   // Constants value for pass API
    public static final String MORNING = "Morning";
    public static final String EVENING = "Evening";
    public static final String BUYER = "BUYER";
    public static final String SELLER = "SELLER";
    public static final String ALL = "ALL";

    public static final String IS_LOGIN = "IS_LOGIN";
    public static final String DAIRY_TOKEN = "DAIRY_TOKEN";
    public static final String DAIRY_ID = "DAIRY_ID";
    public static final String USER_NAME = "USER_NAME";
    public static final String USER_MO_NUMBER = "USER_MO_NUMBER";
    public static final String DAIRY_NAME = "DAIRY_NAME";
    public static final String DAIRY_ADDRESS = "DAIRY_ADDRESS";

    public static final String USER_FULL_NAME = "USER_FULL_NAME";
    public static final String USER_EMAIL = "USER_EMAIL";
    public static final String USER_PROFILE = "USER_PROFILE";

    public static final String DEVICE_TYPE = "DEVICE_TYPE";
    public static final String DEVICE_TOKEN = "DEVICE_TOKEN";
    public static final String PHONE_NO = "PHONE_NO";
    public static final String COMPANY_NAME = "COMPANY_NAME";

    public static final String COW_CHART_DATA = "COW_CHART_DATA";
    public static final String BUFFALO_CHART_DATA = "BUFFALO_CHART_DATA";

    public static final String COW_FAT_LIMIT = "COW_FAT_LIMIT";

    public static final String COW_LOCAL_SALE_RATE = "COW_LOCAL_SALE_RATE";
    public static final String BUFFALO_LOCAL_SALE_RATE = "BUFFALO_LOCAL_SALE_RATE";

    public static final String FAT_PD = "FAT_PD";
    public static final String LITER_PD = "LITER_PD";
    public static final String GOVT_PD = "GOVT_PD";


    public static final String SELECTED_LANGUAGE = "SELECTED_LANGUAGE";

    public static final int COW_LOCAL_SALE_PRICE = 40;
    public static final int BUFFALO_LOCAL_SALE_PRICE = 60;

    // MACHINE SETTING ----------> This use for store value of setting
    public static final String WEIGHT_SCALE_ON = "WEIGHT_SCALE_ON";
    public static final String FAT_MACHINE_ON = "FAT_MACHINE_ON";
    public static final String PRINTER_ON = "PRINTER_ON";
    public static final String BACK_MACHINE_ON = "BACK_MACHINE_ON";
    public static final String DISPLAY_BOARD_ON = "DISPLAY_BOARD_ON";


    // SLIP MESSAGE ----------> This use for store value of setting
    public static final String SLIP_MESSAGE = "SLIP_MESSAGE";
    public static final String SLIP_MESSAGE_HINDI = "SLIP_MESSAGE_HINDI";

    // MILK SLIP SETTING ----------> This use for store value of setting
    public static final String SLIP_IN_HINDI = "SLIP_IN_HINDI";
    public static final String SERIAL_NO_IN_SLIP = "SERIAL_NO_IN_SLIP";
    public static final String PER_LITER_RATE_IN_SLIP = "PER_LITER_RATE_IN_SLIP";
    public static final String KILO_FAT_RATE_IN_SLIP = "KILO_FAT_RATE_IN_SLIP";
    public static final String SNF_IN_SLIP = "SNF_IN_SLIP";
    public static final String CLR_IN_SLIP = "CLR_IN_SLIP";
    public static final String WATER_IN_SLIP = "WATER_IN_SLIP";
    public static final String PER_FAT_PD_IN_SLIP = "PER_FAT_PD_IN_SLIP";
    public static final String PER_LITER_PD_IN_SLIP = "PER_LITER_PD_IN_SLIP";
    public static final String GOVT_PD_IN_SLIP = "PER_LITER_PD_IN_SLIP";


    public static final String MOBILE_NUMBER = "9829015097";
    public static final String FARM_NAME = "M.P.C.S PVT. LTD";


    // This mail use for support
    public static final String CONTACT_US_MAIL = "demo@gmail.com";
}
