package com.example.dairy.modal;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Result implements Serializable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("userName")
    @Expose
    private String userName;
    @SerializedName("customerName")
    @Expose
    private String customerName;
    @SerializedName("customer_name_hindi")
    @Expose
    private String customer_name_hindi;

    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("centerName")
    @Expose
    private String centerName;
    @SerializedName("dairyName")
    @Expose
    private String dairyName;
    @SerializedName("dairyAddress")
    @Expose
    private String dairyAddress;
    @SerializedName("phoneNumber")
    @Expose
    private String phoneNumber;
    @SerializedName("paymentDate")
    @Expose
    private String paymentDate;
    @SerializedName("updatedAt")
    @Expose
    private String updatedAt;
    @SerializedName("createdAt")
    @Expose
    private String createdAt;
    @SerializedName("memberCode")
    @Expose
    private String memberCode;


    // master key
    @SerializedName("fatherName")
    @Expose
    private String fatherName;
    @SerializedName("accountNumber")
    @Expose
    private String accountNumber;
    @SerializedName("IFSCCode")
    @Expose
    private String iFSCCode;
    @SerializedName("bankName")
    @Expose
    private String bankName;
    @SerializedName("villageName")
    @Expose
    private String villageName;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("DairyId")
    @Expose
    private Integer dairyId;


    @SerializedName("memberID")
    @Expose
    private String memberID;
    @SerializedName("liter")
    @Expose
    private String liter;
    @SerializedName("fat")
    @Expose
    private String fat;
    @SerializedName("snf")
    @Expose
    private String snf;
    @SerializedName("rate")
    @Expose
    private String rate;
    @SerializedName("amount")
    @Expose
    private String amount;
    @SerializedName("addDate")
    @Expose
    private String addDate;
    @SerializedName("timeslot")
    @Expose
    private String timeslot;
    @SerializedName("animalType")
    @Expose
    private String animalType;





    @SerializedName("saleDate")
    @Expose
    private String saleDate;
    @SerializedName("totalAmount")
    @Expose
    private String totalAmount;
    @SerializedName("CustomerId")
    @Expose
    private Integer customerId;
    @SerializedName("Customer")
    @Expose
    private Customer customer;

    @SerializedName("memberType")
    @Expose
    private String memberType;

    @SerializedName("fat_pd")
    @Expose
    private String fatPd;

    @SerializedName("liter_pd")
    @Expose
    private String literPd;

    @SerializedName("gov_pd")
    @Expose
    private String govPd;

    @SerializedName("total_rate_with_pd")
    @Expose
    private String totalRateWithPd;

    @SerializedName("local_milk_sale_rate_for_cow")
    @Expose
    private String localMilkSaleRateForCow;

    @SerializedName("local_milk_sale_rate_for_buffalo")
    @Expose
    private String localMilkSaleRateForBuffalo;

    @SerializedName("durationDate")
    @Expose
    private String durationDate;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCenterName() {
        return centerName;
    }

    public void setCenterName(String centerName) {
        this.centerName = centerName;
    }

    public String getDairyName() {
        return dairyName;
    }

    public void setDairyName(String dairyName) {
        this.dairyName = dairyName;
    }

    public String getDairyAddress() {
        return dairyAddress;
    }

    public void setDairyAddress(String dairyAddress) {
        this.dairyAddress = dairyAddress;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getMemberID() {
        return memberID;
    }

    public void setMemberID(String memberID) {
        this.memberID = memberID;
    }

    public String getLiter() {
        return liter;
    }

    public void setLiter(String liter) {
        this.liter = liter;
    }

    public String getFat() {
        return fat;
    }

    public void setFat(String fat) {
        this.fat = fat;
    }

    public String getSnf() {
        return snf;
    }

    public void setSnf(String snf) {
        this.snf = snf;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getAddDate() {
        return addDate;
    }

    public void setAddDate(String addDate) {
        this.addDate = addDate;
    }

    public String getTimeslot() {
        return timeslot;
    }

    public void setTimeslot(String timeslot) {
        this.timeslot = timeslot;
    }

    public String getAnimalType() {
        return animalType;
    }

    public void setAnimalType(String animalType) {
        this.animalType = animalType;
    }

    public String getFatherName() {
        return fatherName;
    }

    public void setFatherName(String fatherName) {
        this.fatherName = fatherName;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getiFSCCode() {
        return iFSCCode;
    }

    public void setiFSCCode(String iFSCCode) {
        this.iFSCCode = iFSCCode;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getVillageName() {
        return villageName;
    }

    public void setVillageName(String villageName) {
        this.villageName = villageName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getDairyId() {
        return dairyId;
    }

    public void setDairyId(Integer dairyId) {
        this.dairyId = dairyId;
    }

    public String getSaleDate() {
        return saleDate;
    }

    public void setSaleDate(String saleDate) {
        this.saleDate = saleDate;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        this.totalAmount = totalAmount;
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public String getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(String paymentDate) {
        this.paymentDate = paymentDate;
    }

    public String getMemberCode() {
        return memberCode;
    }

    public void setMemberCode(String memberCode) {
        this.memberCode = memberCode;
    }

    public String getMemberType() {
        return memberType;
    }

    public void setMemberType(String memberType) {
        this.memberType = memberType;
    }

    public String getLocalMilkSaleRateForCow() {
        return localMilkSaleRateForCow;
    }

    public void setLocalMilkSaleRateForCow(String localMilkSaleRateForCow) {
        this.localMilkSaleRateForCow = localMilkSaleRateForCow;
    }

    public String getLocalMilkSaleRateForBuffalo() {
        return localMilkSaleRateForBuffalo;
    }

    public void setLocalMilkSaleRateForBuffalo(String localMilkSaleRateForBuffalo) {
        this.localMilkSaleRateForBuffalo = localMilkSaleRateForBuffalo;
    }

    public String getFatPd() {
        return fatPd;
    }

    public void setFatPd(String fatPd) {
        this.fatPd = fatPd;
    }

    public String getLiterPd() {
        return literPd;
    }

    public void setLiterPd(String literPd) {
        this.literPd = literPd;
    }

    public String getGovPd() {
        return govPd;
    }

    public void setGovPd(String govPd) {
        this.govPd = govPd;
    }

    public String getTotalRateWithPd() {
        return totalRateWithPd;
    }

    public String getCustomer_name_hindi() {
        return customer_name_hindi;
    }

    public void setCustomer_name_hindi(String customer_name_hindi) {
        this.customer_name_hindi = customer_name_hindi;
    }

    public void setTotalRateWithPd(String totalRateWithPd) {
        this.totalRateWithPd = totalRateWithPd;
    }

    public String getDurationDate() {
        return durationDate;
    }

    public void setDurationDate(String durationDate) {
        this.durationDate = durationDate;
    }
}