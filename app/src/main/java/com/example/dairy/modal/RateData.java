package com.example.dairy.modal;

import java.io.Serializable;

public class RateData implements Serializable {

    float fatRate = 0f;
    float rate = 0f;

    public RateData( float fatRate ,
            float rate){
        this.fatRate = fatRate;
        this.rate = rate;
    }

    public float getFatRate() {
        return fatRate;
    }

    public void setFatRate(float fatRate) {
        this.fatRate = fatRate;
    }

    public float getRate() {
        return rate;
    }

    public void setRate(float rate) {
        this.rate = rate;
    }
}
