package com.example.dairy.modal;

import com.example.dairy.utils.Constants;

import java.io.Serializable;

public class Categories implements Serializable {

    int image;
    String title;
    Constants.CATEGORIES_CLICK_TYPE optionType;

    public Categories(int image,
                      String title,
                      Constants.CATEGORIES_CLICK_TYPE optionType) {
        this.image = image;
        this.title = title;
        this.optionType = optionType;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Constants.CATEGORIES_CLICK_TYPE getOptionType() {
        return optionType;
    }

    public void setOptionType(Constants.CATEGORIES_CLICK_TYPE optionType) {
        this.optionType = optionType;
    }
}
