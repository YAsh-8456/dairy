package com.example.dairy.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.dairy.R;
import com.example.dairy.databinding.ListMilkSellItemBinding;
import com.example.dairy.modal.Result;
import com.example.dairy.utils.Constants;
import com.example.dairy.utils.MemberClickInterface;
import com.example.dairy.utils.Utils;

import java.util.ArrayList;
import java.util.List;


public class MilkSellerAdapter extends RecyclerView.Adapter<MilkSellerAdapter.ViewHolder> {

    private Context context;
    private MemberClickInterface memberClickInterface;
    private int detailViewOpenPosition = -1;

    public List<Result> list = new ArrayList<>();
    private boolean isClickable = true;

    public MilkSellerAdapter(Context context, MemberClickInterface memberClickInterface) {
        this.context = context;
        this.memberClickInterface = memberClickInterface;
    }

    public void manageViewClick(boolean isClickable) {
        this.isClickable = isClickable;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ListMilkSellItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.list_milk_sell_item, parent, false);
        return new ViewHolder(binding.getRoot());
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        // manage userInfo
//        if(holder.getAdapterPosition() % 3 == 0){
//            holder.binding.includeUserInfo.mainView.setVisibility(View.VISIBLE);
//        }else {
//            holder.binding.includeUserInfo.mainView.setVisibility(View.GONE);
//        }
//
//        // manage summary details
//        if(holder.getAdapterPosition() != 0 && holder.getAdapterPosition() % 2 == 0){
//            holder.binding.layoutSummary.mainView.setVisibility(View.VISIBLE);
//        }else {
//            holder.binding.layoutSummary.mainView.setVisibility(View.GONE);
//        }
//
//        holder.binding.llDetailView.setVisibility(View.GONE);

        final Result result = list.get(position);
        final Result data = list.get(position);

        Glide
                .with(context)
                .load(Constants.BUFFALO.toUpperCase().equals(data.getAnimalType().toUpperCase()) ? R.drawable.buffalo : R.drawable.cow)
                .placeholder(R.drawable.no_image)
                .into(holder.binding.ivPet);

        final int recordPosition = position + 1;
        holder.binding.tvRecordId.setText("" + recordPosition);
        holder.binding.tvMemberId.setText(data.getCustomer() != null && Utils.isNotEmpty(data.getCustomer().getMemberCode()) ? "" + data.getCustomer().getMemberCode() : "--");
        holder.binding.tvMemberName.setText(data.getCustomer() != null && Utils.isNotEmpty(data.getCustomer().getCustomerName()) ? "" + data.getCustomer().getCustomerName() : "--");
        holder.binding.tvTotalAmount.setText(Utils.isNotEmpty(data.getTotalAmount()) ? "" + data.getTotalAmount() : "--");
        holder.binding.tvQty.setText(Utils.isNotEmpty(data.getLiter()) ? "" + data.getLiter() : "--");

        if (detailViewOpenPosition == position) {
            holder.binding.llDetailView.setVisibility(View.VISIBLE);
        } else {
            holder.binding.llDetailView.setVisibility(View.GONE);
        }


        holder.binding.llMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (isClickable) {
                    if (detailViewOpenPosition == holder.getAdapterPosition()) {
                        detailViewOpenPosition = -1;
                    } else {
                        detailViewOpenPosition = holder.getAdapterPosition();
                    }
                    notifyDataSetChanged();
                } else {
                    memberClickInterface.view(result, holder.getAdapterPosition());
                }

            }
        });

        holder.binding.llDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (detailViewOpenPosition == holder.getAdapterPosition()) {
                    detailViewOpenPosition = -1;
                } else {
                    detailViewOpenPosition = holder.getAdapterPosition();
                }

                memberClickInterface.delete(result, holder.getAdapterPosition());
                list.remove(holder.getAdapterPosition());
                notifyItemRemoved(holder.getAdapterPosition());
            }
        });

        holder.binding.llEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (detailViewOpenPosition == holder.getAdapterPosition()) {
                    detailViewOpenPosition = -1;
                } else {
                    detailViewOpenPosition = holder.getAdapterPosition();
                }

                memberClickInterface.edit(result, holder.getAdapterPosition());
                notifyDataSetChanged();
//                Toast.makeText(context, "Working progress", Toast.LENGTH_SHORT).show();
            }
        });

        holder.binding.llView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //  Toast.makeText(context, "Working progress", Toast.LENGTH_SHORT).show();
                if (detailViewOpenPosition == holder.getAdapterPosition()) {
                    detailViewOpenPosition = -1;
                } else {
                    detailViewOpenPosition = holder.getAdapterPosition();
                }
                notifyDataSetChanged();
                memberClickInterface.view(result, holder.getAdapterPosition());
            }
        });


        holder.binding.llPrint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                memberClickInterface.print(result, holder.getAdapterPosition());
            }
        });


    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void addData(List<Result> data) {
        list.clear();
        list.addAll(data);
        notifyDataSetChanged();
    }

    public void addSingleRecord(Result data) {
        list.add(data);
        notifyDataSetChanged();
    }

    public void editData(Result data, int position) {
        list.set(position, data);
        //list.add(position, data);
        detailViewOpenPosition = -1;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ListMilkSellItemBinding binding;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);
        }
    }
}
