package com.example.dairy.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.dairy.R;
import com.example.dairy.databinding.ListMilkBuyItem2Binding;
import com.example.dairy.modal.Result;
import com.example.dairy.utils.Constants;
import com.example.dairy.utils.MemberClickInterface;
import com.example.dairy.utils.Utils;

import java.util.ArrayList;
import java.util.List;


public class MilkBuyAdapter extends RecyclerView.Adapter<MilkBuyAdapter.ViewHolder> {

    private Context context;
    private MemberClickInterface memberClickInterface;
    private int detailViewOpenPosition = -1;
    private boolean isClickable = true;

    public List<Result> list = new ArrayList<>();

    public MilkBuyAdapter(Context context, MemberClickInterface memberClickInterface) {
        this.context = context;
        this.memberClickInterface = memberClickInterface;
    }

    public void manageViewClick(boolean isClickable) {
        this.isClickable = isClickable;
    }

    @NonNull
    @Override
    public MilkBuyAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ListMilkBuyItem2Binding binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.list_milk_buy_item2, parent, false);
        return new ViewHolder(binding.getRoot());
    }

    @Override
    public void onBindViewHolder(@NonNull final MilkBuyAdapter.ViewHolder holder, final int position) {

        final Result result = list.get(position);

//        Glide
//                .with(context)
//                .load(list.get(position).getImage())
//                .placeholder(R.drawable.no_image)
//                .centerCrop()
//                .into(holder.binding.ivCategories);
//
//        holder.binding.tvCategoriesName.setText(list.get(position).getTitle());
//
//        holder.binding.llMainView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                categoriesInterface.categoriesClick(Constants.CATEGORIES_CLICK_TYPE.ADVANCE_PAYMENT);
//            }
//        });

        final Result data = list.get(position);

        Glide
                .with(context)
                .load(Constants.BUFFALO.toUpperCase().equals(data.getAnimalType().toUpperCase()) ? R.drawable.buffalo : R.drawable.cow)
                .placeholder(R.drawable.no_image)
                .into(holder.binding.ivPet);

        final int recordPosition  = position +1;
        holder.binding.tvRecordId.setText(""+ recordPosition);
        holder.binding.tvMemberId.setText(data.getCustomer() != null && Utils.isNotEmpty(data.getCustomer().getMemberCode()) ? "" + data.getCustomer().getMemberCode() : "--");
//        if (data.getCustomer() != null) {
//            holder.binding.tvMemberName.setText(Utils.isNotEmpty(data.getCustomer().getCustomerName()) ? "" + data.getCustomer().getCustomerName() : "--");
//        }
        holder.binding.tvTotalAmount.setText(Utils.isNotEmpty(data.getAmount()) ? "" + data.getAmount() : "--");
        holder.binding.tvFAT.setText(Utils.isNotEmpty(data.getFat()) ? "" + data.getFat() : "--");
        holder.binding.tvSNF.setText(Utils.isNotEmpty(data.getSnf()) ? "" + data.getSnf() : "--");
        holder.binding.tvQty.setText(Utils.isNotEmpty(data.getLiter()) ? "" + data.getLiter() : "--");


        if (detailViewOpenPosition == position) {
            holder.binding.llDetailView.setVisibility(View.VISIBLE);
        } else {
            holder.binding.llDetailView.setVisibility(View.GONE);
        }


        holder.binding.llMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isClickable) {
                    if (detailViewOpenPosition == holder.getAdapterPosition()) {
                        detailViewOpenPosition = -1;
                    } else {
                        detailViewOpenPosition = holder.getAdapterPosition();
                    }
                    notifyDataSetChanged();
                } else {
                    memberClickInterface.view(result, position);
                }
            }
        });


        holder.binding.llDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (detailViewOpenPosition == holder.getAdapterPosition()) {
                    detailViewOpenPosition = -1;
                } else {
                    detailViewOpenPosition = holder.getAdapterPosition();
                }
                notifyDataSetChanged();
                memberClickInterface.delete(result, position);
                list.remove(position);
                notifyItemRemoved(position);
            }
        });

        holder.binding.llEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (detailViewOpenPosition == holder.getAdapterPosition()) {
                    detailViewOpenPosition = -1;
                } else {
                    detailViewOpenPosition = holder.getAdapterPosition();
                }
                notifyDataSetChanged();
                memberClickInterface.edit(result, position);
//                Toast.makeText(context, "Working progress", Toast.LENGTH_SHORT).show();
            }
        });

        holder.binding.llView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (detailViewOpenPosition == holder.getAdapterPosition()) {
                    detailViewOpenPosition = -1;
                } else {
                    detailViewOpenPosition = holder.getAdapterPosition();
                }
                notifyDataSetChanged();
                memberClickInterface.view(result, position);
                // Toast.makeText(context, "Working progress", Toast.LENGTH_SHORT).show();
            }
        });


        holder.binding.llPrint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                memberClickInterface.print(result, position);
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void addData(List<Result> data) {
        list.clear();
        list.addAll(data);
        notifyDataSetChanged();
    }

    public void addSingleRecord(Result data) {
        list.add(data);
        notifyDataSetChanged();
    }

    public void editData(Result data, int position) {
        list.set(position, data);
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ListMilkBuyItem2Binding binding;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);
        }
    }
}
