package com.example.dairy.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.example.dairy.R;
import com.example.dairy.databinding.ListMasterItemBinding;
import com.example.dairy.modal.Result;
import com.example.dairy.utils.CategoriesInterface;
import com.example.dairy.utils.Constants;
import com.example.dairy.utils.MemberClickInterface;
import com.example.dairy.utils.Utils;

import java.util.ArrayList;
import java.util.List;


public class MasterAdapter extends RecyclerView.Adapter<MasterAdapter.ViewHolder> {

    private Context context;
    private CategoriesInterface categoriesInterface;
    private int detailViewOpenPosition = -1;
    private List<Result> list = new ArrayList<>();
    public List<Result> masterList = new ArrayList<>();
    MemberClickInterface memberClickInterface;
    private boolean searchMember = false;
    private boolean isClickable = true;

    public MasterAdapter(Context context, MemberClickInterface memberClickInterface) {
        this.context = context;
        this.memberClickInterface = memberClickInterface;
    }

    public void manageViewClick(boolean isClickable) {
        this.isClickable = isClickable;
    }

    public void isSearchMember(boolean isSearchMember) {
        this.searchMember = isSearchMember;
    }

    @NonNull
    @Override
    public MasterAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ListMasterItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.list_master_item, parent, false);
        return new ViewHolder(binding.getRoot());
    }

    @Override
    public void onBindViewHolder(@NonNull final MasterAdapter.ViewHolder holder, int position) {

//        Glide
//                .with(context)
//                .load(list.get(position).getImage())
//                .placeholder(R.drawable.no_image)
//                .centerCrop()
//                .into(holder.binding.ivCategories);
//

//
//        holder.binding.llMainView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                categoriesInterface.categoriesClick(Constants.CATEGORIES_CLICK_TYPE.ADVANCE_PAYMENT);
//            }
//        });

        final Result data = list.get(position);

        holder.binding.memberId.setText("" + data.getMemberCode());
//        if(PreferenceManager.getValue(Constants.SELECTED_LANGUAGE).toUpperCase().equals("HI")){
//            holder.binding.tvMemberName.setText(Utils.isNotEmpty(data.getCustomer_name_hindi()) ? data.getCustomer_name_hindi() : "--");
//        }else {
//            holder.binding.tvMemberName.setText(Utils.isNotEmpty(data.getCustomerName()) ? data.getCustomerName() : "--");
//        }

        holder.binding.tvMemberName.setText(Utils.isNotEmpty(data.getCustomerName()) ? data.getCustomerName() : "--");
        holder.binding.tvMemberNumber.setText(Utils.isNotEmpty(data.getPhoneNumber()) ? data.getPhoneNumber() : "--");

        if (Utils.isNotEmpty(data.getMemberType())) {

            if (data.getMemberType().toUpperCase().equals(Constants.ALL)) {
                holder.binding.tvMemberType.setText(context.getResources().getString(R.string.both).toUpperCase());
                holder.binding.tvMemberType.setBackground(context.getResources().getDrawable(R.drawable.background_yellow));

            } else if (data.getMemberType().toUpperCase().equals("SELLER")) {
                holder.binding.tvMemberType.setText(context.getResources().getString(R.string.seller).toUpperCase());
                holder.binding.tvMemberType.setBackground(context.getResources().getDrawable(R.drawable.background_red));

            } else if (data.getMemberType().toUpperCase().equals(Constants.BUYER)) {
                holder.binding.tvMemberType.setBackground(context.getResources().getDrawable(R.drawable.background_green));
                holder.binding.tvMemberType.setText(context.getResources().getString(R.string.buyer).toUpperCase());
            }
        }

        if (detailViewOpenPosition == position) {
            holder.binding.llDetailView.setVisibility(View.VISIBLE);
        } else {
            holder.binding.llDetailView.setVisibility(View.GONE);
        }


        holder.binding.llMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (searchMember || !isClickable) {
                    memberClickInterface.view(data, holder.getAdapterPosition());
                } else {
                    if (detailViewOpenPosition == holder.getAdapterPosition()) {
                        detailViewOpenPosition = -1;
                    } else {
                        detailViewOpenPosition = holder.getAdapterPosition();
                    }
                    notifyDataSetChanged();
                }

            }
        });


        holder.binding.llDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                memberClickInterface.delete(data, holder.getAdapterPosition());
                list.remove(holder.getAdapterPosition());
                notifyItemRemoved(holder.getAdapterPosition());
            }
        });

        holder.binding.llEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                memberClickInterface.edit(data, holder.getAdapterPosition());

            }
        });

        holder.binding.llView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                memberClickInterface.view(data, holder.getAdapterPosition());

            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public void editSingleRecord(int position, Result data) {
        list.set(position, data);
        masterList.set(position, data);
        detailViewOpenPosition = -1;
        notifyDataSetChanged();
    }

    public void addSingleRecord(Result data) {
        list.add(data);
        masterList.add(data);
        detailViewOpenPosition = -1;
        notifyDataSetChanged();
    }


    public void addData(List<Result> data) {
        list.clear();
        masterList.clear();
        list.addAll(data);
        masterList.addAll(data);
        notifyDataSetChanged();
    }

    public void filter(String text) {
        //new array list that will hold the filtered data
        List<Result> filterList = new ArrayList<>();

        //looping through existing elements
        if (Utils.isNotEmpty(text)) {
            for (Result result : masterList) {
                //if the existing elements contains the search input
                if (result.getCustomerName().toLowerCase().contains(text.toLowerCase()) || result.getPhoneNumber().toLowerCase().contains(text.toLowerCase()) || String.valueOf(result.getId()).toLowerCase().contains(text.toLowerCase())) {
                    //adding the element to filtered list
                    filterList.add(result);
                }
            }
        } else {
            filterList = masterList;
        }


        this.list = filterList;
        notifyDataSetChanged();
    }

    public List<Result> getMemberList() {
        return masterList;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ListMasterItemBinding binding;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);
        }
    }
}
