package com.example.dairy.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.example.dairy.R;
import com.example.dairy.databinding.ListRateChartBinding;
import com.example.dairy.utils.CategoriesInterface;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


public class RateChatAdapter extends RecyclerView.Adapter<RateChatAdapter.ViewHolder> {

    private Context context;
    private CategoriesInterface categoriesInterface;
    private int SNFPosition  = -1;

    private List<Map<Integer, Object>> list = new ArrayList<>();

    public RateChatAdapter(Context context, int SNFPosition) {
        this.context = context;
        this.SNFPosition = SNFPosition;
    }

    @NonNull
    @Override
    public RateChatAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ListRateChartBinding binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.list_rate_chart, parent, false);
        return new ViewHolder(binding.getRoot());
    }

    @Override
    public void onBindViewHolder(@NonNull final RateChatAdapter.ViewHolder holder, int position) {
        // FAT rate always get in 0 position
        Map<Integer, Object> data = list.get(position);
        holder.binding.tvFAT.setText(String.valueOf(data.get(0)));
        holder.binding.tvRate.setText(String.valueOf(data.get(SNFPosition)));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void addData(List<Map<Integer, Object>> data) {
        list.clear();
        list.addAll(data);
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ListRateChartBinding binding;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);
        }
    }
}
