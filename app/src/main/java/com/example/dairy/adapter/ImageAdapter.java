package com.example.dairy.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.dairy.R;
import com.example.dairy.databinding.ItemImagePageBinding;
import com.example.dairy.utils.Debug;
import com.github.islamkhsh.CardSliderAdapter;

import java.util.ArrayList;
import java.util.List;


public class ImageAdapter extends CardSliderAdapter<ImageAdapter.viewHolder> {

    private Context context;
    boolean isBanner = true;
    private List<Integer> list = new ArrayList<>();
    private String TAG = "ImageAdapter";

    public ImageAdapter(Context context, boolean isBanner) {
        this.context = context;
        this.isBanner = isBanner;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public viewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemImagePageBinding binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.item_image_page, parent, false);
        return new viewHolder(binding.getRoot());
    }

    @Override
    public void bindVH(viewHolder viewHolder, int i) {

        Debug.e(TAG, "bindVH: " + list.get(i));
        Glide
                .with(context)
                .load(isBanner ? list.get(i) :"")
                .placeholder(R.drawable.no_image)
                .fitCenter()
                .into(viewHolder.binding.ivBanner);
    }


    public void addData(List<Integer> data) {
        list.clear();
        list.addAll(data);
        notifyDataSetChanged();
    }

    public class viewHolder extends RecyclerView.ViewHolder {

        ItemImagePageBinding binding;

        public viewHolder(@NonNull View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);
        }
    }
}