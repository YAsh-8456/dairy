package com.example.dairy.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.dairy.R;
import com.example.dairy.databinding.ListOptionItemBinding;
import com.example.dairy.databinding.ListPrinterTestBinding;
import com.example.dairy.modal.Categories;
import com.example.dairy.utils.CategoriesInterface;
import com.example.dairy.utils.Constants;
import com.example.dairy.utils.PrintTextInterface;

import java.util.ArrayList;
import java.util.List;


public class PrintTestAdapter extends RecyclerView.Adapter<PrintTestAdapter.ViewHolder> {

    private Context context;
    private final PrintTextInterface printTextInterface;

    private List<Constants.PRINT_TYPE> list = new ArrayList<>();

    public PrintTestAdapter(Context context,PrintTextInterface printTextInterface) {
        this.context = context;
        this.printTextInterface = printTextInterface;
    }

    @NonNull
    @Override
    public PrintTestAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ListPrinterTestBinding binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.list_printer_test, parent, false);
        return new ViewHolder(binding.getRoot());
    }

    @Override
    public void onBindViewHolder(@NonNull final PrintTestAdapter.ViewHolder holder, int position) {

        holder.binding.tvCategoriesName.setText(list.get(position).toString());
        holder.binding.llMainView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                printTextInterface.printTextClick(list.get(holder.getAdapterPosition()));
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void addData(List<Constants.PRINT_TYPE> data) {
        list.clear();
        list.addAll(data);
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ListPrinterTestBinding binding;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);
        }
    }
}
