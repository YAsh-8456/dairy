package com.example.dairy.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.example.dairy.R;
import com.example.dairy.databinding.ListAdvancePaymentItemBinding;
import com.example.dairy.modal.Result;
import com.example.dairy.utils.MemberClickInterface;
import com.example.dairy.utils.Utils;

import java.util.ArrayList;
import java.util.List;


public class AdvancePaymentAdapter extends RecyclerView.Adapter<AdvancePaymentAdapter.ViewHolder> {

    private Context context;
    private MemberClickInterface memberClickInterface;
    private int detailViewOpenPosition = -1;
    private boolean isClickable = true;

    public List<Result> list = new ArrayList<>();

    public AdvancePaymentAdapter(Context context, MemberClickInterface memberClickInterface) {
        this.context = context;
        this.memberClickInterface = memberClickInterface;
    }

    public void manageViewClick(boolean isClickable) {
        this.isClickable = isClickable;
    }

    @NonNull
    @Override
    public AdvancePaymentAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ListAdvancePaymentItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.list_advance_payment_item, parent, false);
        return new ViewHolder(binding.getRoot());
    }

    @Override
    public void onBindViewHolder(@NonNull final AdvancePaymentAdapter.ViewHolder holder, int position) {

//        final Result result = list.get(position);
//
//
//        final Result data = list.get(position);
//        final int recordPosition = holder.getAdapterPosition() + 1;
//        holder.binding.tvRecordId.setText("" + recordPosition);
//        holder.binding.tvMemberId.setText(Utils.isNotEmpty(data.getCustomer().getMemberCode()) ? "" + data.getCustomer().getMemberCode() : "--");
//        holder.binding.tvGiveDate.setText(Utils.isNotEmpty(data.getPaymentDate()) ? "" + Utils.finalAdvancePaymentDateFormat(data.getPaymentDate()) : "--");
//        holder.binding.tvDeductionDate.setText(Utils.isNotEmpty(data.getDurationDate()) ? "" + Utils.finalAdvancePaymentDateFormat(data.getDurationDate()) : "--");
//        holder.binding.tvTotalAmount.setText(Utils.isNotEmpty(data.getAmount()) ? "" + data.getAmount() : "--");


        // manage summary details
        if(3 == holder.getAdapterPosition()){
            holder.binding.layoutSummary.mainView.setVisibility(View.VISIBLE);
        }else {
            holder.binding.layoutSummary.mainView.setVisibility(View.GONE);
        }

        holder.binding.llDetailView.setVisibility(View.GONE);
//        if (detailViewOpenPosition == position) {
//            holder.binding.llDetailView.setVisibility(View.VISIBLE);
//        } else {
//            holder.binding.llDetailView.setVisibility(View.GONE);
//        }
//
//
//        holder.binding.llMain.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (isClickable) {
//                    if (detailViewOpenPosition == holder.getAdapterPosition()) {
//                        detailViewOpenPosition = -1;
//                    } else {
//                        detailViewOpenPosition = holder.getAdapterPosition();
//                    }
//                    notifyDataSetChanged();
//                } else {
//                    memberClickInterface.view(result, holder.getAdapterPosition());
//                }
//            }
//        });
//
//
//        holder.binding.llDelete.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (detailViewOpenPosition == holder.getAdapterPosition()) {
//                    detailViewOpenPosition = -1;
//                } else {
//                    detailViewOpenPosition = holder.getAdapterPosition();
//                }
//                notifyDataSetChanged();
//                memberClickInterface.delete(result, holder.getAdapterPosition());
//                list.remove(holder.getAdapterPosition());
//                notifyItemRemoved(holder.getAdapterPosition());
//            }
//        });
//
//        holder.binding.llEdit.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (detailViewOpenPosition == holder.getAdapterPosition()) {
//                    detailViewOpenPosition = -1;
//                } else {
//                    detailViewOpenPosition = holder.getAdapterPosition();
//                }
//                notifyDataSetChanged();
//                memberClickInterface.edit(result, holder.getAdapterPosition());
////                Toast.makeText(context, "Working progress", Toast.LENGTH_SHORT).show();
//            }
//        });
//
//        holder.binding.llView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (detailViewOpenPosition == holder.getAdapterPosition()) {
//                    detailViewOpenPosition = -1;
//                } else {
//                    detailViewOpenPosition = holder.getAdapterPosition();
//                }
//                notifyDataSetChanged();
//                memberClickInterface.view(result, holder.getAdapterPosition());
//                // Toast.makeText(context, "Working progress", Toast.LENGTH_SHORT).show();
//            }
//        });
//
//
//        holder.binding.llPrint.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                memberClickInterface.print(result, holder.getAdapterPosition());
//                Toast.makeText(context, "Working progress", Toast.LENGTH_SHORT).show();
//            }
//        });

    }

    @Override
    public int getItemCount() {
        return 4;
    }

    public void addData(List<Result> data) {
        list.clear();
        list.addAll(data);
        notifyDataSetChanged();
    }

    public void addSingleRecord(Result data) {
        list.add(data);
        notifyDataSetChanged();
    }

    public void editData(Result data, int position) {
        list.set(position, data);
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ListAdvancePaymentItemBinding binding;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);
        }
    }
}
