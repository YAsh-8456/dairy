package com.example.dairy.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.dairy.R;
import com.example.dairy.databinding.ListOptionItemBinding;
import com.example.dairy.modal.Categories;
import com.example.dairy.utils.CategoriesInterface;

import java.util.ArrayList;
import java.util.List;


public class CategoriesAdapter extends RecyclerView.Adapter<CategoriesAdapter.ViewHolder> {

    private Context context;
    private CategoriesInterface categoriesInterface;

    private List<Categories> list = new ArrayList<>();

    public CategoriesAdapter(Context context, CategoriesInterface categoriesInterface) {
        this.context = context;
        this.categoriesInterface = categoriesInterface;
    }

    @NonNull
    @Override
    public CategoriesAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ListOptionItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.list_option_item, parent, false);
        return new ViewHolder(binding.getRoot());
    }

    @Override
    public void onBindViewHolder(@NonNull final CategoriesAdapter.ViewHolder holder, int position) {

        Glide
                .with(context)
                .load(list.get(position).getImage())
                .centerCrop()
                .into(holder.binding.ivCategories);

        holder.binding.tvCategoriesName.setText(list.get(position).getTitle());

        holder.binding.llMainView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                categoriesInterface.categoriesClick(list.get(holder.getAdapterPosition()).getOptionType());
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void addData(List<Categories> data) {
        list.clear();
        list.addAll(data);
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ListOptionItemBinding binding;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);
        }
    }
}
