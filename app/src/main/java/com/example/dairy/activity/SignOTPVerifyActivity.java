package com.example.dairy.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;

import androidx.databinding.DataBindingUtil;

import com.example.dairy.R;
import com.example.dairy.databinding.ActivityOTPVerifyBinding;
import com.example.dairy.modal.CommonResponse;
import com.example.dairy.utils.Constants;
import com.example.dairy.utils.Debug;
import com.example.dairy.utils.PreferenceManager;
import com.example.dairy.utils.Utils;
import com.example.dairy.webService.APIController;
import com.example.dairy.webService.ResponseInterface;


public class SignOTPVerifyActivity extends MyAppBaseActivity implements View.OnClickListener, ResponseInterface {

    private static final String TAG = "OTPActivity";
    ActivityOTPVerifyBinding binding;
    private APIController apiController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_o_t_p_verify);


        binding.ivBack.setOnClickListener(this);
        binding.resendOTP.setOnClickListener(this);
        binding.verification.setOnClickListener(this);

        manageEditTextPosition();

        apiController = new APIController(this);
    }

    private void manageEditTextPosition() {

        binding.etFist.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if (s.length() > 0) {
                    binding.etSecond.requestFocus();
                } else {
                    binding.etFist.requestFocus();
                }

            }
        });


        binding.etSecond.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if (s.length() > 0) {
                    binding.etThird.requestFocus();
                } else {
                    binding.etFist.requestFocus();
                }

            }
        });


        binding.etThird.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if (s.length() > 0) {
                    binding.etForth.requestFocus();
                } else {
                    binding.etSecond.requestFocus();
                }

            }
        });

        binding.etForth.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if (s.length() > 0) {

                } else {
                    binding.etThird.requestFocus();
                }

            }
        });


    }

    private void callOTPApi() {

        String etFullName = getIntent().getStringExtra("name");
        String etEmail = getIntent().getStringExtra("email");
        String etPhoneNumber = getIntent().getStringExtra("phone_no");
        String etCompanyName = getIntent().getStringExtra("company_name");
        String etCompanyAddress = getIntent().getStringExtra("company_address");
        String etGSTNo = getIntent().getStringExtra("gst_no");
        String etBirthday = getIntent().getStringExtra("company_birth_date");
        String password = getIntent().getStringExtra("password");

        String code1 = binding.etFist.getText().toString().trim();
        String code2 = binding.etSecond.getText().toString().trim();
        String code3 = binding.etThird.getText().toString().trim();
        String code4 = binding.etForth.getText().toString().trim();

        String otpCode = code1 + code2 + code3 + code4;

        if (binding.etFist.length() == 1 && binding.etSecond.length() == 1 && binding.etThird.length() == 1 && binding.etForth.length() == 1) {

            Intent openMain = new Intent(SignOTPVerifyActivity.this, MainActivity.class);
            startActivity(openMain);
            finishAffinity();


//            HashMap<String, String> hashMap = new HashMap<>();
//            hashMap.put("otp", otpCode);
//            hashMap.put("name", etFullName);
//            hashMap.put("email", etEmail);
//            hashMap.put("phone_no", etPhoneNumber);
//            hashMap.put("password", password);
//            hashMap.put("company_name", etCompanyName);
//            hashMap.put("company_address", etCompanyAddress);
//            hashMap.put("company_birth_date", etBirthday);
//            hashMap.put("gst_no", etGSTNo);
//            hashMap.put("device_type", "A");
//            hashMap.put("device_token", Utils.isNotEmpty(FCMToken) ? FCMToken : "test");
//
//            if (!Utils.isInternetConnected(OTPVerify.this)) {
//                AlertMassage.MassageAlert(this, getResources().getString(R.string.message_intent_conection));
//                return;
//            }
//            String token = getIntent().getStringExtra("token");
//            binding.llProgressBar.setVisibility(View.VISIBLE);
//            Utils.hideSoftKeyword(OTPVerify.this);
//            apiController.verifyApi(Constants.API_TYPE.VERIFY, token, hashMap);
        } else {
            Utils.showSnackbar(SignOTPVerifyActivity.this, binding.llMainLayout, getResources().getString(R.string.message_enter_otp));
        }

    }


    @Override
    public void successResponse(Constants.API_TYPE type, Object response, int code) {
        binding.llProgressBar.setVisibility(View.GONE);

        switch (type) {
            case VERIFY:
                try {
                    if (code == Constants.RESULT_OK) {
                        CommonResponse verifyResponse = ((retrofit2.Response<CommonResponse>) response).body();
                        if (verifyResponse != null) {
                            Debug.e(TAG, "OTP verification response :--->" + verifyResponse.toString());
                            PreferenceManager.putValueBoolean(Constants.IS_LOGIN, true);
                            Intent openMain = new Intent(SignOTPVerifyActivity.this, MainActivity.class);
                            startActivity(openMain);
                            finishAffinity();

                        }else {
                            Utils.showSnackbar(this, binding.llMainLayout, verifyResponse.getMessage());
                        }
                    } else {
                        Utils.showSnackbar(this, binding.llMainLayout, "API Error");

                    }
                } catch (Exception e) {
                    Debug.e(TAG, "Login Exception" + e.getMessage());
                }
                break;
        }

    }

    @Override
    public void failureResponse(Constants.API_TYPE type, String error) {
        binding.llProgressBar.setVisibility(View.GONE);
        Debug.e(TAG, "Fail Error =----->" + error);
    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.ivBack:
                onBackPressed();
                break;

            case R.id.resendOTP:
                break;

            case R.id.verification:
                callOTPApi();
                break;
        }
    }
}