package com.example.dairy.activity;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.example.dairy.R;
import com.example.dairy.adapter.ExcelAdapter;
import com.example.dairy.databinding.ActivityUploadChartBinding;
import com.example.dairy.utils.Constants;
import com.example.dairy.utils.CreateChartInterface;
import com.example.dairy.utils.Debug;
import com.example.dairy.utils.ExcelUtil;
import com.example.dairy.utils.PreferenceManager;
import com.example.dairy.utils.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class UploadChartActivity extends MyAppBaseActivity implements View.OnClickListener, CreateChartInterface {

    ActivityUploadChartBinding binding;
    public static final String TAG = UploadChartActivity.class.getSimpleName();
    private UploadChartActivity mContext;
    CreateChartInterface createChartInterface;
    private int FILE_SELECTOR_CODE = 10000;
    private int DIR_SELECTOR_CODE = 20000;
    private boolean isBuffalo = false;
    boolean isCreateChart = false;
    ExcelAdapter excelAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_upload_chart);
        isCreateChart = getIntent().getExtras().getBoolean("createChart", false);
        getSupportActionBar().setTitle(isCreateChart ? R.string.uploadChart : R.string.create_chart);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mContext = this;
        createChartInterface = this;
        init();
    }

    private void init() {
        binding.llBuffaloLayout.setOnClickListener(this);
        binding.llCowLayout.setOnClickListener(this);
        binding.tvUploadChart.setOnClickListener(this);
        binding.tvCreateChart.setOnClickListener(this);
        binding.tvExportChart.setOnClickListener(this);
        binding.llBuffaloLayout.performClick();

        if (isCreateChart) {
            binding.tvUploadChart.setVisibility(View.GONE);
            binding.tvExportChart.setVisibility(View.GONE);
            binding.tvCreateChart.setVisibility(View.VISIBLE);
            binding.llCreateChart.setVisibility(View.VISIBLE);
        } else {
            binding.llCreateChart.setVisibility(View.GONE);
            binding.tvUploadChart.setVisibility(View.VISIBLE);
            binding.tvExportChart.setVisibility(View.VISIBLE);
            binding.tvCreateChart.setVisibility(View.GONE);
        }

        initViews();
//        setStaticData();
        binding.etFromFAT.addTextChangedListener(setFromFAT);
        binding.etToFAT.addTextChangedListener(setToFAT);

        binding.etFormSNF.addTextChangedListener(setFormSNF);
        binding.etToSNF.addTextChangedListener(setToSNF);

        binding.etFATRate.addTextChangedListener(setFATRate);
        binding.etSNFRate.addTextChangedListener(setSNFRate);

        binding.etCommission.addTextChangedListener(setCommission);
        binding.etDeduction.addTextChangedListener(setDeduction);

       binding.etFatPD.addTextChangedListener(setFATPD);
       binding.etLiterPD.addTextChangedListener(setLiterPD);
        binding.etGovtPD.addTextChangedListener(setGovtPD);

        setStaticData();
    }

    public void initViews() {
        List<Map<Integer, Object>> data = new ArrayList<>();
        excelAdapter = new ExcelAdapter(data);
        binding.excelContentRv.setLayoutManager(new LinearLayoutManager(this));
        binding.excelContentRv.setNestedScrollingEnabled(false);
        binding.excelContentRv.setAdapter(excelAdapter);
    }

    TextWatcher setToFAT = new TextWatcher() {
        @Override
        public void afterTextChanged(Editable s) {
            if (s.toString().length() == 0) {
                binding.etToFAT.setText("0");
            }
        }
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }
    };

    TextWatcher setLiterPD = new TextWatcher() {
        @Override
        public void afterTextChanged(Editable s) {
            if (s.toString().length() == 0) {
                binding.etLiterPD.setText("0");
            }
        }
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }
    };

    TextWatcher setFATPD = new TextWatcher() {
        @Override
        public void afterTextChanged(Editable s) {
            if (s.toString().length() == 0) {
                binding.etFatPD.setText("0");
            }
        }
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }
    };

    TextWatcher setGovtPD = new TextWatcher() {
        @Override
        public void afterTextChanged(Editable s) {
            if (s.toString().length() == 0) {
                binding.etGovtPD.setText("0");
            }
        }
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }
    };

    TextWatcher setToSNF = new TextWatcher() {
        @Override
        public void afterTextChanged(Editable s) {
            if (s.toString().length() == 0) {
                binding.etToSNF.setText("0");
            }
        }
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }
    };

    TextWatcher setFormSNF = new TextWatcher() {
        @Override
        public void afterTextChanged(Editable s) {
            if (s.toString().length() == 0) {
                binding.etFormSNF.setText("0");
            }
        }
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }
    };

    TextWatcher setFATRate = new TextWatcher() {
        @Override
        public void afterTextChanged(Editable s) {
            if (s.toString().length() == 0) {
                binding.etFATRate.setText("0");
            }
        }
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }
    };

    TextWatcher setSNFRate = new TextWatcher() {
        @Override
        public void afterTextChanged(Editable s) {
            if (s.toString().length() == 0) {
                binding.etSNFRate.setText("0");
            }
        }
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }
    };

    TextWatcher setFromFAT = new TextWatcher() {
        @Override
        public void afterTextChanged(Editable s) {
            if (s.toString().length() == 0) {
                binding.etFromFAT.setText("0");
            }
        }
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }
    };

    TextWatcher setCommission = new TextWatcher() {
        @Override
        public void afterTextChanged(Editable s) {
            if (s.toString().length() == 0) {
                binding.etCommission.setText("0");
            }
        }
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }
    };

    TextWatcher setDeduction = new TextWatcher() {
        @Override
        public void afterTextChanged(Editable s) {
            if (s.toString().length() == 0) {
                binding.etDeduction.setText("0");
            }
        }
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }
    };

    public void setStaticData() {

        binding.etFromFAT.setText("5.5");
        binding.etToFAT.setText("9.5");

        binding.etFormSNF.setText("6.5");
        binding.etToSNF.setText("9.5");

        binding.etFATRate.setText("100.00");
        binding.etSNFRate.setText("200.00");

        binding.etCommission.setText("1.00");
        binding.etDeduction.setText("0.10");

        binding.etFatPD.setText("0");
        binding.etLiterPD.setText("0");
        binding.etGovtPD.setText("0");
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.llBuffaloLayout:
                binding.llBuffaloLayout.setBackgroundColor(getResources().getColor(R.color.btn_color));
                binding.llCowLayout.setBackgroundColor(getResources().getColor(R.color.btn_color_gray));
                isBuffalo = true;
                break;

            case R.id.llCowLayout:
                binding.llBuffaloLayout.setBackgroundColor(getResources().getColor(R.color.btn_color_gray));
                binding.llCowLayout.setBackgroundColor(getResources().getColor(R.color.btn_color));
                isBuffalo = false;
                break;

            case R.id.tvUploadChart:
                openFileSelector();
                break;

            case R.id.tvExportChart:
                openFolderSelector();
                break;

            case R.id.tvCreateChart:
                createChart();
                break;
        }


    }

    private void createChart() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                String fromFAT = binding.etFromFAT.getText().toString().trim();
                String toFAT = binding.etToFAT.getText().toString().trim();

                String fromSNF = binding.etFormSNF.getText().toString().trim();
                String toSNF = binding.etToSNF.getText().toString().trim();

                String fatRate = binding.etFATRate.getText().toString().trim();
                String snfRate = binding.etSNFRate.getText().toString().trim();

                String commission = binding.etCommission.getText().toString().trim();
                String deduction = binding.etDeduction.getText().toString().trim();

                String perFAT_PD = binding.etFatPD.getText().toString().trim();
                String perLiterPD = binding.etLiterPD.getText().toString().trim();
                String perGovtPD = binding.etGovtPD.getText().toString().trim();

                if (Utils.isEmpty(fromFAT) || Utils.isEmpty(toFAT) || Utils.isEmpty(fromSNF) || Utils.isEmpty(toSNF) || Utils.isEmpty(fatRate) || Utils.isEmpty(snfRate)) {
                    runOnUiThread(new Runnable() {
                        public void run() {
                            Toast.makeText(mContext, "Please add all required details", Toast.LENGTH_SHORT).show();

                        }
                    });
                    return;
                }

                float fatRateData = Float.parseFloat(fatRate);
                float snfRateData = Float.parseFloat(snfRate);
                float commissionData = Utils.isNotEmpty(commission) ? Float.parseFloat(commission) : Float.parseFloat("0");
                float dedicationData = Utils.isNotEmpty(deduction) ? Float.parseFloat(deduction) : Float.parseFloat("0");

                Debug.e(TAG, "fromFAT" + Utils.getStringToFloat(fromFAT));
                Debug.e(TAG, "toFAT" + Utils.getStringToFloat(toFAT));
                Debug.e(TAG, "fromSNF" + Utils.getStringToFloat(fromSNF));
                Debug.e(TAG, "toSNF" + Utils.getStringToFloat(toSNF));
                Debug.e(TAG, "fatRateData" + fatRateData);
                Debug.e(TAG, "snfRateData" + snfRateData);
                Debug.e(TAG, "commissionData" + commissionData);
                Debug.e(TAG, "dedicationData" + dedicationData);

                Utils.addPD(perFAT_PD, perLiterPD, perGovtPD);
                Utils.createChartData(mContext, mContext, Utils.getStringToFloat(fromFAT), Utils.getStringToFloat(toFAT), Utils.getStringToFloat(fromSNF), Utils.getStringToFloat(toSNF), fatRateData, snfRateData, commissionData, dedicationData, isBuffalo);

            }
        }).start();

    }


    /**
     * open local filer to select file
     */
    private void openFileSelector() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("application/*");
        startActivityForResult(intent, FILE_SELECTOR_CODE);
    }

    private void openFolderSelector() {
        Intent intent = new Intent(Intent.ACTION_CREATE_DOCUMENT);
        intent.setType("application/*");
        intent.putExtra(Intent.EXTRA_TITLE,
                System.currentTimeMillis() + ".xlsx");
        startActivityForResult(intent, DIR_SELECTOR_CODE);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == FILE_SELECTOR_CODE && resultCode == Activity.RESULT_OK) {
            Uri uri = data.getData();
            if (uri == null) return;
            Log.i(TAG, "onActivityResult: " + "filePath：" + uri.getPath());
            //select file and import
            importExcelDeal(uri);
        } else if (requestCode == DIR_SELECTOR_CODE && resultCode == Activity.RESULT_OK) {
            Uri uri = data.getData();
            if (uri == null) return;
            Log.i(TAG, "onActivityResult: " + "filePath：" + uri.getPath());
            Toast.makeText(mContext, "Exporting...", Toast.LENGTH_SHORT).show();
            //you can modify readExcelList, then write to excel.

            List<Map<Integer, Object>> readExcelNew = new ArrayList<>();
            if (isBuffalo) {
                Debug.e(TAG, "Get Buffalo data =--->: " + Utils.convertStringToChartList(PreferenceManager.getValue(Constants.BUFFALO_CHART_DATA)));
                readExcelNew = Utils.convertStringToChartList(PreferenceManager.getValue(Constants.BUFFALO_CHART_DATA));
            } else {
                Debug.e(TAG, "Get Cow data =--->: " + Utils.convertStringToChartList(PreferenceManager.getValue(Constants.COW_CHART_DATA)));
                readExcelNew = Utils.convertStringToChartList(PreferenceManager.getValue(Constants.COW_CHART_DATA));
            }
            ExcelUtil.writeExcelNew(this, readExcelNew, uri);
        }
    }

    private void importExcelDeal(final Uri uri) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Log.i(TAG, "doInBackground: Importing...");
                UploadChartActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(mContext, "Importing...", Toast.LENGTH_SHORT).show();
                    }
                });

                Log.e(TAG, "importExcelDeal: " + ExcelUtil.readExcelNew(mContext, uri, uri.getPath()));
                List<Map<Integer, Object>> readExcelNew = ExcelUtil.readExcelNew(mContext, uri, uri.getPath());

                Log.i(TAG, "onActivityResult:readExcelNew " + ((readExcelNew != null) ? readExcelNew.size() : ""));

                if (readExcelNew != null && readExcelNew.size() > 0) {
//                readExcelList.clear();
//                readExcelList.addAll(readExcelNew);
//                    UploadChart.this.updateUI();

                    UploadChartActivity.this.storeChartData(readExcelNew);

                    Log.i(TAG, "run: successfully imported");
                    UploadChartActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(mContext, "successfully imported", Toast.LENGTH_SHORT).show();
                        }
                    });
                } else {
                    UploadChartActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(mContext, "no data", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }
        }).start();
    }

    private void storeChartData(List<Map<Integer, Object>> readExcelNew) {

        if (isBuffalo) {
            PreferenceManager.putValue(Constants.BUFFALO_CHART_DATA, Utils.convertChartListToString(readExcelNew));
            Debug.e(TAG, "Get Buffalo data =--->: " + Utils.convertChartListToString(readExcelNew));
        } else {
            PreferenceManager.putValue(Constants.COW_CHART_DATA, Utils.convertChartListToString(readExcelNew));
            Debug.e(TAG, "Get Cow data =--->: " + Utils.convertChartListToString(readExcelNew));
        }
    }


    /**
     * refresh RecyclerView
     */
    private void updateUI() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
//            if (readExcelList != null && readExcelList.size() > 0) {
//                excelAdapter.notifyDataSetChanged();
//            }
            }
        });
    }

    @Override
    public void createdChart(boolean isBuffalo, final List<Map<Integer, Object>> data) {
        Debug.e(TAG, "Create Chart =--->" + data);
        storeChartData(data);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                excelAdapter.setNewData(data);
                excelAdapter.notifyDataSetChanged();
            }
        });

    }

    @Override
    public void createdCowChart(boolean isBuffalo, List<Map<Integer, Object>> data) {

    }

    @Override
    public void createdBuffaloChart(boolean isBuffalo, List<Map<Integer, Object>> data) {

    }
}