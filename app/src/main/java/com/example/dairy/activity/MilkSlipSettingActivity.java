package com.example.dairy.activity;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.widget.CompoundButton;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.example.dairy.R;
import com.example.dairy.databinding.ActivityMilkSettingBinding;
import com.example.dairy.utils.Constants;
import com.example.dairy.utils.PreferenceManager;

public class MilkSlipSettingActivity extends AppCompatActivity {


    private static final String TAG = "MilkSlipSettingActivity";
    ActivityMilkSettingBinding binding;
    public String contactUsHindi = "";
    public String contactUsEnglish = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_milk_setting);
        getSupportActionBar().setTitle(R.string.milk_setting);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        init();
    }

    TextWatcher setSlipMessageEnglish = new TextWatcher() {
        @Override
        public void afterTextChanged(Editable s) {
            if(s.toString().length() > 0){
                String tempValue = s.toString();
                PreferenceManager.putValue(Constants.SLIP_MESSAGE, tempValue);
            }else {
                binding.etSlipMessageEnglish.setText(contactUsEnglish);
                PreferenceManager.putValue(Constants.SLIP_MESSAGE, contactUsEnglish);
            }
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }


    };

    TextWatcher setSlipMessageHindi = new TextWatcher() {
        @Override
        public void afterTextChanged(Editable s) {
            if(s.toString().length() > 0){
                String tempValue = s.toString();
                PreferenceManager.putValue(Constants.SLIP_MESSAGE_HINDI, tempValue);
            }else {
                binding.etSlipMessageEnglish.setText(contactUsEnglish);
                PreferenceManager.putValue(Constants.SLIP_MESSAGE_HINDI, contactUsHindi);
            }
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }


    };


    private void init() {

        contactUsHindi = PreferenceManager.getValue(Constants.SLIP_MESSAGE_HINDI);
        contactUsEnglish = PreferenceManager.getValue(Constants.SLIP_MESSAGE);

        binding.etSlipMessageEnglish.setText(contactUsEnglish);
        binding.etSlipMessageHindi.setText(contactUsHindi);

        binding.etSlipMessageEnglish.addTextChangedListener(setSlipMessageEnglish);
        binding.etSlipMessageHindi.addTextChangedListener(setSlipMessageHindi);


        // Machine setting
        binding.weightScaleOn.setChecked(PreferenceManager.getValueBoolean(Constants.WEIGHT_SCALE_ON));
        binding.fatMachineOn.setChecked(PreferenceManager.getValueBoolean(Constants.FAT_MACHINE_ON));
        binding.printerOn.setChecked(PreferenceManager.getValueBoolean(Constants.PRINTER_ON));
        binding.backMachineOn.setChecked(PreferenceManager.getValueBoolean(Constants.BACK_MACHINE_ON));
        binding.displayBoardOn.setChecked(PreferenceManager.getValueBoolean(Constants.DISPLAY_BOARD_ON));

        binding.weightScaleOn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
//                Toast.makeText(MilkSlipSettingActivity.this, "setOnCheckedChangeListener" + b, Toast.LENGTH_SHORT).show();
                PreferenceManager.putValueBoolean(Constants.WEIGHT_SCALE_ON, b);
            }
        });

        binding.fatMachineOn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
//                Toast.makeText(MilkSlipSettingActivity.this, "setOnCheckedChangeListener" + b, Toast.LENGTH_SHORT).show();
                PreferenceManager.putValueBoolean(Constants.FAT_MACHINE_ON, b);
            }
        });

        binding.printerOn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
//                Toast.makeText(MilkSlipSettingActivity.this, "setOnCheckedChangeListener" + b, Toast.LENGTH_SHORT).show();
                PreferenceManager.putValueBoolean(Constants.PRINTER_ON, b);
            }
        });

        binding.backMachineOn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
//                Toast.makeText(MilkSlipSettingActivity.this, "setOnCheckedChangeListener" + b, Toast.LENGTH_SHORT).show();
                PreferenceManager.putValueBoolean(Constants.BACK_MACHINE_ON, b);
            }
        });


        binding.displayBoardOn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
//                Toast.makeText(MilkSlipSettingActivity.this, "setOnCheckedChangeListener" + b, Toast.LENGTH_SHORT).show();
                PreferenceManager.putValueBoolean(Constants.DISPLAY_BOARD_ON, b);
            }
        });






        // Slip setting
        binding.slipInHindi.setChecked(PreferenceManager.getValueBoolean(Constants.SLIP_IN_HINDI));
        binding.serialNoInSlip.setChecked(PreferenceManager.getValueBoolean(Constants.SERIAL_NO_IN_SLIP));
        binding.perLiterRateInSlip.setChecked(PreferenceManager.getValueBoolean(Constants.PER_LITER_RATE_IN_SLIP));
        binding.kiloFATRateInSlip.setChecked(PreferenceManager.getValueBoolean(Constants.KILO_FAT_RATE_IN_SLIP));
        binding.SNFInSlip.setChecked(PreferenceManager.getValueBoolean(Constants.SNF_IN_SLIP));
        binding.CLRInSlip.setChecked(PreferenceManager.getValueBoolean(Constants.CLR_IN_SLIP));
        binding.waterInSlip.setChecked(PreferenceManager.getValueBoolean(Constants.WATER_IN_SLIP));
        binding.perFATPDInSlip.setChecked(PreferenceManager.getValueBoolean(Constants.PER_FAT_PD_IN_SLIP));
        binding.perLiterPDInSlip.setChecked(PreferenceManager.getValueBoolean(Constants.PER_LITER_PD_IN_SLIP));
        binding.GovtPDInSlip.setChecked(PreferenceManager.getValueBoolean(Constants.GOVT_PD_IN_SLIP));


        binding.slipInHindi.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
//                Toast.makeText(MilkSlipSettingActivity.this, "setOnCheckedChangeListener" + b, Toast.LENGTH_SHORT).show();
                PreferenceManager.putValueBoolean(Constants.SLIP_IN_HINDI, b);
            }
        });

        binding.serialNoInSlip.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
//                Toast.makeText(MilkSlipSettingActivity.this, "setOnCheckedChangeListener" + b, Toast.LENGTH_SHORT).show();
                PreferenceManager.putValueBoolean(Constants.SERIAL_NO_IN_SLIP, b);
            }
        });

        binding.perLiterRateInSlip.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
//                Toast.makeText(MilkSlipSettingActivity.this, "setOnCheckedChangeListener" + b, Toast.LENGTH_SHORT).show();
                PreferenceManager.putValueBoolean(Constants.PER_LITER_RATE_IN_SLIP, b);
            }
        });

        binding.kiloFATRateInSlip.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
//                Toast.makeText(MilkSlipSettingActivity.this, "setOnCheckedChangeListener" + b, Toast.LENGTH_SHORT).show();
                PreferenceManager.putValueBoolean(Constants.KILO_FAT_RATE_IN_SLIP, b);
            }
        });


        binding.SNFInSlip.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
//                Toast.makeText(MilkSlipSettingActivity.this, "setOnCheckedChangeListener" + b, Toast.LENGTH_SHORT).show();
                PreferenceManager.putValueBoolean(Constants.SNF_IN_SLIP, b);
            }
        });

        binding.CLRInSlip.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
//                Toast.makeText(MilkSlipSettingActivity.this, "setOnCheckedChangeListener" + b, Toast.LENGTH_SHORT).show();
                PreferenceManager.putValueBoolean(Constants.CLR_IN_SLIP, b);
            }
        });

        binding.waterInSlip.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
//                Toast.makeText(MilkSlipSettingActivity.this, "setOnCheckedChangeListener" + b, Toast.LENGTH_SHORT).show();
                PreferenceManager.putValueBoolean(Constants.WATER_IN_SLIP, b);
            }
        });

        binding.perFATPDInSlip.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
//                Toast.makeText(MilkSlipSettingActivity.this, "setOnCheckedChangeListener" + b, Toast.LENGTH_SHORT).show();
                PreferenceManager.putValueBoolean(Constants.PER_FAT_PD_IN_SLIP, b);
            }
        });


        binding.perLiterPDInSlip.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
//                Toast.makeText(MilkSlipSettingActivity.this, "setOnCheckedChangeListener" + b, Toast.LENGTH_SHORT).show();
                PreferenceManager.putValueBoolean(Constants.PER_LITER_PD_IN_SLIP, b);
            }
        });

        binding.GovtPDInSlip.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
//                Toast.makeText(MilkSlipSettingActivity.this, "setOnCheckedChangeListener" + b, Toast.LENGTH_SHORT).show();
                PreferenceManager.putValueBoolean(Constants.GOVT_PD_IN_SLIP, b);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}