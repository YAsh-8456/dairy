package com.example.dairy.activity;

import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;

import androidx.databinding.DataBindingUtil;

import com.example.dairy.R;
import com.example.dairy.databinding.ActivityAdvanceViewPaymetDetailsBinding;
import com.example.dairy.modal.Result;

public class AdvanceViewPaymentDetails extends MyAppBaseActivity {


    private ActivityAdvanceViewPaymetDetailsBinding binding;
    private Result result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_advance_view_paymet_details);
        getSupportActionBar().setTitle(R.string.paymentdetails);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        result = (Result) getIntent().getSerializableExtra("data");
        Log.d("data", result.toString());
        init();

    }

    private void init() {
        binding.tvcustomerId.setText(result.getCustomerId().toString());
        binding.tvcustomerName.setText(result.getCustomer().getCustomerName());
        binding.tvpaymendate.setText(result.getPaymentDate());
        binding.tvamount.setText(result.getAmount());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;


            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }
}