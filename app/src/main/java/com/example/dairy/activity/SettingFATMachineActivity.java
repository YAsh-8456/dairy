package com.example.dairy.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import androidx.databinding.DataBindingUtil;

import com.example.dairy.R;
import com.example.dairy.databinding.ActivitySettingFatMachineBinding;
import com.example.dairy.utils.Constants;
import com.example.dairy.webService.APIController;
import com.example.dairy.webService.ResponseInterface;

public class SettingFATMachineActivity extends MyAppBaseActivity implements View.OnClickListener, ResponseInterface {

    private static final String TAG = "Setting";
    ActivitySettingFatMachineBinding binding;
    private static final int EDITABLE_DIARY_RECORD = 234;
    private APIController apiController;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_setting_fat_machine);
        getSupportActionBar().setTitle(R.string.fat_machine_setting);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        init();
    }

    private void init() {


    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {


        }
    }

    private void restartActivity() {
        Intent intent = getIntent();
        finish();
        startActivity(intent);
    }


    @Override
    public void successResponse(Constants.API_TYPE type, Object response, int code) {
        switch (type) {

        }


    }

    @Override
    public void failureResponse(Constants.API_TYPE type, String error) {
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }
}