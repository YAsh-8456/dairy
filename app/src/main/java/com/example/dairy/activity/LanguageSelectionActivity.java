package com.example.dairy.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.example.dairy.R;
import com.example.dairy.databinding.ActivityLanguageSupportBinding;
import com.example.dairy.utils.Constants;
import com.example.dairy.utils.PreferenceManager;
import com.example.dairy.utils.Utils;

public class LanguageSelectionActivity extends AppCompatActivity implements View.OnClickListener {

    ActivityLanguageSupportBinding binding;
    private String selectedLanguage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_language_support);
        getSupportActionBar().setTitle(R.string.language_selection);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        init();
    }

    private void init() {

        binding.tvHindi.setOnClickListener(this);
        binding.tvEnglish.setOnClickListener(this);
        binding.tvUpdateLanguage.setOnClickListener(this);

        selectedLanguage = Utils.isNotEmpty(PreferenceManager.getValue(Constants.SELECTED_LANGUAGE)) ? PreferenceManager.getValue(Constants.SELECTED_LANGUAGE) : Constants.ENGLISH;

        if (selectedLanguage.equals(Constants.ENGLISH)) {
            binding.tvHindi.setBackground(getResources().getDrawable(R.drawable.unselected_languange_bg));
            binding.tvEnglish.setBackground(getResources().getDrawable(R.drawable.selected_languange_bg));

        } else if (selectedLanguage.equals(Constants.HINDI)) {
            binding.tvHindi.setBackground(getResources().getDrawable(R.drawable.selected_languange_bg));
            binding.tvEnglish.setBackground(getResources().getDrawable(R.drawable.unselected_languange_bg));
        }

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tvUpdateLanguage:

                if (selectedLanguage.equals(Constants.ENGLISH)) {
                    PreferenceManager.putValue(Constants.SELECTED_LANGUAGE, Constants.ENGLISH);
                } else if (selectedLanguage.equals(Constants.HINDI)) {
                    PreferenceManager.putValue(Constants.SELECTED_LANGUAGE, Constants.HINDI);
                }
                refreshApp();
                break;

            case R.id.tvHindi:
                selectedLanguage = Constants.HINDI;
                binding.tvHindi.setBackground(getResources().getDrawable(R.drawable.selected_languange_bg));
                binding.tvEnglish.setBackground(getResources().getDrawable(R.drawable.unselected_languange_bg));
                break;

            case R.id.tvEnglish:
                selectedLanguage = Constants.ENGLISH;
                binding.tvEnglish.setBackground(getResources().getDrawable(R.drawable.selected_languange_bg));
                binding.tvHindi.setBackground(getResources().getDrawable(R.drawable.unselected_languange_bg));
                break;
        }
    }

    private void refreshApp() {

        Intent refreshApp = new Intent(this, SplashActivity.class);
        startActivity(refreshApp);
        finishAffinity();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }
}