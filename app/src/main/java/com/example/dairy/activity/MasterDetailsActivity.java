package com.example.dairy.activity;

import android.os.Bundle;
import android.view.MenuItem;

import androidx.databinding.DataBindingUtil;

import com.example.dairy.R;
import com.example.dairy.databinding.ActivityMemberDetailsBinding;
import com.example.dairy.modal.Result;
import com.example.dairy.utils.Utils;

public class MasterDetailsActivity extends MyAppBaseActivity {

    ActivityMemberDetailsBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_member_details);
        getSupportActionBar().setTitle(R.string.master_details);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        init();
    }

    private void init() {
        if(getIntent() != null){
            Result result = (Result) getIntent().getSerializableExtra("memberDetails");
            setMemberInformation(result);
        }

    }

    private void setMemberInformation(Result result) {

        String emptyData = "--";
        binding.tvMemberId.setText(Utils.isNotEmpty(String.valueOf(result.getId())) ? ""+result.getId() : emptyData);
        binding.tvMemberName.setText(Utils.isNotEmpty(result.getCustomerName()) ? result.getCustomerName() : emptyData);
        binding.tvFatherName.setText(Utils.isNotEmpty(result.getFatherName()) ? result.getFatherName() : emptyData);
        binding.tvAccountNumber.setText(Utils.isNotEmpty(result.getAmount()) ? result.getAmount() : emptyData);
        binding.tvIFSCCode.setText(Utils.isNotEmpty(result.getiFSCCode()) ? result.getiFSCCode() : emptyData);
        binding.tvBankName.setText(Utils.isNotEmpty(result.getBankName()) ? result.getBankName() : emptyData);
        binding.tvVillageName.setText(Utils.isNotEmpty(result.getVillageName()) ? result.getVillageName() : emptyData);
        binding.tvAddress.setText(Utils.isNotEmpty(result.getAddress()) ? result.getAddress() : emptyData);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;


            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }
}