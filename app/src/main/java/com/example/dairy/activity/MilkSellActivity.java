package com.example.dairy.activity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.example.dairy.R;
import com.example.dairy.adapter.MilkSellerAdapter;
import com.example.dairy.databinding.ActivityMilkSellBinding;
import com.example.dairy.fragment.SearchMemberFragment;
import com.example.dairy.modal.CommonResponse;
import com.example.dairy.modal.Result;
import com.example.dairy.utils.AlertMassage;
import com.example.dairy.utils.Constants;
import com.example.dairy.utils.Debug;
import com.example.dairy.utils.MemberClickInterface;
import com.example.dairy.utils.PreferenceManager;
import com.example.dairy.utils.SelectMemberInterface;
import com.example.dairy.utils.ThermalOperation;
import com.example.dairy.utils.Utils;
import com.example.dairy.webService.APIController;
import com.example.dairy.webService.ResponseInterface;

import org.json.JSONObject;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Response;

public class MilkSellActivity extends MyAppBaseActivity implements View.OnClickListener, ResponseInterface, MemberClickInterface, SelectMemberInterface {


    ActivityMilkSellBinding binding;
    MilkSellerAdapter milkSellerAdapter;
    final Calendar myCalendar = Calendar.getInstance();
    DatePickerDialog.OnDateSetListener date;
    boolean isMorning = Utils.isMorning();
    boolean isBuffalo = true;
    private String TAG = "MilkSell";
    private boolean isEditable = false;
    private int editableUSerPosition = -1;
    private APIController apiController;
    private Integer editableId;
    private Result selectedMember = new Result();
    private boolean userIdValid = false;
    private int userID = 0;
    private ThermalOperation thermalOperation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_milk_sell);
        getSupportActionBar().setTitle(R.string.categories_milk_sell);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        init();
    }

    private void noDataFoundManage() {
        if (milkSellerAdapter.list.size() > 0) {
            binding.buyRecordView.setVisibility(View.VISIBLE);
            binding.llNoDataFound.setVisibility(View.GONE);
        } else {
            binding.buyRecordView.setVisibility(View.GONE);
            binding.llNoDataFound.setVisibility(View.VISIBLE);
        }
    }

    private void updatePet(boolean isBuffalo) {

        int cow = PreferenceManager.getValueInt(Constants.COW_LOCAL_SALE_RATE);
        int buffalo = PreferenceManager.getValueInt(Constants.BUFFALO_LOCAL_SALE_RATE);

        if (Utils.isNotEmpty(selectedMember.getCustomerName())) {
            cow = Integer.parseInt(selectedMember.getLocalMilkSaleRateForCow());
            buffalo = Integer.parseInt(selectedMember.getLocalMilkSaleRateForBuffalo());
        }

        if (isBuffalo) {
            binding.ivPet.setImageResource(R.drawable.buffalo);
            binding.tvSelectedPet.setText(R.string.buffalo);
            binding.etLiterRate.setText(String.valueOf(buffalo));
        } else {
            binding.ivPet.setImageResource(R.drawable.cow);
            binding.tvSelectedPet.setText(R.string.cow);
            binding.etLiterRate.setText(String.valueOf(cow));
        }
        setTotalAmount();
    }

    private void updateTimeSlot(boolean isMorning) {

        if (isMorning) {
            binding.ivTimer.setImageResource(R.drawable.sun);
            binding.tvSelectedTime.setText(R.string.morning);
        } else {
            binding.ivTimer.setImageResource(R.drawable.moon);
            binding.tvSelectedTime.setText(R.string.evening);
        }
    }

    private void init() {

        apiController = new APIController(this);
        thermalOperation = new ThermalOperation(this);

        binding.myCompanyName.setSelected(true);
        String DairyName = PreferenceManager.getValue(Constants.DAIRY_NAME);
        binding.myCompanyName.setText(DairyName + "  " + getString(R.string.milk_selling_note));

        binding.llSelectDate.setOnClickListener(this);
        binding.llSelectPet.setOnClickListener(this);
        binding.llSelectTime.setOnClickListener(this);
        binding.addRecord.setOnClickListener(this);
        binding.tvMemberId.setOnClickListener(this);
        binding.tvMemberName.setOnClickListener(this);


        updateDate();
        updateTimeSlot(isMorning);
        updatePet(isBuffalo);
//        getMilkSell();  // get sell details

        binding.llEditableLayout.setVisibility(View.GONE);

        //
        date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateDate();
            }

        };

        LinearLayoutManager manager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        binding.buyRecordView.setLayoutManager(manager);
        milkSellerAdapter = new MilkSellerAdapter(this, this);
        binding.buyRecordView.setAdapter(milkSellerAdapter);

        binding.tvMemberId.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {


            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                Debug.e(TAG, "Call add ID =---->");
                if (Utils.isNotEmpty(charSequence.toString())) {

                    Result result = Utils.checkIDValidOrNotSell(charSequence.toString());
                    if (result != null) {
                        userIdValid = true;
                        binding.ivIsValidUser.setVisibility(View.VISIBLE);
                        binding.tvMemberName.setText(result.getCustomerName());
                        userID = result.getId();
                        selectedMember = result;
                        if (isBuffalo) {
                            binding.etLiterRate.setText(result.getLocalMilkSaleRateForBuffalo());
                        } else {
                            binding.etLiterRate.setText(result.getLocalMilkSaleRateForCow());
                        }
                    } else {
                        binding.tvMemberName.setText("");
                        binding.ivIsValidUser.setVisibility(View.GONE);
                        userID = 0;
                    }

                } else {
                    userIdValid = false;
                    binding.tvMemberName.setText("");
                    binding.ivIsValidUser.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        binding.tvLiter.addTextChangedListener(setValueInTotalAmountAndLiterRate);
        binding.etLiterRate.addTextChangedListener(setValueInTotalAmountAndLiterRate);

        noDataFoundManage();
    }

    TextWatcher setValueInTotalAmountAndLiterRate = new TextWatcher() {
        @Override
        public void afterTextChanged(Editable s) {
            setTotalAmount();
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }


    };


    private void setTotalAmount() {

        // set liter amount
        String literString = binding.tvLiter.getText().toString().trim();
        if (Utils.isStringInteger(literString)) {
            Debug.e(TAG, "literString convert float =--->" + String.valueOf((float) Integer.parseInt(literString)));
        }

        float liter = Utils.isNotEmpty(binding.tvLiter.getText().toString().trim()) ?
                Float.parseFloat(binding.tvLiter.getText().toString().trim()) : 0F;
        String litter = binding.etLiterRate.getText().toString().trim();
        Float literAmount = Float.valueOf(litter);
        //float literAmount = 45;
        float totalAmount = literAmount * liter;

//        binding.etLiterRate.setText(String.valueOf(Utils.arrangeFloatValue(literAmount)));
        binding.etTotalAmount.setText(String.valueOf(Utils.arrangeFloatValue(totalAmount)));

    }

    private void updateDate() {
        String myFormat = "dd-MM-yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        binding.tvSelectedDate.setText(sdf.format(myCalendar.getTime()));
        getMilkSell();

    }

    private void addMilkSell() {

        String memberName = binding.tvMemberName.getText().toString().trim();
        String memberId = binding.tvMemberId.getText().toString().trim();
        String memberPrimaryCode = String.valueOf(userID);
//        String fat = binding.tvFat.getText().toString().trim();
//        String snf = binding.tvSNF.getText().toString().trim();
        String literRate = binding.etLiterRate.getText().toString().trim();
        String totalLiter = binding.tvLiter.getText().toString().trim();
        String totalAmount = binding.etTotalAmount.getText().toString().trim();
        String selectedDate = binding.tvSelectedDate.getText().toString().trim();
        String timeSlot = binding.tvSelectedTime.getText().toString().trim();
        String selectedAnimal = binding.tvSelectedPet.getText().toString().trim();


        if (Utils.isEmpty(memberId) || Utils.isEmpty(memberName)) {
            Utils.showSnackbar(this, binding.llMainLayout, getResources().getString(R.string.message_select_first_member));
            return;
        }

        if (Utils.isEmpty(totalLiter)) {
            Utils.showSnackbar(this, binding.llMainLayout, getResources().getString(R.string.message_enter_liter_value));
            return;
        }

        if ((int) Float.parseFloat(totalAmount) <= 0) {
            Utils.showSnackbar(this, binding.llMainLayout, getResources().getString(R.string.message_enter_valid_details));
            return;
        }

        String dairyId = String.valueOf(PreferenceManager.getValueInt(Constants.DAIRY_ID));
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("DairyId", dairyId);
            jsonObject.put("CustomerId", memberPrimaryCode);
            jsonObject.put("liter", totalLiter);
            jsonObject.put("rate", literRate);
            jsonObject.put("totalAmount", totalAmount);
            jsonObject.put("saleDate", Utils.finalDateFormat(selectedDate, isMorning));
            jsonObject.put("timeslot", isMorning ? Constants.MORNING : Constants.EVENING);
            jsonObject.put("animalType", isBuffalo ? Constants.BUFFALO : Constants.COW);
//            jsonObject.put("animalType", "");



            if (!Utils.isInternetConnected(this)) {
                AlertMassage.MassageAlert(this, getResources().getString(R.string.message_intent_conection));
                return;
            }

            binding.llProgressBar.setVisibility(View.VISIBLE);
            Utils.hideSoftKeyword(this);
            RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), jsonObject.toString());

            if (isEditable) {
                isEditable = false;

                apiController.editMilkSell(Constants.API_TYPE.EDIT_MILK_SELL, editableId.toString(), body);
            } else {
                apiController.addMilkSell(Constants.API_TYPE.ADD_MILK_SELL, body);
            }

        } catch (Exception e) {
            Debug.e(TAG, "Error" + e.getMessage());
        }
    }

    private void getMilkSell() {

        String selectedDate = binding.tvSelectedDate.getText().toString().trim();
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("startDate", Utils.finalDateFormat(selectedDate, isMorning));
            jsonObject.put("endDate", Utils.finalDateFormat(selectedDate, isMorning));
            jsonObject.put("timeslot", isMorning ? Constants.MORNING : Constants.EVENING);
//            jsonObject.put("animalType", isBuffalo ? Constants.BUFFALO : Constants.COW);
            jsonObject.put("animalType", "All");


            if (!Utils.isInternetConnected(this)) {
                AlertMassage.MassageAlert(this, getResources().getString(R.string.message_intent_conection));
                return;
            }

            binding.llProgressBar.setVisibility(View.VISIBLE);
            Utils.hideSoftKeyword(this);
            RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), jsonObject.toString());
            apiController.getMilkSell(Constants.API_TYPE.GET_MILK_SELL, "", body);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void deleteMilkSell(Result result) {
        try {
            if (!Utils.isInternetConnected(this)) {
                AlertMassage.MassageAlert(this, getResources().getString(R.string.message_intent_conection));
                return;
            }
            binding.llProgressBar.setVisibility(View.VISIBLE);
            Utils.hideSoftKeyword(this);
            String deleteId = String.valueOf(result.getId());
            Debug.e(TAG, "Delete ID =--->" + result.getId().toString());
            apiController.deleteMilkSell(Constants.API_TYPE.REMOVE_MILK_SELL, deleteId);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void openBottomSheet() {
        SearchMemberFragment searchMemberFragment =
                SearchMemberFragment.newInstance("SELLER", "");
        searchMemberFragment.show(getSupportFragmentManager(),
                "set_value_dialog_fragment");
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.llSelectDate:
                new DatePickerDialog(this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
                break;

            case R.id.llSelectPet:
                isBuffalo = !isBuffalo;
                updatePet(isBuffalo);
//                getMilkSell();
                break;

            case R.id.llSelectTime:
                isMorning = !isMorning;
                updateTimeSlot(isMorning);
                getMilkSell();

                break;

            case R.id.addRecord:
                addMilkSell();
                break;

            case R.id.tvMemberName:
                openBottomSheet();
                break;
        }

    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        MenuInflater inflater = getMenuInflater();
//        inflater.inflate(R.menu.setting, menu);
//        return true;
//    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }


    private void clearData() {
        isEditable = false;
        editableUSerPosition = -1;
        binding.tvMemberId.setText("");
        binding.tvMemberName.setText("");
        binding.tvLiter.setText("0");
        binding.etTotalAmount.setText("0");
        selectedMember = new Result();
        binding.llEditableLayout.setVisibility(View.GONE);

    }

    @Override
    public void successResponse(Constants.API_TYPE type, Object response, int code) {
        binding.llProgressBar.setVisibility(View.GONE);
        switch (type) {
            case ADD_MILK_SELL:
                try {
                    if (code == Constants.RESULT_OK) {
                        CommonResponse addMilkBuyResponse = ((Response<CommonResponse>) response).body();
                        if (addMilkBuyResponse != null && addMilkBuyResponse.getStatus()) {
                            clearData();
                            milkSellerAdapter.addSingleRecord(addMilkBuyResponse.getResult().get(0));
                            thermalOperation.getMilkSellSlip(addMilkBuyResponse.getResult().get(0));
                            noDataFoundManage();
                        } else {
                            Utils.showSnackbar(this, binding.llMainLayout, addMilkBuyResponse.getMessage());
                        }

                    } else {
                        Utils.showSnackbar(this, binding.llMainLayout, "API Error");

                    }
                } catch (Exception e) {
                    Debug.e(TAG, "ADD_MILK_BUY" + e.getMessage());
                }
                break;

            case EDIT_MILK_SELL:
                try {
                    if (code == Constants.RESULT_OK) {
                        CommonResponse editMasterResponse = ((Response<CommonResponse>) response).body();
                        if (editMasterResponse != null && editMasterResponse.getStatus()) {
                            Utils.showSnackbar(this, binding.llMainLayout, editMasterResponse.getMessage());
                            milkSellerAdapter.editData(editMasterResponse.getResult().get(0), editableUSerPosition);
                            clearData();

                        } else {
                            Utils.showSnackbar(this, binding.llMainLayout, editMasterResponse.getMessage());
                        }

                    } else {
                        Utils.showSnackbar(this, binding.llMainLayout, "API Error");

                    }
                } catch (Exception e) {
                    Debug.e(TAG, "EDIT_MILK_BUY" + e.getMessage());
                }
                break;

            case REMOVE_MILK_SELL:
                try {
                    if (code == Constants.RESULT_OK) {
                        CommonResponse removeMilkBuyResponse = ((Response<CommonResponse>) response).body();
                        if (removeMilkBuyResponse != null && removeMilkBuyResponse.getStatus()) {
                            noDataFoundManage();
                        } else {
                        }

                    } else {
                        Utils.showSnackbar(this, binding.llMainLayout, "API Error");

                    }
                } catch (Exception e) {
                    Debug.e(TAG, "REMOVE_MILK_BUY" + e.getMessage());
                }
                break;

            case GET_MILK_SELL:
                try {
                    if (code == Constants.RESULT_OK) {
                        CommonResponse getMilkDataResponse = ((Response<CommonResponse>) response).body();
                        if (getMilkDataResponse != null && getMilkDataResponse.getStatus()) {
                            Utils.showSnackbar(this, binding.llMainLayout, getMilkDataResponse.getMessage());
                            Debug.e(TAG, getMilkDataResponse.getResult().toString());
                            milkSellerAdapter.addData(getMilkDataResponse.getResult());
                            noDataFoundManage();
                        } else {
                            Utils.showSnackbar(this, binding.llMainLayout, getMilkDataResponse.getMessage());
                        }

                    } else {
                        Utils.showSnackbar(this, binding.llMainLayout, "API Error");

                    }
                } catch (Exception e) {
                    Debug.e(TAG, "GET_MILK_BUY" + e.getMessage());
                }
                break;
        }
    }

    @Override
    public void failureResponse(Constants.API_TYPE type, String error) {
        binding.llProgressBar.setVisibility(View.GONE);
        Debug.e(TAG, "Fail Error =----->" + error);
    }

    @Override
    public void delete(Result result, int position) {
        deleteMilkSell(result);
    }

    @Override
    public void view(Result result, int position) {

        Intent intent = new Intent(this, MilkSellDetailsActivity.class);
        intent.putExtra("data", (Serializable) result);
        startActivity(intent);


    }

    @Override
    public void edit(Result result, int position) {
        isEditable = true;
        editableUSerPosition = position;
        editableId = result.getId();
        binding.tvMemberId.setText("" + result.getCustomer().getMemberCode());
        binding.tvMemberName.setText(result.getCustomer().getCustomerName());
        userID = result.getCustomerId();

//        if (isBuffalo) {
//            binding.etLiterRate.setText(result.getLocalMilkSaleRateForBuffalo());
//        } else {
//            binding.etLiterRate.setText(result.getLocalMilkSaleRateForCow());
//        }

        binding.tvSelectedDate.setText(result.getSaleDate());


        binding.etTotalAmount.setText(result.getTotalAmount());

        if (result.getAnimalType().equals(getResources().getString(R.string.buffalo))) {
            isBuffalo = true;
        } else {
            isBuffalo = false;
        }

        updatePet(isBuffalo);

        if (result.getTimeslot() != null) {
            if (result.getTimeslot().equals(getResources().getString(R.string.morning))) {
                isMorning = true;
            } else {
                isMorning = false;
            }
            updateTimeSlot(isMorning);

        }

        binding.tvLiter.setText(result.getLiter());
        binding.etLiterRate.setText(result.getRate());
        binding.llEditableLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void print(Result result, int position) {
        thermalOperation.printSingleSell(result);
    }

    @Override
    public void selectedMember(Result result) {
        binding.tvMemberId.setText(String.valueOf(result.getMemberCode()));
        userID = result.getId();
        binding.tvMemberName.setText(result.getCustomerName());
        selectedMember = result;
        if (isBuffalo) {
            binding.etLiterRate.setText(result.getLocalMilkSaleRateForBuffalo());
        } else {
            binding.etLiterRate.setText(result.getLocalMilkSaleRateForCow());
        }
    }
}