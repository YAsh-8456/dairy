package com.example.dairy.activity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.example.dairy.R;
import com.example.dairy.adapter.AdvancePaymentAdapter;
import com.example.dairy.databinding.ActivityAdvancePaymentBinding;
import com.example.dairy.fragment.SearchMemberFragment;
import com.example.dairy.modal.CommonResponse;
import com.example.dairy.modal.Result;
import com.example.dairy.utils.AlertMassage;
import com.example.dairy.utils.Constants;
import com.example.dairy.utils.Debug;
import com.example.dairy.utils.MemberClickInterface;
import com.example.dairy.utils.PreferenceManager;
import com.example.dairy.utils.SelectMemberInterface;
import com.example.dairy.utils.ThermalOperation;
import com.example.dairy.utils.Utils;
import com.example.dairy.webService.APIController;
import com.example.dairy.webService.ResponseInterface;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Response;

public class AdvancePayment extends MyAppBaseActivity implements View.OnClickListener, SelectMemberInterface, ResponseInterface, MemberClickInterface {


    private static final String TAG = "AdvancePayment";
    ActivityAdvancePaymentBinding binding;
    boolean isMorning = Utils.isMorning();

    final Calendar myCalendar = Calendar.getInstance();
    final Calendar myDeductionCalendar = Calendar.getInstance();

    private DatePickerDialog.OnDateSetListener date;
    private DatePickerDialog.OnDateSetListener deductionDate;

    private int dairyId;

    private boolean isEditable = false;
    private APIController apiController;
    private AdvancePaymentAdapter advancePaymentAdapter;
    private int editableUSerPosition;
    private int editable_id;

    ThermalOperation thermalOperation;
    private int userID = 0;
    private boolean userIdValid = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_advance_payment);
        getSupportActionBar().setTitle(R.string.categories_milk_advance_payment);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        init();
    }

    private void init() {
        apiController = new APIController(this);
        thermalOperation = new ThermalOperation(this);

        binding.llEditableLayout.setVisibility(View.GONE);

        binding.myCompanyName.setSelected(true);
        String DairyName = PreferenceManager.getValue(Constants.DAIRY_NAME);
        binding.myCompanyName.setText(DairyName + "  " + getString(R.string.advance_payment_note));


        LinearLayoutManager manager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        binding.buyRecordView.setLayoutManager(manager);
        advancePaymentAdapter = new AdvancePaymentAdapter(this, this);
        binding.buyRecordView.setAdapter(advancePaymentAdapter);

        binding.llSelectDate.setOnClickListener(this);
        binding.llSelectTime.setOnClickListener(this);
        binding.tvMemberId.setOnClickListener(this);
        binding.tvMemberName.setOnClickListener(this);
        binding.addRecord.setOnClickListener(this);
        binding.llEditableLayout.setOnClickListener(this);
        binding.llDeductionDate.setOnClickListener(this);

        updateTimeSlot(isMorning);

        //
        updateDate();
        date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateDate();
            }

        };

        updateDeductionDate();
        deductionDate = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myDeductionCalendar.set(Calendar.YEAR, year);
                myDeductionCalendar.set(Calendar.MONTH, monthOfYear);
                myDeductionCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateDeductionDate();
            }

        };


        binding.tvMemberId.addTextChangedListener(setMemberIdTextChange);
       
        noDataFoundManage();
    }

    TextWatcher setMemberIdTextChange = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            Debug.e(TAG, "Call add ID =---->");
            if (Utils.isNotEmpty(charSequence.toString())) {

                Result result = Utils.checkIDValidOrNot(charSequence.toString());
                if (result != null) {
                    userIdValid = true;
                    binding.ivIsValidUser.setVisibility(View.VISIBLE);
                    binding.tvMemberName.setText(result.getCustomerName());
                    userID = result.getId();
                } else {
                    binding.tvMemberName.setText("");
                    binding.ivIsValidUser.setVisibility(View.GONE);
                    userID = 0;
                }

            } else {
                userIdValid = false;
                binding.tvMemberName.setText("");
                binding.ivIsValidUser.setVisibility(View.GONE);
            }
        }

        @Override
        public void afterTextChanged(Editable editable) {

        }
    };

    private void noDataFoundManage() {
//        if (advancePaymentAdapter.list.size() > 0) {
//            binding.buyRecordView.setVisibility(View.VISIBLE);
//            binding.llNoDataFound.setVisibility(View.GONE);
//        } else {
//            binding.buyRecordView.setVisibility(View.GONE);
//            binding.llNoDataFound.setVisibility(View.VISIBLE);
//        }
    }

    private void getAdvancePayment() {

        String selectedDate = binding.tvSelectedDate.getText().toString().trim();

        try {
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("startDate", Utils.finalDateFormat(selectedDate, isMorning));
            jsonObject.addProperty("endDate", Utils.finalDateFormat(selectedDate, isMorning));
            jsonObject.addProperty("startMemberCode", (Number) null);
            jsonObject.addProperty("endMemberCode", (Number) null);
            if (!Utils.isInternetConnected(this)) {
                AlertMassage.MassageAlert(this, getResources().getString(R.string.message_intent_conection));
                return;
            }

            binding.llProgressBar.setVisibility(View.VISIBLE);
            Utils.hideSoftKeyword(this);
            RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), jsonObject.toString());

            apiController.getAdvancePayment(Constants.API_TYPE.GET_ADVANCE_PAYMENT, body);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateDate() {
        String myFormat = "dd-MM-yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        binding.tvSelectedDate.setText(sdf.format(myCalendar.getTime()));
        getAdvancePayment();
    }

    private void updateDeductionDate() {
        String myFormat = "dd-MM-yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        binding.tvDeductionDate.setText(sdf.format(myCalendar.getTime()));
    }

    private void updateTimeSlot(boolean isMorning) {

        if (isMorning) {
            binding.ivTimer.setImageResource(R.drawable.sun);
            binding.tvSelectedTime.setText(R.string.morning);
        } else {
            binding.ivTimer.setImageResource(R.drawable.moon);
            binding.tvSelectedTime.setText(R.string.evening);
        }
    }


    private void openBottomSheet() {

        SearchMemberFragment searchMemberFragment =
                SearchMemberFragment.newInstance("", "");
        searchMemberFragment.show(getSupportFragmentManager(),
                "set_value_dialog_fragment");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                return true;


            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.llSelectDate:
                new DatePickerDialog(this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
                break;

            case R.id.llDeductionDate:
                new DatePickerDialog(this, deductionDate, myDeductionCalendar
                        .get(Calendar.YEAR), myDeductionCalendar.get(Calendar.MONTH),
                        myDeductionCalendar.get(Calendar.DAY_OF_MONTH)).show();
                break;

            case R.id.llSelectTime:
                isMorning = !isMorning;
                updateTimeSlot(isMorning);
                break;


            case R.id.tvMemberId:
            case R.id.tvMemberName:
                openBottomSheet();
                break;

            case R.id.addRecord:
                addAdvancePayment();
                break;

            case R.id.llEditableLayout:
                clearData();
                break;

        }
    }

    private void clearData() {
        isEditable = false;
        editableUSerPosition = -1;

        binding.tvMemberId.setText("");
        binding.tvMemberName.setText("");
        binding.etTotalAmount.setText("");

        binding.llEditableLayout.setVisibility(View.GONE);
    }

    private void addAdvancePayment() {
        String memberName = binding.tvMemberName.getText().toString().trim();
        String memberId = binding.tvMemberId.getText().toString().trim();
        String totalAmount = binding.etTotalAmount.getText().toString().trim();
        String selectedDate = binding.tvSelectedDate.getText().toString().trim();
        String deductionDate = binding.tvDeductionDate.getText().toString().trim();

        String memberPrimaryCode = String.valueOf(userID);
        String dairyId = String.valueOf(PreferenceManager.getValueInt(Constants.DAIRY_ID));

        if (Utils.isEmpty(memberId) || Utils.isEmpty(memberName)) {
            Utils.showSnackbar(this, binding.llMainLayout, getResources().getString(R.string.message_select_first_member));
            return;
        }

        if ((int) Float.parseFloat(totalAmount) <= 0) {
            Utils.showSnackbar(this, binding.llMainLayout, getResources().getString(R.string.advance_payment));
            return;
        }

        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("DairyId", dairyId);
            jsonObject.put("CustomerId", memberPrimaryCode);
            jsonObject.put("amount", totalAmount);
            jsonObject.put("paymentDate", Utils.finalDateFormat(selectedDate, isMorning));

            jsonObject.put("durationDate", Utils.finalDateFormat(deductionDate, isMorning));
            jsonObject.put("note", "Note");



            if (!Utils.isInternetConnected(this)) {
                AlertMassage.MassageAlert(this, getResources().getString(R.string.message_intent_conection));
                return;
            }

            binding.llProgressBar.setVisibility(View.VISIBLE);
            Utils.hideSoftKeyword(this);
            RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), jsonObject.toString());

            if (isEditable) {
                isEditable = false;
                apiController.editAdvancePayment(Constants.API_TYPE.EDIT_ADVANCE_PAYMENT, String.valueOf(editable_id), body);
            } else {
                apiController.addAdvancePayment(Constants.API_TYPE.ADD_ADVANCE_PAYMENT, body);

            }

        } catch (Exception e) {
            Debug.e("Advance Paymen", "Error" + e.getMessage());
        }


    }

    @Override
    public void selectedMember(Result result) {
        binding.tvMemberId.setText(String.valueOf(result.getMemberCode()));
        binding.tvMemberName.setText(result.getCustomerName());
        userID = result.getId();
        dairyId = result.getDairyId();
    }

    @Override
    public void successResponse(Constants.API_TYPE type, Object response, int code) {
        binding.llProgressBar.setVisibility(View.GONE);
        switch (type) {
            case ADD_ADVANCE_PAYMENT:
                try {
                    if (code == Constants.RESULT_OK) {
                        CommonResponse addMilkBuyResponse = ((Response<CommonResponse>) response).body();
                        if (addMilkBuyResponse != null && addMilkBuyResponse.getStatus()) {
                            Debug.e(TAG, "Record is added");
                           clearData();
                            advancePaymentAdapter.addSingleRecord(addMilkBuyResponse.getResult().get(0));
                            noDataFoundManage();

                            thermalOperation.printSingleAdvancePayment(addMilkBuyResponse.getResult().get(0));

                        } else {
                            Utils.showSnackbar(this, binding.llMainLayout, addMilkBuyResponse.getMessage());
                        }

                    } else {
                        Utils.showSnackbar(this, binding.llMainLayout, "API Error");

                    }
                } catch (Exception e) {
                    Debug.e("Advanve Payment", "ADD_Advance_payment" + e.getMessage());
                }
                break;

            case EDIT_ADVANCE_PAYMENT:
                try {
                    if (code == Constants.RESULT_OK) {
                        CommonResponse editMasterResponse = ((Response<CommonResponse>) response).body();
                        if (editMasterResponse != null && editMasterResponse.getStatus()) {
                            Utils.showSnackbar(this, binding.llMainLayout, editMasterResponse.getMessage());

                            advancePaymentAdapter.editData(editMasterResponse.getResult().get(0), editableUSerPosition);
                           clearData();

                        } else {
                            Utils.showSnackbar(this, binding.llMainLayout, editMasterResponse.getMessage());
                        }

                    } else {
                        Utils.showSnackbar(this, binding.llMainLayout, "API Error");

                    }

                } catch (Exception e) {
                    Debug.e("Advanve Payment", "EDIT_Advance" + e.getMessage());
                }
                break;

            case REMOVE_ADVANCE_PAYMENT:
                try {
                    if (code == Constants.RESULT_OK) {
                        CommonResponse removeMilkBuyResponse = ((Response<CommonResponse>) response).body();
                        if (removeMilkBuyResponse != null && removeMilkBuyResponse.getStatus()) {
                            noDataFoundManage();
                        } else {
                        }

                    } else {
                        Utils.showSnackbar(this, binding.llMainLayout, "API Error");

                    }
                } catch (Exception e) {
                    Debug.e("Advanve Payment", "REMOVE_Adavnce" + e.getMessage());
                }
                break;

            case GET_ADVANCE_PAYMENT:
                try {
                    if (code == Constants.RESULT_OK) {
                        CommonResponse getMilkDataResponse = ((Response<CommonResponse>) response).body();
                        if (getMilkDataResponse != null && getMilkDataResponse.getStatus()) {
                            Utils.showSnackbar(this, binding.llMainLayout, getMilkDataResponse.getMessage());
                            Debug.e("Advanve Payment", getMilkDataResponse.getResult().toString());
                            advancePaymentAdapter.addData(getMilkDataResponse.getResult());
                            noDataFoundManage();

                        } else {
                            Utils.showSnackbar(this, binding.llMainLayout, getMilkDataResponse.getMessage());
                        }

                    } else {
                        Utils.showSnackbar(this, binding.llMainLayout, "API Error");

                    }
                } catch (Exception e) {
                    Debug.e("Advanve Payment", "GET_Adavnce" + e.getMessage());
                }
                break;
        }
    }

    @Override
    public void failureResponse(Constants.API_TYPE type, String error) {
        binding.llProgressBar.setVisibility(View.GONE);
        Debug.e("Advanve Payment", "Fail Error =----->" + error);
    }

    @Override
    public void delete(Result result, int position) {
        deleteAdvancePayment(result);
    }

    private void deleteAdvancePayment(Result result) {
        try {
            if (!Utils.isInternetConnected(this)) {
                AlertMassage.MassageAlert(this, getResources().getString(R.string.message_intent_conection));
                return;
            }
            binding.llProgressBar.setVisibility(View.VISIBLE);
            Utils.hideSoftKeyword(this);
            apiController.deleteAdvancePayment(Constants.API_TYPE.REMOVE_ADVANCE_PAYMENT, String.valueOf(result.getId()));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void view(Result result, int position) {
        Intent intent = new Intent(this, AdvanceViewPaymentDetails.class);
        intent.putExtra("data", (Serializable) result);
        startActivity(intent);

    }

    @Override
    public void edit(Result result, int position) {

        isEditable = true;
        editableUSerPosition = position;

        editable_id = result.getId();
        binding.tvMemberId.setText(String.valueOf(result.getCustomer().getMemberCode()));
        userID = result.getCustomerId();
        binding.tvMemberName.setText(result.getCustomer().getCustomerName());
        binding.tvSelectedDate.setText(result.getPaymentDate());
        binding.etTotalAmount.setText(result.getAmount());

        if (result.getTimeslot() != null) {
            if (result.getTimeslot().equals(getResources().getString(R.string.morning))) {
                isMorning = true;
            } else {
                isMorning = false;
            }
            updateTimeSlot(isMorning);
        }

        binding.llEditableLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void print(Result result, int position) {
        thermalOperation.printSingleAdvancePayment(result);
    }
}