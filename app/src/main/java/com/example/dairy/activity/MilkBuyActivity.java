package com.example.dairy.activity;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.example.dairy.R;
import com.example.dairy.adapter.MilkBuyAdapter;
import com.example.dairy.databinding.ActivityMilkBuyBinding;
import com.example.dairy.fragment.SearchMemberFragment;
import com.example.dairy.modal.CommonResponse;
import com.example.dairy.modal.Result;
import com.example.dairy.utils.AlertMassage;
import com.example.dairy.utils.Constants;
import com.example.dairy.utils.Debug;
import com.example.dairy.utils.MemberClickInterface;
import com.example.dairy.utils.PreferenceManager;
import com.example.dairy.utils.SelectMemberInterface;
import com.example.dairy.utils.ThermalOperation;
import com.example.dairy.utils.Utils;
import com.example.dairy.webService.APIController;
import com.example.dairy.webService.ResponseInterface;

import org.json.JSONObject;

import java.io.IOException;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Response;

public class MilkBuyActivity extends MyAppBaseActivity implements View.OnClickListener, ResponseInterface,
        MemberClickInterface, SelectMemberInterface {

    ActivityMilkBuyBinding binding;
    MilkBuyAdapter milkBuyAdapter;
    final Calendar myCalendar = Calendar.getInstance();
    DatePickerDialog.OnDateSetListener date;
    boolean isMorning = Utils.isMorning();
    boolean isBuffalo = true;
    private final String TAG = "MilkBuy";
    private APIController apiController;
    private boolean isEditable = false;
    private int editableUSerPosition = -1;
    private List<Map<Integer, Object>> cowRateChat = new ArrayList<>();
    private List<Map<Integer, Object>> buffaloRateChat = new ArrayList<>();
    private Integer editable_id;
    private Float _FAT_PD = 0f;
    private Float literPD = 0f;
    private Float govtPD = 0f;

    private boolean enableCalculateByLimit = false;
    private float cowFatLimit = 0f;

    //
    private boolean userIdValid = false;
    private int userID = 0;
    ThermalOperation thermalOperation;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_milk_buy);
        getSupportActionBar().setTitle(R.string.categories_milk_buy);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        init();
        getPDValue();
    }

    @Override
    protected void onResume() {
        super.onResume();
        enableCalculateByLimit = PreferenceManager.getValueBoolean(Constants.CALCULATE_BY_FAT);
        cowFatLimit = PreferenceManager.getFloatValue(Constants.COW_FAT_LIMIT);
    }

    private void init() {


        apiController = new APIController(this);
        thermalOperation = new ThermalOperation(this);

        binding.llSelectDate.setOnClickListener(this);
        binding.llSelectPet.setOnClickListener(this);
        binding.llSelectTime.setOnClickListener(this);
        binding.addRecord.setOnClickListener(this);
        binding.tvMemberId.setOnClickListener(this);
        binding.tvMemberName.setOnClickListener(this);
        binding.llEditableLayout.setOnClickListener(this);

        binding.myCompanyName.setSelected(true);
        binding.llEditableLayout.setVisibility(View.GONE);


        String DairyName = PreferenceManager.getValue(Constants.DAIRY_NAME);
        binding.myCompanyName.setText(DairyName + "  " + getString(R.string.milk_buy_note));
        cowRateChat = Utils.convertStringToChartList(PreferenceManager.getValue(Constants.COW_CHART_DATA));
        buffaloRateChat = Utils.convertStringToChartList(PreferenceManager.getValue(Constants.BUFFALO_CHART_DATA));

        updateDate();
        updateTimeSlot(isMorning);
        updatePet(isBuffalo);

        //
        date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateDate();
            }

        };


        binding.tvFat.addTextChangedListener(setValueInTotalAmountAndLiterRate);
        binding.tvLiter.addTextChangedListener(setValueInTotalAmountAndLiterRate);
        binding.tvSNF.addTextChangedListener(setValueInTotalAmountAndLiterRate);

        binding.tvMemberId.addTextChangedListener(setMemberId);


        LinearLayoutManager manager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        binding.buyRecordView.setLayoutManager(manager);
        milkBuyAdapter = new MilkBuyAdapter(this, this);
        binding.buyRecordView.setAdapter(milkBuyAdapter);

        noDataFoundManage();

    }

    private void noDataFoundManage() {
        if (milkBuyAdapter.list.size() > 0) {
            binding.buyRecordView.setVisibility(View.VISIBLE);
            binding.llNoDataFound.setVisibility(View.GONE);
        } else {
            binding.buyRecordView.setVisibility(View.GONE);
            binding.llNoDataFound.setVisibility(View.VISIBLE);
        }
    }

    private void getPDValue() {
        _FAT_PD = Float.valueOf(Utils.isNotEmpty(PreferenceManager.getValue(Constants.FAT_PD)) ? PreferenceManager.getValue(Constants.FAT_PD) : "0.00");
        literPD = Float.valueOf(Utils.isNotEmpty(PreferenceManager.getValue(Constants.LITER_PD)) ? PreferenceManager.getValue(Constants.LITER_PD) : "0.00");
        govtPD = Float.valueOf(Utils.isNotEmpty(PreferenceManager.getValue(Constants.GOVT_PD)) ? PreferenceManager.getValue(Constants.GOVT_PD) : "0.00");
    }

    private void updatePet(boolean isBuffalo) {

        if (isBuffalo) {
            binding.ivPet.setImageResource(R.drawable.buffalo);
            binding.tvSelectedPet.setText(R.string.buffalo);
        } else {
            binding.ivPet.setImageResource(R.drawable.cow);
            binding.tvSelectedPet.setText(R.string.cow);
        }
    }

    private void updateTimeSlot(boolean isMorning) {

        if (isMorning) {
            binding.ivTimer.setImageResource(R.drawable.sun);
            binding.tvSelectedTime.setText(R.string.morning);
        } else {
            binding.ivTimer.setImageResource(R.drawable.moon);
            binding.tvSelectedTime.setText(R.string.evening);
        }
    }


    TextWatcher setValueInTotalAmountAndLiterRate = new TextWatcher() {
        @Override
        public void afterTextChanged(Editable s) {
            binding.etLiterRate.setText(Utils.arrangeFloatValue(getTotalAmount(Constants.GET_VALUE_TYPE.GET_LITER_PRICE)));
            binding.etTotalAmount.setText(Utils.arrangeFloatValue(getTotalAmount(Constants.GET_VALUE_TYPE.GET_TOTAL_PRICE)));
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }


    };

    TextWatcher setMemberId = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            Debug.e(TAG, "Call add ID =---->");
            if (Utils.isNotEmpty(charSequence.toString())) {

                Result result = Utils.checkIDValidOrNot(charSequence.toString());
                if (result != null) {
                    userIdValid = true;
                    binding.ivIsValidUser.setVisibility(View.VISIBLE);
                    binding.tvMemberName.setText(result.getCustomerName());
                    userID = result.getId();
                } else {
                    binding.tvMemberName.setText("");
                    binding.ivIsValidUser.setVisibility(View.GONE);
                    userID = 0;
                }

            } else {
                userIdValid = false;
                binding.tvMemberName.setText("");
                binding.ivIsValidUser.setVisibility(View.GONE);
            }
        }

        @Override
        public void afterTextChanged(Editable editable) {

        }
    };


    private float getTotalAmount(Constants.GET_VALUE_TYPE type) {
        try {

            String FAT = Utils.isNotEmpty(binding.tvFat.getText().toString().trim()) ? binding.tvFat.getText().toString().trim() : "";
            String SNF = Utils.isNotEmpty(binding.tvSNF.getText().toString().trim()) ? binding.tvSNF.getText().toString().trim() : "";
            float liter = Utils.isNotEmpty(binding.tvLiter.getText().toString().trim()) ? Float.parseFloat(binding.tvLiter.getText().toString().trim()) : 0F;

            String finalFAT = FAT;
            String finalSNF = SNF;

            if (Utils.isStringInteger(FAT)) {
                Debug.e(TAG, "FAT Value after convert float =--->" + String.valueOf((float) Integer.parseInt(FAT)));
                finalFAT = String.valueOf((float) Integer.parseInt(FAT));
            }

            if (Utils.isStringInteger(SNF)) {
                Debug.e(TAG, "SNF Value after convert float =--->" + String.valueOf((float) Integer.parseInt(SNF)));
                finalSNF = String.valueOf((float) Integer.parseInt(SNF));
            }


            float literAmount = 0f;
            // calculate use by FAT value
            if (enableCalculateByLimit) {

                if (Float.parseFloat(FAT) > cowFatLimit) {
                    // This record consider as buffalo
                    isBuffalo = true;
                    updatePet(true);

                } else {
                    // This record consider as cow
                    isBuffalo = false;
                    updatePet(false);
                }
            } else {
//                literAmount = getTotalAmount(isBuffalo ? buffaloRateChat : cowRateChat, finalFAT, finalSNF);
            }
            literAmount = getTotalAmount(isBuffalo ? buffaloRateChat : cowRateChat, finalFAT, finalSNF);
//            float literAmount = getTotalAmount(isBuffalo ? buffaloRateChat : cowRateChat, finalFAT, finalSNF);
            float totalAmount = literAmount * liter;


            // add PD amount
            float literPDData = liter * literPD;
            float fatPDData = Float.valueOf(FAT) * _FAT_PD;
            float withPDLiterRate = literAmount + literPDData + fatPDData + govtPD;
            float withPDTotalRate = withPDLiterRate * liter;


            Debug.e(TAG, "LiterAmount =--->" + String.valueOf(literAmount));
            Debug.e(TAG, "TotalAmount =--->" + String.valueOf(totalAmount));
            Debug.e(TAG, "WithPDTotalRate =--->" + String.valueOf(withPDTotalRate));

            if (type.equals(Constants.GET_VALUE_TYPE.GET_LITER_PRICE)) {
                return literAmount;

            } else if (type.equals(Constants.GET_VALUE_TYPE.GET_TOTAL_PRICE)) {
                return Utils.arrangeFloatValueReturnFloat(totalAmount);

            } else if (type.equals(Constants.GET_VALUE_TYPE.GET_TOTAL_PRICE_WITH_PD)) {
                return Utils.arrangeFloatValueReturnFloat(withPDTotalRate);
            }


        } catch (Exception e) {
            e.printStackTrace();
        }

        return 0.0f;
    }


    private float getTotalAmount(List<Map<Integer, Object>> maps, String fat, String snf) {
        Debug.e(TAG, "Get Total calculation :- " + fat + snf);
        if (Utils.isNotEmpty(fat) && Utils.isNotEmpty(snf)) {
            if (maps != null && maps.size() > 0) {
                Map<Integer, Object> data = maps.get(0);
                for (Map.Entry<Integer, Object> entry : data.entrySet()) {
                    int key = entry.getKey();
                    String value = String.valueOf(entry.getValue());
                    // do stuff
                    if (snf.toUpperCase().equals(value)) {
                        for (int i = 0; i < maps.size(); i++) {
                            Map<Integer, Object> singleRecord = maps.get(i);
                            if (singleRecord.get(0).equals(fat)) {
                                return Float.parseFloat(String.valueOf(singleRecord.get(key)));
                            }
                        }
                    }
                }
            } else {
                Toast.makeText(this, "List is empty", Toast.LENGTH_SHORT).show();
            }
        }
        return 0;
    }


    private void updateDate() {
        String myFormat = "dd-MM-yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        binding.tvSelectedDate.setText(sdf.format(myCalendar.getTime()));
        getMilkBuy();
    }

    private void clearData() {
        isEditable = false;
        editableUSerPosition = -1;

        binding.tvMemberName.setText("");
        binding.tvMemberId.setText("");
        binding.tvFat.setText("0");
        binding.tvSNF.setText("0");
        binding.etLiterRate.setText("0");
        binding.tvLiter.setText("0");
        binding.etTotalAmount.setText("0");
        binding.llEditableLayout.setVisibility(View.GONE);
    }

    private void addMilkBuy() {

        String memberName = binding.tvMemberName.getText().toString().trim();
        String memberId = binding.tvMemberId.getText().toString().trim();
        String memberPrimaryCode = String.valueOf(userID);
        String fat = binding.tvFat.getText().toString().trim();
        String snf = binding.tvSNF.getText().toString().trim();
        String literRate = binding.etLiterRate.getText().toString().trim();
        String totalLiter = binding.tvLiter.getText().toString().trim();
        String totalAmount = binding.etTotalAmount.getText().toString().trim();
        String selectedDate = binding.tvSelectedDate.getText().toString().trim();
        String timeSlot = binding.tvSelectedTime.getText().toString().trim();
        String selectedAnimal = binding.tvSelectedPet.getText().toString().trim();


        if (Utils.isEmpty(memberId) || Utils.isEmpty(memberName)) {
            Utils.showSnackbar(this, binding.llMainLayout, getResources().getString(R.string.message_select_first_member));
            return;
        }

        if (Utils.isEmpty(totalLiter)) {
            Utils.showSnackbar(this, binding.llMainLayout, getResources().getString(R.string.message_enter_liter_value));
            return;
        }

        if (Utils.isEmpty(fat)) {
            Utils.showSnackbar(this, binding.llMainLayout, getResources().getString(R.string.message_enter_fat_value));
            return;
        }

        if (Utils.isEmpty(snf)) {
            Utils.showSnackbar(this, binding.llMainLayout, getResources().getString(R.string.message_enter_snf_value));
            return;
        }

        if ((int) Float.parseFloat(totalAmount) <= 0) {
            Utils.showSnackbar(this, binding.llMainLayout, getResources().getString(R.string.message_enter_valid_details));
            return;
        }

        String dairyId = String.valueOf(PreferenceManager.getValueInt(Constants.DAIRY_ID));
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("DairyId", dairyId);
            jsonObject.put("CustomerId", memberPrimaryCode);
            jsonObject.put("liter", totalLiter);
            jsonObject.put("fat", Utils.isStringInteger(fat) ? String.valueOf((float) Integer.parseInt(fat)) : fat);
            jsonObject.put("snf", Utils.isStringInteger(snf) ? String.valueOf((float) Integer.parseInt(snf)) : snf);
            jsonObject.put("rate", literRate);
            jsonObject.put("amount", Utils.arrangeFloatValue(getTotalAmount(Constants.GET_VALUE_TYPE.GET_TOTAL_PRICE)));
            jsonObject.put("addDate", Utils.finalDateFormat(selectedDate, isMorning));
            jsonObject.put("timeslot", isMorning ? Constants.MORNING : Constants.EVENING);
            jsonObject.put("animalType", isBuffalo ? Constants.BUFFALO : Constants.COW);

            jsonObject.put("fat_pd", _FAT_PD);
            jsonObject.put("liter_pd", literPD);
            jsonObject.put("govt_pd", govtPD);
            jsonObject.put("total_with_pd_rate", Utils.arrangeFloatValue(getTotalAmount(Constants.GET_VALUE_TYPE.GET_TOTAL_PRICE_WITH_PD)));

            if (!Utils.isInternetConnected(this)) {
                AlertMassage.MassageAlert(this, getResources().getString(R.string.message_intent_conection));
                return;
            }

            binding.llProgressBar.setVisibility(View.VISIBLE);
            Utils.hideSoftKeyword(this);
            RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), jsonObject.toString());

            if (isEditable) {
                isEditable = false;
                apiController.editMilkBuy(Constants.API_TYPE.EDIT_MILK_BUY, editable_id.toString(), body);
            } else {
                apiController.addMilkBuy(Constants.API_TYPE.ADD_MILK_BUY, body);

            }

        } catch (Exception e) {
            Debug.e(TAG, "Error" + e.getMessage());
        }
    }

    private void getMilkBuy() {

        String selectedDate = binding.tvSelectedDate.getText().toString().trim();

        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("addDate", Utils.finalDateFormat(selectedDate, isMorning));
            jsonObject.put("timeslot", isMorning ? Constants.MORNING : Constants.EVENING);
            jsonObject.put("animalType", "All");


            if (!Utils.isInternetConnected(this)) {
                AlertMassage.MassageAlert(this, getResources().getString(R.string.message_intent_conection));
                return;
            }

            binding.llProgressBar.setVisibility(View.VISIBLE);
            Utils.hideSoftKeyword(this);
            RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), jsonObject.toString());

            apiController.getMilkBuy(Constants.API_TYPE.GET_MILK_BUY, body);


        } catch (Exception e) {
            Debug.e(TAG, "Error" + e.getMessage());
            binding.llProgressBar.setVisibility(View.GONE);
        }
    }


    private void deleteMilkBuy(Result result) {
        try {
            if (!Utils.isInternetConnected(this)) {
                AlertMassage.MassageAlert(this, getResources().getString(R.string.message_intent_conection));
                return;
            }
            binding.llProgressBar.setVisibility(View.VISIBLE);
            Utils.hideSoftKeyword(this);
            apiController.deleteMilkBuy(Constants.API_TYPE.DELETE_MASTER, String.valueOf(result.getId()));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void openBottomSheet() {

        SearchMemberFragment searchMemberFragment =
                SearchMemberFragment.newInstance("BUYER", "");
        searchMemberFragment.show(getSupportFragmentManager(),
                "set_value_dialog_fragment");
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.llSelectDate:
                new DatePickerDialog(this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
                break;

            case R.id.llSelectPet:
                isBuffalo = !isBuffalo;
                updatePet(isBuffalo);
//                getMilkBuy();
                break;

            case R.id.llSelectTime:
                isMorning = !isMorning;
                updateTimeSlot(isMorning);
                getMilkBuy();
                break;

            case R.id.addRecord:
//                new AlertDialog.Builder(this)
//                        .setIcon(R.drawable.ic_error)
//                        .setTitle("Alert")
//                        .setMessage("You are already add Lakhanbhai milk Do you add once again")
//                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialog, int which) {
//                            }
//
//                        })
//                        .setNegativeButton("No", null)
//                        .show();
                addMilkBuy();
                break;

            case R.id.tvMemberName:
                openBottomSheet();
                break;

            case R.id.llEditableLayout:
                clearData();
                break;
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.setting, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_setting:
                Intent openSetting = new Intent(this, MilkBuySetting.class);
                startActivity(openSetting);
                return true;

            case android.R.id.home:
                onBackPressed();
                return true;


            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }

    @Override
    public void successResponse(Constants.API_TYPE type, Object response, int code) {
        binding.llProgressBar.setVisibility(View.GONE);
        switch (type) {
            case ADD_MILK_BUY:
                try {
                    if (code == Constants.RESULT_OK) {
                        CommonResponse addMilkBuyResponse = ((Response<CommonResponse>) response).body();
                        if (addMilkBuyResponse != null && addMilkBuyResponse.getStatus()) {
                            milkBuyAdapter.addSingleRecord(addMilkBuyResponse.getResult().get(0));
                            clearData();
                            noDataFoundManage();
                            if (checkPrintEnable()) {
                                thermalOperation.printSingleBuy(addMilkBuyResponse.getResult().get(0));
                            }

                        } else {
                            Utils.showSnackbar(this, binding.llMainLayout, addMilkBuyResponse.getMessage());
                        }

                    } else {
                        String error = null;
                        try {
                            error = ((retrofit2.Response<String>) response).errorBody().string();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        CommonResponse errorResponse = Utils.convertStringToList(error);
                        Utils.showSnackbar(this, binding.llMainLayout, errorResponse.getMessage());

                    }
                } catch (Exception e) {
                    Debug.e(TAG, "ADD_MILK_BUY" + e.getMessage());
                }
                break;

            case EDIT_MILK_BUY:
                try {
                    if (code == Constants.RESULT_OK) {
                        CommonResponse editMasterResponse = ((Response<CommonResponse>) response).body();
                        if (editMasterResponse != null && editMasterResponse.getStatus()) {
                            Utils.showSnackbar(this, binding.llMainLayout, editMasterResponse.getMessage());
                            milkBuyAdapter.editData(editMasterResponse.getResult().get(0), editableUSerPosition);
                            clearData();

                        } else {
                            Utils.showSnackbar(this, binding.llMainLayout, editMasterResponse.getMessage());
                        }

                    } else {
                        Utils.showSnackbar(this, binding.llMainLayout, "API Error");

                    }
                } catch (Exception e) {
                    Debug.e(TAG, "EDIT_MILK_BUY" + e.getMessage());
                }
                break;

            case REMOVE_MILK_BUY:
                try {
                    if (code == Constants.RESULT_OK) {
                        CommonResponse removeMilkBuyResponse = ((Response<CommonResponse>) response).body();
                        if (removeMilkBuyResponse != null && removeMilkBuyResponse.getStatus()) {
                            noDataFoundManage();
                        } else {
                        }

                    } else {
                        Utils.showSnackbar(this, binding.llMainLayout, "API Error");

                    }
                } catch (Exception e) {
                    Debug.e(TAG, "REMOVE_MILK_BUY" + e.getMessage());
                }
                break;

            case GET_MILK_BUY:
                try {
                    if (code == Constants.RESULT_OK) {
                        CommonResponse getMilkDataResponse = ((Response<CommonResponse>) response).body();
                        if (getMilkDataResponse != null && getMilkDataResponse.getStatus()) {
                            Utils.showSnackbar(this, binding.llMainLayout, getMilkDataResponse.getMessage());
                            Debug.e(TAG, getMilkDataResponse.getResult().toString());
                            milkBuyAdapter.addData(getMilkDataResponse.getResult());
                            noDataFoundManage();
                        } else {
                            Utils.showSnackbar(this, binding.llMainLayout, getMilkDataResponse.getMessage());
                        }

                    } else {
                        Utils.showSnackbar(this, binding.llMainLayout, "API Error");
                    }
                } catch (Exception e) {
                    Debug.e(TAG, "GET_MILK_BUY" + e.getMessage());
                }
                break;
        }
    }

    private boolean checkPrintEnable() {
        return PreferenceManager.getValueBoolean(Constants.PRINT_ENABLE);
    }

    @Override
    public void failureResponse(Constants.API_TYPE type, String error) {
        binding.llProgressBar.setVisibility(View.GONE);
        Debug.e(TAG, "Fail Error =----->" + error);
    }

    @Override
    public void delete(Result result, int position) {
        deleteMilkBuy(result);
    }

    @Override
    public void view(Result result, int position) {
        Intent intent = new Intent(this, MilkBuyDetailsActivity.class);
        intent.putExtra("data", (Serializable) result);
        startActivity(intent);


    }

    @Override
    public void edit(Result result, int position) {
        isEditable = true;
        editableUSerPosition = position;

        editable_id = result.getId();
        binding.tvMemberId.setText(String.valueOf(result.getCustomer().getMemberCode()));
        binding.tvMemberName.setText(result.getCustomer().getCustomerName());
        userID = result.getCustomerId();

        binding.tvSelectedDate.setText(result.getAddDate());

        binding.tvFat.setText(result.getFat());
        binding.tvSNF.setText(result.getSnf());
        binding.tvLiter.setText(result.getLiter());

        binding.etTotalAmount.setText(result.getAmount());
        binding.etLiterRate.setText(result.getRate());

        if (result.getAnimalType().equals(getResources().getString(R.string.buffalo))) {
            isBuffalo = true;
        } else {
            isBuffalo = false;
        }
        updatePet(isBuffalo);


        if (result.getTimeslot().equals(getResources().getString(R.string.morning))) {
            isMorning = true;
        } else {
            isMorning = false;
        }
        updateTimeSlot(isMorning);
        binding.llEditableLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void print(Result result, int position) {
        thermalOperation.printSingleBuy(result);
    }

    @Override
    public void selectedMember(Result result) {
        binding.tvMemberId.setText(String.valueOf(result.getMemberCode()));
        binding.tvMemberName.setText(result.getCustomerName());
        userID = result.getId();
    }
}