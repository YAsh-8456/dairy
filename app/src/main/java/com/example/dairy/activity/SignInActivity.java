package com.example.dairy.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.View;

import androidx.databinding.DataBindingUtil;

import com.example.dairy.R;
import com.example.dairy.databinding.ActivityLoginBinding;
import com.example.dairy.modal.CommonResponse;
import com.example.dairy.utils.AlertMassage;
import com.example.dairy.utils.Constants;
import com.example.dairy.utils.CreateChartInterface;
import com.example.dairy.utils.Debug;
import com.example.dairy.utils.PreferenceManager;
import com.example.dairy.utils.Utils;
import com.example.dairy.webService.APIController;
import com.example.dairy.webService.ResponseInterface;

import org.json.JSONObject;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Response;

public class SignInActivity extends MyAppBaseActivity implements View.OnClickListener, ResponseInterface, CreateChartInterface {

    ActivityLoginBinding binding;
    private static final String TAG = "LoginActivity";
    private APIController apiController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login);
        init();
    }

    private void init() {

        binding.tvLoginClick.setOnClickListener(this);
        binding.tvSignUpClick.setOnClickListener(this);

        apiController = new APIController(this);
    }

    public Bitmap textAsBitmap(String text, float textSize, int textColor) {
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setTextSize(textSize);
        paint.setColor(textColor);
        paint.setTextAlign(Paint.Align.LEFT);

        float baseline = -paint.ascent(); // ascent() is negative
        int width = (int) (paint.measureText(text) + 0.5f); // round
        int height = (int) (baseline + paint.descent() + 0.5f);
        Bitmap image = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(image);
        canvas.drawText("रिपोर्ट प्राप्त करें", 0, baseline, paint);
        canvas.drawText("दूध बिक्री की रिपोर्", 0, baseline, paint);
        return image;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvLoginClick:
                checkValidation();
                break;

            case R.id.tvSignUpClick:
                Intent openSignUpScreen = new Intent(SignInActivity.this, SignUpActivity.class);
                startActivity(openSignUpScreen);
                break;
        }
    }

    private void checkValidation() {

        String userName = binding.etUser.getText().toString().trim();
        String password = binding.etPassword.getText().toString().trim();

        if (Utils.isEmpty(userName)) {
            Utils.showSnackbar(this, binding.llMainLayout, getResources().getString(R.string.message_enter_username));
            return;
        }

        if (Utils.isEmpty(password)) {
            Utils.showSnackbar(this, binding.llMainLayout, getResources().getString(R.string.message_enter_password));
            return;
        }

        callLoginApi();
    }

    private void callLoginApi() {

        try {
            String userName = binding.etUser.getText().toString().trim();
            String password = binding.etPassword.getText().toString().trim();

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("userName", userName);
            jsonObject.put("password", password);


            if (!Utils.isInternetConnected(SignInActivity.this)) {
                AlertMassage.MassageAlert(this, getResources().getString(R.string.message_intent_conection));
                return;
            }

            binding.llProgressBar.setVisibility(View.VISIBLE);
            Utils.hideSoftKeyword(SignInActivity.this);
            RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), jsonObject.toString());
            apiController.dairyLogin(Constants.API_TYPE.DAIRY_LOGIN, body);

        } catch (Exception e) {
            Debug.e(TAG, "Error" + e.getMessage());
        }

    }


    @Override
    public void successResponse(Constants.API_TYPE type, Object response, int code) {
        binding.llProgressBar.setVisibility(View.GONE);
        switch (type) {
            case DAIRY_LOGIN:
                try {
                    if (code == Constants.RESULT_OK) {
                        CommonResponse loginResponse = ((Response<CommonResponse>) response).body();
                        if (loginResponse != null && loginResponse.getStatus()) {
                            Utils.saveDairyDetails(loginResponse.get_token(), loginResponse.getData().get(0));
                            Utils.setDefaultData(this, this);

                        } else {
                            Utils.showSnackbar(this, binding.llMainLayout, loginResponse.getMessage());
                        }

                    } else {
                        String error = null;
                        try {
                            error = ((retrofit2.Response<String>) response).errorBody().string();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        CommonResponse errorResponse = Utils.convertStringToList(error);
                        Utils.showSnackbar(this, binding.llMainLayout, errorResponse.getMessage());

                    }
                } catch (Exception e) {
                    Debug.e(TAG, "Login Exception" + e.getMessage());
                }
                break;
        }

    }


    @Override
    public void failureResponse(Constants.API_TYPE type, String error) {
        binding.llProgressBar.setVisibility(View.GONE);
        Debug.e(TAG, "Fail Error =----->" + error);
    }

    @Override
    public void createdChart(boolean isBuffalo, List<Map<Integer, Object>> data) {


        if (isBuffalo) {
            PreferenceManager.putValue(Constants.BUFFALO_CHART_DATA, Utils.convertChartListToString(data));
            Debug.e(TAG, "Get Buffalo data =--->: " + Utils.convertChartListToString(data));
            Utils.createChartData(this, this, Constants.cowStartFAT, Constants.cowEndFAT, Constants.cowStartSNF, Constants.cowEndSNF, Constants.fatRateData, Constants.snfRateData, Constants.commissionData, Constants.dedicationData, false);
        } else {
            PreferenceManager.putValue(Constants.COW_CHART_DATA, Utils.convertChartListToString(data));
            Debug.e(TAG, "Get Cow data =--->: " + Utils.convertChartListToString(data));
        }

        if (Utils.isNotEmpty(PreferenceManager.getValue(Constants.BUFFALO_CHART_DATA)) && Utils.isNotEmpty(PreferenceManager.getValue(Constants.COW_CHART_DATA))) {
            Debug.e(TAG, "Create Chart =--->" + isBuffalo);
            Intent openMainScreen = new Intent(SignInActivity.this, MainActivity.class);
            startActivity(openMainScreen);
            finish();
        }


    }

    @Override
    public void createdCowChart(boolean isBuffalo, List<Map<Integer, Object>> data) {

    }

    @Override
    public void createdBuffaloChart(boolean isBuffalo, List<Map<Integer, Object>> data) {

    }
}