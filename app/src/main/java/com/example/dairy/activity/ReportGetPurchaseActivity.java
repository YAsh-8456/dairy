package com.example.dairy.activity;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.Toast;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.example.dairy.R;
import com.example.dairy.adapter.MilkBuyAdapter;
import com.example.dairy.databinding.ActivityGetPurchaseReportBinding;
import com.example.dairy.modal.CommonResponse;
import com.example.dairy.modal.Result;
import com.example.dairy.utils.AlertMassage;
import com.example.dairy.utils.Constants;
import com.example.dairy.utils.Debug;
import com.example.dairy.utils.MemberClickInterface;
import com.example.dairy.utils.PDFOperation;
import com.example.dairy.utils.ThermalOperation;
import com.example.dairy.utils.Utils;
import com.example.dairy.webService.APIController;
import com.example.dairy.webService.ResponseInterface;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Response;

public class ReportGetPurchaseActivity extends MyAppBaseActivity implements View.OnClickListener, ResponseInterface, MemberClickInterface {

    ActivityGetPurchaseReportBinding binding;
    final Calendar myCalendar = Calendar.getInstance();
    DatePickerDialog.OnDateSetListener startDate;
    DatePickerDialog.OnDateSetListener endDate;
    MilkBuyAdapter milkBuyAdapter;
    boolean isMorning = Utils.isMorning();
    private String TAG = "MilkBuy";
    private APIController apiController;
    private ThermalOperation thermalOperation;
    private PDFOperation pdfOperation;
    private String selectedPet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_get_purchase_report);
        String title = getIntent().getExtras().getString("title");
        getSupportActionBar().setTitle(title);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        init();
    }

//    private void updatePet(boolean isBuffalo) {
//
//        if (isBuffalo) {
//            binding.ivPet.setImageResource(R.drawable.buffalo);
//            binding.tvSelectedPet.setText(R.string.buffalo);
//        } else {
//            binding.ivPet.setImageResource(R.drawable.cow);
//            binding.tvSelectedPet.setText(R.string.cow);
//        }
//    }

    private void selectedPet(String petType) {
        selectedPet = petType;
        if (petType.equals(Constants.COW)) {
            binding.llCowSelected.setBackgroundColor(getResources().getColor(R.color.lightGreen));
            binding.llBuffaloSelected.setBackgroundColor(getResources().getColor(R.color.btn_color));
            binding.llAllSelected.setBackgroundColor(getResources().getColor(R.color.btn_color));


        } else if (petType.equals(Constants.BUFFALO)) {
            binding.llCowSelected.setBackgroundColor(getResources().getColor(R.color.btn_color));
            binding.llBuffaloSelected.setBackgroundColor(getResources().getColor(R.color.lightGreen));
            binding.llAllSelected.setBackgroundColor(getResources().getColor(R.color.btn_color));

        } else if (petType.equals(Constants.ALL)) {
            binding.llCowSelected.setBackgroundColor(getResources().getColor(R.color.btn_color));
            binding.llBuffaloSelected.setBackgroundColor(getResources().getColor(R.color.btn_color));
            binding.llAllSelected.setBackgroundColor(getResources().getColor(R.color.lightGreen));


        }
    }

    private void updateTimeSlot(boolean isMorning) {

        if (isMorning) {
            binding.ivTimer.setImageResource(R.drawable.sun);
            binding.tvSelectedTime.setText(R.string.morning);
        } else {
            binding.ivTimer.setImageResource(R.drawable.moon);
            binding.tvSelectedTime.setText(R.string.evening);
        }
    }

    private void init() {

        thermalOperation = new ThermalOperation(this);
        pdfOperation = new PDFOperation(this);

        binding.llSelectDate.setOnClickListener(this);
//        binding.llSelectPet.setOnClickListener(this);
        binding.llCowSelected.setOnClickListener(this);
        binding.llBuffaloSelected.setOnClickListener(this);
        binding.llAllSelected.setOnClickListener(this);
        binding.llSelectTime.setOnClickListener(this);
        binding.includeReportBtn.llGetReport.setOnClickListener(this);

        updateDate();
        updateTimeSlot(isMorning);
        selectedPet(Constants.COW);

        apiController = new APIController(this);
        //
        startDate = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateDate();
            }

        };


        LinearLayoutManager manager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        binding.buyRecordView.setLayoutManager(manager);
        milkBuyAdapter = new MilkBuyAdapter(this, this);
        binding.buyRecordView.setAdapter(milkBuyAdapter);
        milkBuyAdapter.manageViewClick(false);
        noDataFoundManage();
    }


    private void noDataFoundManage() {
        if (milkBuyAdapter.list.size() > 0) {
            binding.buyRecordView.setVisibility(View.VISIBLE);
            binding.llNoDataFound.setVisibility(View.GONE);
        } else {
            binding.buyRecordView.setVisibility(View.GONE);
            binding.llNoDataFound.setVisibility(View.VISIBLE);
        }
    }

    private void updateDate() {
        String myFormat = "dd-MM-yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        binding.tvSelectedDate.setText(sdf.format(myCalendar.getTime()));


    }


    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.llSelectDate:
                new DatePickerDialog(this, startDate, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
                break;


            case R.id.llCowSelected:
                selectedPet(Constants.COW);
                break;

            case R.id.llBuffaloSelected:
                selectedPet(Constants.BUFFALO);
                break;

            case R.id.llAllSelected:
                selectedPet(Constants.ALL);
                break;

            case R.id.llSelectTime:
                isMorning = !isMorning;
                updateTimeSlot(isMorning);
                break;

            case R.id.llGetReport:
                Toast.makeText(this, "Click get Report", Toast.LENGTH_SHORT).show();
                getReportAPICall();
                break;
        }
    }

    private void getReportAPICall() {

        String selectedDate = binding.tvSelectedDate.getText().toString().trim();

        String timeSlot = binding.tvSelectedTime.getText().toString().trim();
        String selectedAnimal = selectedPet.equals(Constants.ALL) ? "All" : selectedPet;

        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("addDate",  Utils.finalDateFormat(selectedDate, isMorning));
            jsonObject.put("timeslot", isMorning ? Constants.MORNING : Constants.EVENING);
            jsonObject.put("animalType", selectedAnimal);


            if (!Utils.isInternetConnected(this)) {
                AlertMassage.MassageAlert(this, getResources().getString(R.string.message_intent_conection));
                return;
            }

            binding.llProgressBar.setVisibility(View.VISIBLE);
            Utils.hideSoftKeyword(this);
            RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), jsonObject.toString());

            apiController.getMilkBuy(Constants.API_TYPE.GET_PURCHASE_REPORT, body);


        } catch (Exception e) {
            Debug.e(TAG, "Error" + e.getMessage());
            binding.llProgressBar.setVisibility(View.GONE);
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.report_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.action_save_pdf:
                if (milkBuyAdapter.list.size() > 0) {
                    pdfOperation.createPdfForPurchaseList(false, milkBuyAdapter.list);
                } else {
                    Toast.makeText(this, R.string.no_record_found, Toast.LENGTH_SHORT).show();
                }
                return true;

            case R.id.action_print_pdf:
                if (milkBuyAdapter.list.size() > 0) {
                    pdfOperation.createPdfForPurchaseList(true, milkBuyAdapter.list);
                } else {
                    Toast.makeText(this, R.string.no_record_found, Toast.LENGTH_SHORT).show();
                }
                return true;

            case R.id.action_thermal_print:
                if (milkBuyAdapter.list.size() > 0) {
                    thermalOperation.printPurchaseList(milkBuyAdapter.list, isMorning ? Constants.MORNING : Constants.EVENING, selectedPet);
                } else {
                    Toast.makeText(this, R.string.no_record_found, Toast.LENGTH_SHORT).show();
                }
                return true;

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }

    @Override
    public void successResponse(Constants.API_TYPE type, Object response, int code) {
        binding.llProgressBar.setVisibility(View.GONE);
        switch (type) {
            case GET_PURCHASE_REPORT:
                try {
                    if (code == Constants.RESULT_OK) {
                        CommonResponse addMilkBuyResponse = ((Response<CommonResponse>) response).body();
                        if (addMilkBuyResponse != null && addMilkBuyResponse.getStatus()) {
                            milkBuyAdapter.addData(addMilkBuyResponse.getResult());
                            noDataFoundManage();
                        } else {
                            Utils.showSnackbar(this, binding.llMainLayout, addMilkBuyResponse.getMessage());
                        }

                    } else {
                        Utils.showSnackbar(this, binding.llMainLayout, "API Error");

                    }
                } catch (Exception e) {
                    Debug.e(TAG, "Login Exception" + e.getMessage());
                }
                break;

        }
    }

    @Override
    public void failureResponse(Constants.API_TYPE type, String error) {
        binding.llProgressBar.setVisibility(View.GONE);
        Debug.e(TAG, "Fail Error =----->" + error);
    }

    @Override
    public void delete(Result result, int position) {

    }

    @Override
    public void view(Result result, int position) {

    }

    @Override
    public void edit(Result result, int position) {

    }

    @Override
    public void print(Result result, int position) {
    }
}