package com.example.dairy.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.databinding.DataBindingUtil;

import com.example.dairy.R;
import com.example.dairy.databinding.ActivityRateBinding;
import com.example.dairy.utils.Constants;
import com.example.dairy.utils.PreferenceManager;
import com.example.dairy.utils.Utils;

import java.util.List;
import java.util.Map;

public class RateMainActivity extends MyAppBaseActivity implements View.OnClickListener{

    ActivityRateBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_rate);
        getSupportActionBar().setTitle(R.string.categories_milk_rate_chart);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        init();
    }

    private void init() {
        binding.llBuffaloSNFChart.setOnClickListener(this);
        binding.llCowSNFChart.setOnClickListener(this);
        binding.llLocalSellRate.setOnClickListener(this);
        binding.llUploadSNFChart.setOnClickListener(this);
        binding.llCreateSNFChart.setOnClickListener(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
               onBackPressed();
                return true;

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.llBuffaloSNFChart :

                List<Map<Integer, Object>> readExcelNew = Utils.convertStringToChartList(PreferenceManager.getValue(Constants.BUFFALO_CHART_DATA));
                if(readExcelNew != null && readExcelNew.size() > 0){
                    Intent openScreen = new Intent(this, RateChartShowActivity.class);
                    openScreen.putExtra("isBuffalo", true);
                    startActivity(openScreen);
                }else {
                    Toast.makeText(this, "Please Import chart first", Toast.LENGTH_SHORT).show();
                }

                break;

            case R.id.llCowSNFChart :

                List<Map<Integer, Object>> readExcelNew1 = Utils.convertStringToChartList(PreferenceManager.getValue(Constants.COW_CHART_DATA));

                if(readExcelNew1 != null && readExcelNew1.size() > 0){
                    Intent openScreen = new Intent(this, RateChartShowActivity.class);
                    openScreen.putExtra("isBuffalo", false);
                    startActivity(openScreen);
                }else {
                    Toast.makeText(this, "Please Import chart first", Toast.LENGTH_SHORT).show();
                }

                break;

            case R.id.llLocalSellRate :
                Intent sellScreen = new Intent(this, RateLocalSellActivity.class);
                startActivity(sellScreen);
                break;

            case R.id.llUploadSNFChart:
                Intent uploadScreen = new Intent(this, UploadChartActivity.class);
                uploadScreen.putExtra("createChart",false);
                startActivity(uploadScreen);
                break;

            case R.id.llCreateSNFChart:
                Intent uploadScreen1 = new Intent(this, UploadChartActivity.class);
                uploadScreen1.putExtra("createChart",true);
                startActivity(uploadScreen1);
                break;


        }
    }
}