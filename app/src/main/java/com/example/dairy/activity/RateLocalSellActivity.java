package com.example.dairy.activity;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.example.dairy.R;
import com.example.dairy.adapter.MasterAdapter;
import com.example.dairy.databinding.ActivityLocalSellRateBinding;
import com.example.dairy.modal.CommonResponse;
import com.example.dairy.modal.Result;
import com.example.dairy.utils.AlertMassage;
import com.example.dairy.utils.Constants;
import com.example.dairy.utils.Debug;
import com.example.dairy.utils.MemberClickInterface;
import com.example.dairy.utils.PreferenceManager;
import com.example.dairy.utils.Utils;
import com.example.dairy.webService.APIController;
import com.example.dairy.webService.ResponseInterface;
import com.google.gson.JsonObject;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Response;

public class RateLocalSellActivity extends MyAppBaseActivity implements View.OnClickListener, ResponseInterface, MemberClickInterface {

    ActivityLocalSellRateBinding binding;
    private MasterAdapter masterAdapter;
    private APIController apiController;
    private String TAG = "LocalSellRate";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_local_sell_rate);
        getSupportActionBar().setTitle(R.string.categories_milk_rate_chart);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        init();
    }

    private void init() {
        getLocalSaleRate();

        apiController = new APIController(this);
        binding.tvUpdateRate.setOnClickListener(this);

        LinearLayoutManager manager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        binding.masterView.setLayoutManager(manager);
        masterAdapter = new MasterAdapter(this, this);
        binding.masterView.setAdapter(masterAdapter);
        masterAdapter.isSearchMember(true);


        getMasterApiCall();
    }

    private void getMasterApiCall() {


        if (!Utils.isInternetConnected(this)) {
            AlertMassage.MassageAlert(this, getResources().getString(R.string.message_intent_conection));
            return;
        }

        JsonObject jsonObject = new JsonObject();
        try {
            jsonObject.addProperty("memberType", Constants.SELLER);
            jsonObject.addProperty("startMemberCode", (Number) null);
            jsonObject.addProperty("endMemberCode", (Number) null);


            binding.llProgressBar.setVisibility(View.VISIBLE);
            Utils.hideSoftKeyword(this);
            RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), jsonObject.toString());

            apiController.getMasterAPI(Constants.API_TYPE.GET_MEMBER_REPORT, body);

        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    private void getLocalSaleRate() {
        binding.etBuffaloSellRate.setText("" + PreferenceManager.getValueInt(Constants.BUFFALO_LOCAL_SALE_RATE));
        binding.etCowSellRate.setText("" + PreferenceManager.getValueInt(Constants.COW_LOCAL_SALE_RATE));
    }

    private void updateLocalRate() {

        int cowRate = Integer.valueOf(binding.etCowSellRate.getText().toString().trim());
        int buffalo = Integer.valueOf(binding.etBuffaloSellRate.getText().toString().trim());
        PreferenceManager.putValueInt(Constants.COW_LOCAL_SALE_RATE, cowRate);
        PreferenceManager.putValueInt(Constants.BUFFALO_LOCAL_SALE_RATE, buffalo);
        Toast.makeText(this, "Update rate successfully", Toast.LENGTH_SHORT).show();
        onBackPressed();
    }


    @Override
    public void successResponse(Constants.API_TYPE type, Object response, int code) {
        binding.llProgressBar.setVisibility(View.GONE);
        switch (type) {
            case GET_MEMBER_REPORT:
                try {
                    if (code == Constants.RESULT_OK) {
                        CommonResponse getMasterResponse = ((Response<CommonResponse>) response).body();
                        if (getMasterResponse != null && getMasterResponse.getStatus()) {
                            masterAdapter.addData(Utils.getSellerMemberList(getMasterResponse.getResult()));

                        } else {
                            Utils.showSnackbar(this, binding.llMainLayout, getMasterResponse.getMessage());
                        }

                    } else {
                        Utils.showSnackbar(this, binding.llMainLayout, "API Error");

                    }
                } catch (Exception e) {
                    Debug.e(TAG, "Get Master List Exception" + e.getMessage());
                }
                break;

        }

    }

    @Override
    public void failureResponse(Constants.API_TYPE type, String error) {
        binding.llProgressBar.setVisibility(View.GONE);
        Debug.e(TAG, "Fail Error =----->" + error);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tvUpdateRate:
                updateLocalRate();
                break;
        }
    }


    @Override
    public void delete(Result result, int position) {

    }

    @Override
    public void view(Result result, int position) {

    }

    @Override
    public void edit(Result result, int position) {

    }

    @Override
    public void print(Result result, int position) {

    }
}