package com.example.dairy.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingUtil;

import com.example.dairy.R;
import com.example.dairy.databinding.ActivitySettingBinding;
import com.example.dairy.modal.CommonResponse;
import com.example.dairy.utils.AlertMassage;
import com.example.dairy.utils.Constants;
import com.example.dairy.utils.Debug;
import com.example.dairy.utils.PreferenceManager;
import com.example.dairy.utils.Utils;
import com.example.dairy.webService.APIController;
import com.example.dairy.webService.ResponseInterface;

import retrofit2.Response;

public class SettingActivity extends MyAppBaseActivity implements View.OnClickListener, ResponseInterface {

    private static final String TAG = "Setting";
    ActivitySettingBinding binding;
    private static final int EDITABLE_DIARY_RECORD = 234;
    private APIController apiController;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_setting);
        getSupportActionBar().setTitle(R.string.categories_milk_setting);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        init();
    }

    private void init() {

        apiController = new APIController(this);

        binding.llDeleteAccount.setOnClickListener(this);
        binding.llEditDairy.setOnClickListener(this);
        binding.llLanguageUpdate.setOnClickListener(this);
        binding.llMilkSlipSetting.setOnClickListener(this);
        binding.llContactUs.setOnClickListener(this);
        binding.llDealer.setOnClickListener(this);
        binding.llFATMachineSetting.setOnClickListener(this);

//        binding.tvHindi.setOnClickListener(this);
//        binding.tvEnglish.setOnClickListener(this);
//
//
//        if (Utils.isNotEmpty(PreferenceManager.getValue(Constants.SELECTED_LANGUAGE))) {
//
//            if (PreferenceManager.getValue(Constants.SELECTED_LANGUAGE).equals(Constants.ENGLISH)) {
//                binding.tvHindi.setBackground(getResources().getDrawable(R.drawable.unselected_languange_bg));
//                binding.tvEnglish.setBackground(getResources().getDrawable(R.drawable.selected_languange_bg));
//
//            } else if (PreferenceManager.getValue(Constants.SELECTED_LANGUAGE).equals(Constants.HINDI)) {
//                binding.tvHindi.setBackground(getResources().getDrawable(R.drawable.selected_languange_bg));
//                binding.tvEnglish.setBackground(getResources().getDrawable(R.drawable.unselected_languange_bg));
//            }
//        } else {
//            binding.tvHindi.setBackground(getResources().getDrawable(R.drawable.unselected_languange_bg));
//            binding.tvEnglish.setBackground(getResources().getDrawable(R.drawable.selected_languange_bg));
//        }
    }

    private void dairyRemove() {
        try {
            if (!Utils.isInternetConnected(this)) {
                AlertMassage.MassageAlert(this, getResources().getString(R.string.message_intent_conection));
                return;
            }
            binding.llProgressBar.setVisibility(View.VISIBLE);
            Utils.hideSoftKeyword(this);
            apiController.dairyRemove(Constants.API_TYPE.DAIRY_DELETE);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.llDeleteAccount:
                new AlertDialog.Builder(this)
                        .setIcon(R.drawable.ic_error)
                        .setTitle("Delete")
                        .setMessage("Are you sure you delete dairy?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dairyRemove();
                            }

                        })
                        .setNegativeButton("No", null)
                        .show();
                break;

            case R.id.llEditDairy:
                Intent openDairyScreen = new Intent(SettingActivity.this, SignUpActivity.class);
                openDairyScreen.putExtra("isEditable", true);
                startActivityForResult(openDairyScreen, EDITABLE_DIARY_RECORD);
                break;

            case R.id.llLanguageUpdate:
                Intent openLanguageScreen = new Intent(SettingActivity.this, LanguageSelectionActivity.class);
                startActivity(openLanguageScreen);
                break;

            case R.id.llMilkSlipSetting:
                Intent openMilkScreen = new Intent(SettingActivity.this, MilkSlipSettingActivity.class);
                startActivity(openMilkScreen);
                break;

            case R.id.llFATMachineSetting:
                Intent openFATSettingScreen = new Intent(SettingActivity.this, SettingFATMachineActivity.class);
                startActivity(openFATSettingScreen);
                break;

            case R.id.llContactUs:
                /* Fill it with Data */
                final Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
                emailIntent.setData(Uri.parse("mailto:"));
                emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{Constants.CONTACT_US_MAIL});
                emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Subject");
                emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, "Text");

                /* Send it off to the Activity-Chooser */
                startActivity(Intent.createChooser(emailIntent, "Send mail..."));
                break;

            case R.id.llDealer:
                Intent openDealerDetailsScreen = new Intent(SettingActivity.this, DealerDetailsActivity.class);
                startActivity(openDealerDetailsScreen);
                break;


        }
    }

    private void restartActivity() {
        Intent intent = getIntent();
        finish();
        startActivity(intent);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {

            case EDITABLE_DIARY_RECORD:
                if (resultCode == RESULT_OK) {
                    Toast.makeText(this, "Update Dairy Successfully", Toast.LENGTH_SHORT).show();
                }
                break;

        }
    }


    @Override
    public void successResponse(Constants.API_TYPE type, Object response, int code) {
        binding.llProgressBar.setVisibility(View.GONE);
        switch (type) {
            case DAIRY_DELETE:
                try {
                    if (code == Constants.RESULT_OK) {
                        CommonResponse removeMilkBuyResponse = ((Response<CommonResponse>) response).body();
                        if (removeMilkBuyResponse != null && removeMilkBuyResponse.getStatus()) {
                            PreferenceManager.clearData();
                            Intent loginScreen = new Intent(SettingActivity.this, SignInActivity.class);
                            startActivity(loginScreen);
                            finishAffinity();
                        }
                    } else {
                        Utils.showSnackbar(this, binding.llMainLayout, "API Error");
                    }
                } catch (Exception e) {
                    Debug.e(TAG, "REMOVE_DAIRY" + e.getMessage());
                }
                break;
        }


    }

    @Override
    public void failureResponse(Constants.API_TYPE type, String error) {
        binding.llProgressBar.setVisibility(View.GONE);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }
}