package com.example.dairy.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.GridLayoutManager;

import com.example.dairy.R;
import com.example.dairy.adapter.CategoriesAdapter;
import com.example.dairy.adapter.ImageAdapter;
import com.example.dairy.databinding.ActivityMainBinding;
import com.example.dairy.modal.CommonResponse;
import com.example.dairy.utils.AlertMassage;
import com.example.dairy.utils.CategoriesInterface;
import com.example.dairy.utils.Constants;
import com.example.dairy.utils.Debug;
import com.example.dairy.utils.PreferenceManager;
import com.example.dairy.utils.Utils;
import com.example.dairy.webService.APIController;
import com.example.dairy.webService.ResponseInterface;
import com.google.gson.JsonObject;

import java.util.Locale;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Response;

public class MainActivity extends MyAppBaseActivity implements CategoriesInterface, ResponseInterface {

    ActivityMainBinding binding;
    private CategoriesAdapter categoriesAdapter;
    private ImageAdapter imageAdapter;
    private static final String TAG = "Main";
    private String language = "en";
    private APIController apiController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        getSupportActionBar().setTitle(R.string.app_name);

        GridLayoutManager manager = new GridLayoutManager(this, 3, GridLayoutManager.VERTICAL, false);
        binding.optionView.setLayoutManager(manager);
        categoriesAdapter = new CategoriesAdapter(this, this);
        binding.optionView.setAdapter(categoriesAdapter);
        categoriesAdapter.addData(Utils.getCategoriesList(this));


        imageAdapter = new ImageAdapter(this, true);
        binding.viewPager.setAdapter(imageAdapter);
        imageAdapter.addData(Utils.getSliderImagesList(this));

        language = PreferenceManager.getValue(Constants.SELECTED_LANGUAGE);
        getMasterApiCall();

        Debug.e(TAG, "Language :---->" + PreferenceManager.getValue(Constants.SELECTED_LANGUAGE));
        Debug.e(TAG, "Language :---->" + Locale.getDefault().getLanguage().toUpperCase());
//        String newLanguage = PreferenceManager.getValue(Constants.SELECTED_LANGUAGE);
        if (Utils.isEmpty(PreferenceManager.getValue(Constants.SELECTED_LANGUAGE))) {
            PreferenceManager.putValue(Constants.SELECTED_LANGUAGE, Locale.getDefault().getLanguage().toUpperCase());
        }
    }

    @Override
    protected void onResume() {
        super.onResume();


    }

    @Override
    public void categoriesClick(Constants.CATEGORIES_CLICK_TYPE type) {
        Intent openScreen = null;

        switch (type) {
            case MILK_BUY:

//                new AlertDialog.Builder(this)
//                        .setIcon(R.drawable.ic_error)
//                        .setTitle("Alert")
//                        .setMessage("You are account trial validity is over if you continue use this app please contact with app support")
//                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialog, int which) {
//                            }
//                        })
//                        .setNegativeButton("No", null)
//                        .show();
                openScreen = new Intent(this, MilkBuyActivity.class);
                break;

            case MILK_SELL:
                openScreen = new Intent(this, MilkSellActivity.class);
                break;

            case MASTER_ENTRY:
                openScreen = new Intent(this, MasterActivity.class);
                break;

            case CHART:
                openScreen = new Intent(this, RateMainActivity.class);
                break;

            case REPORT:
                openScreen = new Intent(this, ReportActivity.class);
                break;

            case ADVANCE_PAYMENT:
                openScreen = new Intent(this, AdvancePayment.class);
                break;

            case SETTING:
                openScreen = new Intent(this, SettingActivity.class);
                break;

            case CONTACT_MAIL:
                final Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);

                /* Fill it with Data */
                emailIntent.setType("message/rfc822");
                emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{Constants.CONTACT_US_MAIL});
                emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Subject");
                emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, "Text");

                /* Send it off to the Activity-Chooser */
                startActivity(Intent.createChooser(emailIntent, "Send mail..."));
                break;

            case LOGOUT:

                new AlertDialog.Builder(this)
                        .setIcon(R.drawable.ic_error)
                        .setTitle("Logout")
                        .setMessage("Are you sure you want to logout?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
//                                PreferenceManager.clearData();
                                Utils.logoutValueRemove();
                                Intent loginScreen = new Intent(MainActivity.this, SignInActivity.class);
                                startActivity(loginScreen);
                                finishAffinity();
                            }

                        })
                        .setNegativeButton("No", null)
                        .show();
                break;

        }
        if (openScreen != null) {
            startActivity(openScreen);
        }


    }


    private void getMasterApiCall() {

        apiController = new APIController(this);

        if (!Utils.isInternetConnected(this)) {
            AlertMassage.MassageAlert(this, getResources().getString(R.string.message_intent_conection));
            return;
        }

        JsonObject jsonObject = new JsonObject();
        try {
            jsonObject.addProperty("memberType", "");
            jsonObject.addProperty("startMemberCode", (Number) null);
            jsonObject.addProperty("endMemberCode", (Number) null);

            binding.llProgressBar.setVisibility(View.VISIBLE);
            Utils.hideSoftKeyword(this);
            RequestBody body = RequestBody.create(MediaType.parse("application/json"), jsonObject.toString());

            Debug.e(TAG, "Request " + jsonObject.toString());
            apiController.getMasterAPI(Constants.API_TYPE.GET_MASTER, body);

        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    @Override
    public void successResponse(Constants.API_TYPE type, Object response, int code) {
        binding.llProgressBar.setVisibility(View.GONE);
        switch (type) {
            case GET_MASTER:
                try {
                    if (code == Constants.RESULT_OK) {
                        CommonResponse getMasterResponse = ((Response<CommonResponse>) response).body();
                        if (getMasterResponse != null && getMasterResponse.getStatus()) {
                            Utils.setMemberList(getMasterResponse.getResult());
                        } else {
                            Utils.showSnackbar(this, binding.llMainLayout, getMasterResponse.getMessage());
                        }

                    } else {
                        Utils.showSnackbar(this, binding.llMainLayout, "API Error");

                    }
                } catch (Exception e) {
                    Debug.e(TAG, "Get Master List Exception" + e.getMessage());
                }
                break;

            case DELETE_MASTER:
                try {
                    if (code == Constants.RESULT_OK) {
                        CommonResponse deleteMasterResponse = ((Response<CommonResponse>) response).body();
                        if (deleteMasterResponse != null && deleteMasterResponse.getSuccess()) {

                        } else {
                            Utils.showSnackbar(this, binding.llMainLayout, deleteMasterResponse.getMessage());
                        }

                    } else {
                        Utils.showSnackbar(this, binding.llMainLayout, "API Error");

                    }
                } catch (Exception e) {
                    Debug.e(TAG, "Delete Master Exception" + e.getMessage());
                }
                break;
        }

    }

    @Override
    public void failureResponse(Constants.API_TYPE type, String error) {
        binding.llProgressBar.setVisibility(View.GONE);
        Debug.e(TAG, "Fail Error =----->" + error);
    }
}