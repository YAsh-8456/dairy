package com.example.dairy.activity;

import android.content.Context;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.example.dairy.utils.ContextUtils;

public abstract class MyAppBaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ContextUtils.updateLocale(newBase, ContextUtils.setLanguage()));
    }
}
