package com.example.dairy.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import androidx.databinding.DataBindingUtil;

import com.example.dairy.R;
import com.example.dairy.databinding.ActivityReportBinding;
import com.example.dairy.utils.Constants;
import com.example.dairy.webService.ResponseInterface;

public class ReportActivity extends MyAppBaseActivity implements View.OnClickListener, ResponseInterface {

    ActivityReportBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_report);
        getSupportActionBar().setTitle(R.string.categories_milk_report);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        init();
    }

    private void init() {

        binding.llCustomerReport.setOnClickListener(this);
        binding.llMilkBuyReport.setOnClickListener(this);
        binding.llMilkSellReport.setOnClickListener(this);
        binding.llPaymentReport.setOnClickListener(this);
        binding.llMilkLedgerReport.setOnClickListener(this);

    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.llMilkSellReport:
                Intent openMilkSellReport = new Intent(this, ReportGetLocalSaleActivity.class);
                openMilkSellReport.putExtra("title", getResources().getString(R.string.milk_sale_report));
                startActivity(openMilkSellReport);
                break;

            case R.id.llMilkBuyReport:
                Intent openMilkBuyReport = new Intent(this, ReportGetPurchaseActivity.class);
                openMilkBuyReport.putExtra("title", getResources().getString(R.string.milk_buy));
                startActivity(openMilkBuyReport);
                break;

            case R.id.llMilkLedgerReport:
                Intent openMilkLedgerReport = new Intent(this, ReportGetLedgerActivity.class);
                openMilkLedgerReport.putExtra("title", getResources().getString(R.string.ledger_report));
                startActivity(openMilkLedgerReport);
                break;

            case R.id.llCustomerReport:
                Intent openCustomerReport = new Intent(this, ReportGetMemberActivity.class);
                openCustomerReport.putExtra("title", getResources().getString(R.string.customer_report));
                startActivity(openCustomerReport);
                break;

            case R.id.llPaymentReport:
                Intent openPaymentReport = new Intent(this, ReportGetPaymentActivity.class);
                openPaymentReport.putExtra("title", getResources().getString(R.string.payment_report));
                startActivity(openPaymentReport);
                break;
        }
    }



    @Override
    public void successResponse(Constants.API_TYPE type, Object response, int code) {

    }

    @Override
    public void failureResponse(Constants.API_TYPE type, String error) {

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }
}