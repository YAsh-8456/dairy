package com.example.dairy.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.example.dairy.R;
import com.example.dairy.adapter.MasterAdapter;
import com.example.dairy.databinding.ActivityGetMasterBinding;
import com.example.dairy.modal.CommonResponse;
import com.example.dairy.modal.Result;
import com.example.dairy.utils.AlertMassage;
import com.example.dairy.utils.Constants;
import com.example.dairy.utils.Debug;
import com.example.dairy.utils.MemberClickInterface;
import com.example.dairy.utils.Utils;
import com.example.dairy.webService.APIController;
import com.example.dairy.webService.ResponseInterface;
import com.google.gson.JsonObject;

import java.io.Serializable;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Response;

public class MasterActivity extends MyAppBaseActivity implements ResponseInterface, View.OnClickListener, MemberClickInterface {

    ActivityGetMasterBinding binding;
    MasterAdapter masterAdapter;
    private APIController apiController;
    private String TAG = "GetMaster";
    private int EDIT_MASTER = 123;
    private int ADD_MASTER = 124;
    private int REMOVE_MASTER = 125;
    private int editPosition = -1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_get_master);
        getSupportActionBar().setTitle(R.string.master_list);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        apiController = new APIController(this);
        init();
    }

    private void init() {

//        binding.llSelectDate.setOnClickListener(this);
//        binding.llSelectPet.setOnClickListener(this);
//        binding.llSelectTime.setOnClickListener(this);
//
//        updateDate();
//        updateTimeSlot(isMorning);
//        updatePet(isBuffalo);
//
//        //
//        date = new DatePickerDialog.OnDateSetListener() {
//            @Override
//            public void onDateSet(DatePicker view, int year, int monthOfYear,
//                                  int dayOfMonth) {
//                // TODO Auto-generated method stub
//                myCalendar.set(Calendar.YEAR, year);
//                myCalendar.set(Calendar.MONTH, monthOfYear);
//                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
//                updateDate();
//            }
//
//        };

        LinearLayoutManager manager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        binding.masterView.setLayoutManager(manager);
        masterAdapter = new MasterAdapter(this, this);
        binding.masterView.setAdapter(masterAdapter);

        getMasterApiCall();
        noDataFoundManage();
    }

    private void noDataFoundManage() {
        if (masterAdapter.masterList.size() > 0) {
            binding.masterView.setVisibility(View.VISIBLE);
            binding.llNoDataFound.setVisibility(View.GONE);
        } else {
            binding.masterView.setVisibility(View.GONE);
            binding.llNoDataFound.setVisibility(View.VISIBLE);
        }
    }


    private void deleteMasterApiCall(String userId) {
        try {
            if (!Utils.isInternetConnected(this)) {
                AlertMassage.MassageAlert(this, getResources().getString(R.string.message_intent_conection));
                return;
            }
            binding.llProgressBar.setVisibility(View.VISIBLE);
            Utils.hideSoftKeyword(this);
            apiController.deleteMaster(Constants.API_TYPE.DELETE_MASTER, userId);

        } catch (Exception e) {
            Debug.e(TAG, "Error" + e.getMessage());
        }

    }

    private void getMasterApiCall() {


        if (!Utils.isInternetConnected(this)) {
            AlertMassage.MassageAlert(this, getResources().getString(R.string.message_intent_conection));
            return;
        }

        JsonObject jsonObject = new JsonObject();
        try {
            jsonObject.addProperty("memberType", "");
            jsonObject.addProperty("startMemberCode", (Number) null);
            jsonObject.addProperty("endMemberCode", (Number) null);


            binding.llProgressBar.setVisibility(View.VISIBLE);
            Utils.hideSoftKeyword(this);
            RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), jsonObject.toString());

            apiController.getMasterAPI(Constants.API_TYPE.GET_MASTER, body);

        } catch (Exception e) {
            e.printStackTrace();
        }



    }


    @Override
    public void successResponse(Constants.API_TYPE type, Object response, int code) {
        binding.llProgressBar.setVisibility(View.GONE);
        switch (type) {
            case GET_MASTER:
                try {
                    if (code == Constants.RESULT_OK) {
                        CommonResponse getMasterResponse = ((Response<CommonResponse>) response).body();
                        if (getMasterResponse != null && getMasterResponse.getStatus()) {
                            masterAdapter.addData(getMasterResponse.getResult());
                            Utils.setMemberList(getMasterResponse.getResult());
                            noDataFoundManage();

                        } else {
                            Utils.showSnackbar(this, binding.llMainLayout, getMasterResponse.getMessage());
                        }

                    } else {
                        Utils.showSnackbar(this, binding.llMainLayout, "API Error");

                    }
                } catch (Exception e) {
                    Debug.e(TAG, "Get Master List Exception" + e.getMessage());
                }
                break;

            case DELETE_MASTER:
                try {
                    if (code == Constants.RESULT_OK) {
                        CommonResponse deleteMasterResponse = ((Response<CommonResponse>) response).body();
                        if (deleteMasterResponse != null && deleteMasterResponse.getStatus()) {
                            noDataFoundManage();
                        } else {
                            Utils.showSnackbar(this, binding.llMainLayout, deleteMasterResponse.getMessage());
                        }

                    } else {
                        Utils.showSnackbar(this, binding.llMainLayout, "API Error");

                    }
                } catch (Exception e) {
                    Debug.e(TAG, "Delete Master Exception" + e.getMessage());
                }
                break;
        }

    }

    @Override
    public void failureResponse(Constants.API_TYPE type, String error) {
        binding.llProgressBar.setVisibility(View.GONE);
        Debug.e(TAG, "Fail Error =----->" + error);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == EDIT_MASTER && resultCode == Activity.RESULT_OK) {
            Result customer = (Result) data.getSerializableExtra("customer");
            if(editPosition >= 0){
                masterAdapter.editSingleRecord(editPosition,customer);
                editPosition = -1;

                Utils.setMemberList(masterAdapter.getMemberList());

            }



        } else if (requestCode == ADD_MASTER && resultCode == Activity.RESULT_OK) {
            Toast.makeText(this, "Add User Successfully", Toast.LENGTH_SHORT).show();
            Result customer = (Result) data.getSerializableExtra("customer");
            masterAdapter.addSingleRecord(customer);
            noDataFoundManage();
            Utils.setMemberList(masterAdapter.getMemberList());

        } else if (requestCode == REMOVE_MASTER && resultCode == Activity.RESULT_OK) {

            Utils.setMemberList(masterAdapter.getMemberList());
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.add, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.action_add:
                Intent openAddMaster = new Intent(this, MasterEntryActivity.class);
                startActivityForResult(openAddMaster, ADD_MASTER);
                return true;

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {

        }

    }


    @Override
    public void delete(Result result, int position) {
        deleteMasterApiCall(String.valueOf(result.getId()));
    }

    @Override
    public void view(Result result, int position) {

        Intent openMemberDetails = new Intent(this, MasterDetailsActivity.class);
        openMemberDetails.putExtra("memberDetails", (Serializable) result);
        startActivity(openMemberDetails);


    }

    @Override
    public void edit(Result result, int position) {
        editPosition = position;
        Intent openMemberEdit = new Intent(this, MasterEntryActivity.class);
        openMemberEdit.putExtra("isEditable", true);
        openMemberEdit.putExtra("memberDetails", (Serializable) result);
        startActivityForResult(openMemberEdit, EDIT_MASTER);
    }

    @Override
    public void print(Result result, int position) {
    }

    @Override
    protected void onResume() {
        super.onResume();
//        editPosition = -1;
    }
}