package com.example.dairy.activity;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.Toast;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.example.dairy.R;
import com.example.dairy.adapter.MilkSellerAdapter;
import com.example.dairy.databinding.ActivityGetLocalReportBinding;
import com.example.dairy.modal.CommonResponse;
import com.example.dairy.modal.Result;
import com.example.dairy.utils.AlertMassage;
import com.example.dairy.utils.Constants;
import com.example.dairy.utils.Debug;
import com.example.dairy.utils.MemberClickInterface;
import com.example.dairy.utils.PDFOperation;
import com.example.dairy.utils.ThermalOperation;
import com.example.dairy.utils.Utils;
import com.example.dairy.webService.APIController;
import com.example.dairy.webService.ResponseInterface;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Response;

public class ReportGetLocalSaleActivity extends MyAppBaseActivity implements View.OnClickListener, ResponseInterface, MemberClickInterface {

    ActivityGetLocalReportBinding binding;
    final Calendar myCalendar = Calendar.getInstance();
    DatePickerDialog.OnDateSetListener startDate;
    DatePickerDialog.OnDateSetListener endDate;
    MilkSellerAdapter milkBuyAdapter;
    boolean isStartMorning = Utils.isMorning();
    boolean isEndMorning = Utils.isMorning();
    private String selectedPet;
    boolean isBuffalo = true;
    private String TAG = "GetLocalSaleReport";
    private APIController apiController;
    private PDFOperation pdfOperation;
    private ThermalOperation thermalOperation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_get_local_report);
        String title = getIntent().getExtras().getString("title");
        getSupportActionBar().setTitle(title);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        init();
    }



    private void updateStartTimeSlot(boolean isMorning) {

        if (isMorning) {
            binding.ivStartTimer.setImageResource(R.drawable.sun);
            binding.tvSelectedStartTime.setText(R.string.morning);
        } else {
            binding.ivStartTimer.setImageResource(R.drawable.moon);
            binding.tvSelectedStartTime.setText(R.string.evening);
        }
    }

    private void updateEndTimeSlot(boolean isMorning) {

        if (isMorning) {
            binding.ivEndTimer.setImageResource(R.drawable.sun);
            binding.tvSelectedEndTime.setText(R.string.morning);
        } else {
            binding.ivEndTimer.setImageResource(R.drawable.moon);
            binding.tvSelectedEndTime.setText(R.string.evening);
        }
    }

    private void init() {
        apiController = new APIController(this);
        pdfOperation = new PDFOperation(this);
        thermalOperation = new ThermalOperation(this);

        binding.llSelectStartDate.setOnClickListener(this);
        binding.llSelectEndDate.setOnClickListener(this);
        binding.llSelectStartTime.setOnClickListener(this);
        binding.llSelectEndTime.setOnClickListener(this);
        binding.llCowSelected.setOnClickListener(this);
        binding.llBuffaloSelected.setOnClickListener(this);
        binding.llAllSelected.setOnClickListener(this);
        binding.includeReportBtn.llGetReport.setOnClickListener(this);

        updateDate(true);
        updateDate(false);
        updateStartTimeSlot(isStartMorning);
        updateEndTimeSlot(isEndMorning);
        selectedPet(Constants.COW);

        //
        startDate = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateDate(true);
            }

        };

        endDate = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateDate(false);
            }

        };


        LinearLayoutManager manager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        binding.buyRecordView.setLayoutManager(manager);
        milkBuyAdapter = new MilkSellerAdapter(this, this);
        binding.buyRecordView.setAdapter(milkBuyAdapter);
        milkBuyAdapter.manageViewClick(false);
        noDataFoundManage();
    }


    private void noDataFoundManage() {
//        if (milkBuyAdapter.list.size() > 0) {
//            binding.buyRecordView.setVisibility(View.VISIBLE);
//            binding.llNoDataFound.setVisibility(View.GONE);
//        } else {
//            binding.buyRecordView.setVisibility(View.GONE);
//            binding.llNoDataFound.setVisibility(View.VISIBLE);
//        }
    }

    private void updateDate(boolean isStartDate) {
        String myFormat = "dd-MM-yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        if (isStartDate) {
            binding.tvSelectedStartDate.setText(sdf.format(myCalendar.getTime()));

        } else {
            binding.tvSelectedEndDate.setText(sdf.format(myCalendar.getTime()));

        }
    }

    private void selectedPet(String petType) {
        selectedPet = petType;
        if (petType.equals(Constants.COW)) {
            binding.llCowSelected.setBackgroundColor(getResources().getColor(R.color.lightGreen));
            binding.llBuffaloSelected.setBackgroundColor(getResources().getColor(R.color.btn_color));
            binding.llAllSelected.setBackgroundColor(getResources().getColor(R.color.btn_color));


        } else if (petType.equals(Constants.BUFFALO)) {
            binding.llCowSelected.setBackgroundColor(getResources().getColor(R.color.btn_color));
            binding.llBuffaloSelected.setBackgroundColor(getResources().getColor(R.color.lightGreen));
            binding.llAllSelected.setBackgroundColor(getResources().getColor(R.color.btn_color));

        } else if (petType.equals(Constants.ALL)) {
            binding.llCowSelected.setBackgroundColor(getResources().getColor(R.color.btn_color));
            binding.llBuffaloSelected.setBackgroundColor(getResources().getColor(R.color.btn_color));
            binding.llAllSelected.setBackgroundColor(getResources().getColor(R.color.lightGreen));


        }
    }


    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.llSelectStartDate:
                new DatePickerDialog(this, startDate, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
                break;

            case R.id.llSelectEndDate:
                new DatePickerDialog(this, endDate, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
                break;

            case R.id.llSelectStartTime:
                isStartMorning = !isStartMorning;
                updateStartTimeSlot(isStartMorning);
                break;

            case R.id.llSelectEndTime:
                isEndMorning = !isEndMorning;
                updateEndTimeSlot(isEndMorning);
                break;

            case R.id.llCowSelected:
                selectedPet(Constants.COW);
                break;

            case R.id.llBuffaloSelected:
                selectedPet(Constants.BUFFALO);
                break;

            case R.id.llAllSelected:
                selectedPet(Constants.ALL);
                break;

            case R.id.llGetReport:
                Toast.makeText(this, "Click get report", Toast.LENGTH_SHORT).show();
                getReportAPICall();

                break;

        }
    }


    private void getReportAPICall() {

        String selectedStartDate = binding.tvSelectedStartDate.getText().toString().trim();
        String selectedEndDate = binding.tvSelectedEndDate.getText().toString().trim();
        String selectedStartTime = binding.tvSelectedStartTime.getText().toString().trim();
        String selectedEndTime = binding.tvSelectedEndTime.getText().toString().trim();
        String selectedAnimal = selectedPet.equals(Constants.ALL) ? "All" :selectedPet;

        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("startDate", Utils.finalDateFormat(selectedStartDate, isStartMorning));
            jsonObject.put("endDate", Utils.finalDateFormat(selectedEndDate, isEndMorning));
            jsonObject.put("timeslot", (Number) null);
//            jsonObject.put("startTime", selectedStartTime);
//            jsonObject.put("endTime", selectedEndTime);
            jsonObject.put("animalType", selectedAnimal);


            if (!Utils.isInternetConnected(this)) {
                AlertMassage.MassageAlert(this, getResources().getString(R.string.message_intent_conection));
                return;
            }

            binding.llProgressBar.setVisibility(View.VISIBLE);
            Utils.hideSoftKeyword(this);
            RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), jsonObject.toString());

            apiController.getMilkSell(Constants.API_TYPE.GET_LOCAL_SALE_REPORT, "", body);


        } catch (Exception e) {
            Debug.e(TAG, "Error" + e.getMessage());
        }

    }


//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        MenuInflater inflater = getMenuInflater();
//        inflater.inflate(R.menu.setting, menu);
//        return true;
//    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.report_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_save_pdf:
                if (milkBuyAdapter.list.size() > 0) {
                    pdfOperation.createPdfForLocalSellList(false, milkBuyAdapter.list);
                } else {
                    Toast.makeText(this, R.string.no_record_found, Toast.LENGTH_SHORT).show();
                }
                return true;

            case R.id.action_print_pdf:
                if (milkBuyAdapter.list.size() > 0) {
                    pdfOperation.createPdfForLocalSellList(true, milkBuyAdapter.list);
                } else {
                    Toast.makeText(this, R.string.no_record_found, Toast.LENGTH_SHORT).show();
                }
                return true;

            case R.id.action_thermal_print:
                if (milkBuyAdapter.list.size() > 0) {
                    thermalOperation.printSellList(milkBuyAdapter.list, isStartMorning ? Constants.MORNING : Constants.EVENING, selectedPet);

                }else {
                    Toast.makeText(this, R.string.no_record_found, Toast.LENGTH_SHORT).show();
                }
                return true;

            case android.R.id.home:
                onBackPressed();
                return true;


            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }


    @Override
    public void successResponse(Constants.API_TYPE type, Object response, int code) {
        binding.llProgressBar.setVisibility(View.GONE);
        switch (type) {
            case GET_LOCAL_SALE_REPORT:
                try {
                    if (code == Constants.RESULT_OK) {
                        CommonResponse addMilkBuyResponse = ((Response<CommonResponse>) response).body();
                        if (addMilkBuyResponse != null && addMilkBuyResponse.getStatus()) {
                            milkBuyAdapter.addData(addMilkBuyResponse.getResult());
                            noDataFoundManage();

                        } else {
                            Utils.showSnackbar(this, binding.llMainLayout, addMilkBuyResponse.getMessage());
                        }

                    } else {
                        Utils.showSnackbar(this, binding.llMainLayout, "API Error");

                    }
                } catch (Exception e) {
                    Debug.e(TAG, "Login Exception" + e.getMessage());
                }
                break;
        }
    }

    @Override
    public void failureResponse(Constants.API_TYPE type, String error) {
        binding.llProgressBar.setVisibility(View.GONE);
        Debug.e(TAG, "Fail Error =----->" + error);
    }

    @Override
    public void delete(Result result, int position) {

    }

    @Override
    public void view(Result result, int position) {

    }

    @Override
    public void edit(Result result, int position) {

    }

    @Override
    public void print(Result result, int position) {
    }
}