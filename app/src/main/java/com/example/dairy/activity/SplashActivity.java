package com.example.dairy.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import androidx.databinding.DataBindingUtil;

import com.example.dairy.R;
import com.example.dairy.databinding.ActivitySplashBinding;
import com.example.dairy.utils.Constants;
import com.example.dairy.utils.Debug;
import com.example.dairy.utils.PreferenceManager;
import com.example.dairy.utils.Utils;

public class SplashActivity extends MyAppBaseActivity {

    ActivitySplashBinding binding;
    String TAG = "Splash";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_splash);
        Debug.e(TAG, "IS login =--->" + Utils.isLogin());
        init();
    }


    private void init() {
//        setLocalSaleRate();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (Utils.isLogin()) {
                    Intent openLoginScreen = new Intent(SplashActivity.this, MainActivity.class);
                    startActivity(openLoginScreen);
                    finish();
                } else {
                    Intent openMainScreen = new Intent(SplashActivity.this, SignInActivity.class);
                    startActivity(openMainScreen);
                    finish();
                }
            }
        }, 1500);

    }

    private void setLocalSaleRate() {
        if (PreferenceManager.getValueInt(Constants.COW_LOCAL_SALE_RATE) > 0) {
            PreferenceManager.putValueInt(Constants.COW_LOCAL_SALE_RATE, Constants.COW_LOCAL_SALE_PRICE);
        }
        if (PreferenceManager.getValueInt(Constants.BUFFALO_LOCAL_SALE_RATE) > 0) {
            PreferenceManager.putValueInt(Constants.BUFFALO_LOCAL_SALE_RATE, Constants.BUFFALO_LOCAL_SALE_PRICE);
        }
    }
}