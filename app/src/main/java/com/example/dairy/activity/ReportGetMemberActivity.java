package com.example.dairy.activity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.example.dairy.R;
import com.example.dairy.adapter.MasterAdapter;
import com.example.dairy.databinding.ActivityGetMemberReportBinding;
import com.example.dairy.modal.CommonResponse;
import com.example.dairy.modal.Result;
import com.example.dairy.utils.AlertMassage;
import com.example.dairy.utils.Constants;
import com.example.dairy.utils.Debug;
import com.example.dairy.utils.MemberClickInterface;
import com.example.dairy.utils.PDFOperation;
import com.example.dairy.utils.ThermalOperation;
import com.example.dairy.utils.Utils;
import com.example.dairy.webService.APIController;
import com.example.dairy.webService.ResponseInterface;
import com.google.gson.JsonObject;

import java.io.Serializable;
import java.util.Calendar;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Response;

public class ReportGetMemberActivity extends MyAppBaseActivity implements View.OnClickListener, ResponseInterface, MemberClickInterface {

    ActivityGetMemberReportBinding binding;
    final Calendar myCalendar = Calendar.getInstance();
    DatePickerDialog.OnDateSetListener startDate;
    DatePickerDialog.OnDateSetListener endDate;
    MasterAdapter masterAdapter;
    boolean isMorning = Utils.isMorning();
    ;
    boolean isBuffalo = true;
    private String TAG = "MilkBuy";
    private APIController apiController;
    private String customerType = "buyer";
    private PDFOperation pdfOperation;
    private ThermalOperation thermalOperation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_get_member_report);
        String title = getIntent().getExtras().getString("title");
        getSupportActionBar().setTitle(title);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        init();
    }


    private void init() {
        binding.llBuyer.setOnClickListener(this);
        binding.llSeller.setOnClickListener(this);
        binding.llAll.setOnClickListener(this);
        binding.includeReportBtn.llGetReport.setOnClickListener(this);

        LinearLayoutManager manager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        binding.masterView.setLayoutManager(manager);
        masterAdapter = new MasterAdapter(this, this);
        binding.masterView.setAdapter(masterAdapter);
        masterAdapter.manageViewClick(false);

        apiController = new APIController(this);
        pdfOperation = new PDFOperation(this);
        thermalOperation = new ThermalOperation(this);
        selectedMemberType(Constants.ALL);

        noDataFoundManage();
    }

    private void noDataFoundManage() {
        if (masterAdapter.masterList.size() > 0) {
            binding.masterView.setVisibility(View.VISIBLE);
            binding.llNoDataFound.setVisibility(View.GONE);
        } else {
            binding.masterView.setVisibility(View.GONE);
            binding.llNoDataFound.setVisibility(View.VISIBLE);
        }
    }


    private void selectedMemberType(String memberType) {
        customerType = memberType;
        if (memberType.equals(Constants.BUYER)) {
            binding.llBuyer.setBackgroundColor(getResources().getColor(R.color.lightGreen));
            binding.llSeller.setBackgroundColor(getResources().getColor(R.color.btn_color));
            binding.llAll.setBackgroundColor(getResources().getColor(R.color.btn_color));


        } else if (memberType.equals(Constants.SELLER)) {
            binding.llBuyer.setBackgroundColor(getResources().getColor(R.color.btn_color));
            binding.llSeller.setBackgroundColor(getResources().getColor(R.color.lightGreen));
            binding.llAll.setBackgroundColor(getResources().getColor(R.color.btn_color));

        } else if (memberType.equals(Constants.ALL)) {
            binding.llBuyer.setBackgroundColor(getResources().getColor(R.color.btn_color));
            binding.llSeller.setBackgroundColor(getResources().getColor(R.color.btn_color));
            binding.llAll.setBackgroundColor(getResources().getColor(R.color.lightGreen));


        }
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.llBuyer:
                selectedMemberType(Constants.BUYER);
                break;

            case R.id.llSeller:
                selectedMemberType(Constants.SELLER);
                break;

            case R.id.llAll:
                selectedMemberType(Constants.ALL);
                break;

            case R.id.llGetReport:
                Toast.makeText(this, "Get Report", Toast.LENGTH_SHORT).show();
                getReportAPICall();
                break;
        }
    }

    private void getReportAPICall() {

        String memberStartID = binding.etMemberStartId.getText().toString().trim();
        String memberEndID = binding.etMemberEndId.getText().toString().trim();
        String customerTypeData = customerType.equals(Constants.ALL) ? "" : customerType;


        JsonObject jsonObject = new JsonObject();
        try {
            jsonObject.addProperty("memberType", customerTypeData);

            if (Utils.isNotEmpty(memberStartID)) {
                jsonObject.addProperty("startMemberCode", memberStartID);

            } else {
                jsonObject.addProperty("startMemberCode", (Number) null);
            }

            if (Utils.isNotEmpty(memberEndID)) {
                jsonObject.addProperty("endMemberCode", memberEndID);

            } else {
                jsonObject.addProperty("endMemberCode", (Number) null);
            }

            if (!Utils.isInternetConnected(this)) {
                AlertMassage.MassageAlert(this, getResources().getString(R.string.message_intent_conection));
                return;
            }

            binding.llProgressBar.setVisibility(View.VISIBLE);
            Utils.hideSoftKeyword(this);
            RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), jsonObject.toString());
            Debug.e(TAG, "Member list request:- " + jsonObject.toString());

//            apiController.getMemberList(Constants.API_TYPE.GET_MEMBER_REPORT, body);
            apiController.getMasterAPI(Constants.API_TYPE.GET_MEMBER_REPORT, body);


        } catch (Exception e) {
            Debug.e(TAG, "Error" + e.getMessage());
            binding.llProgressBar.setVisibility(View.GONE);
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.report_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_save_pdf:
                if (masterAdapter.masterList.size() > 0) {
                    pdfOperation.createPdfMemberList(false, masterAdapter.masterList);

                } else {
                    Toast.makeText(this, R.string.no_record_found, Toast.LENGTH_SHORT).show();
                }
                return true;

            case R.id.action_print_pdf:
                if (masterAdapter.masterList.size() > 0) {
                    pdfOperation.createPdfMemberList(true, masterAdapter.masterList);
                } else {
                    Toast.makeText(this, R.string.no_record_found, Toast.LENGTH_SHORT).show();
                }
                return true;

            case R.id.action_thermal_print:
                if (masterAdapter.masterList.size() > 0) {
                    thermalOperation.printProducerList(masterAdapter.masterList);
                } else {
                    Toast.makeText(this, R.string.no_record_found, Toast.LENGTH_SHORT).show();
                }
                return true;

            case android.R.id.home:
                onBackPressed();
                return true;


            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }

    @Override
    public void successResponse(Constants.API_TYPE type, Object response, int code) {
        binding.llProgressBar.setVisibility(View.GONE);
        switch (type) {
            case GET_MEMBER_REPORT:
                try {
                    if (code == Constants.RESULT_OK) {
                        CommonResponse addMilkBuyResponse = ((Response<CommonResponse>) response).body();
                        if (addMilkBuyResponse != null && addMilkBuyResponse.getStatus()) {
                            masterAdapter.addData(addMilkBuyResponse.getResult());
                            noDataFoundManage();
                        } else {
                            Utils.showSnackbar(this, binding.llMainLayout, addMilkBuyResponse.getMessage());
                        }

                    } else {
                        Utils.showSnackbar(this, binding.llMainLayout, "API Error");

                    }
                } catch (Exception e) {
                    Debug.e(TAG, "Login Exception" + e.getMessage());
                }
                break;
        }
    }

    @Override
    public void failureResponse(Constants.API_TYPE type, String error) {
        binding.llProgressBar.setVisibility(View.GONE);
        Debug.e(TAG, "Fail Error =----->" + error);
    }

    @Override
    public void delete(Result result, int position) {

    }

    @Override
    public void view(Result result, int position) {

        Intent openMemberDetails = new Intent(this, MasterDetailsActivity.class);
        openMemberDetails.putExtra("memberDetails", (Serializable) result);
        startActivity(openMemberDetails);
    }

    @Override
    public void edit(Result result, int position) {

    }

    @Override
    public void print(Result result, int position) {
    }
}