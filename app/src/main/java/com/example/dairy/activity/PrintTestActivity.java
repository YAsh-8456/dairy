package com.example.dairy.activity;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.GridLayoutManager;

import com.example.dairy.R;
import com.example.dairy.adapter.CategoriesAdapter;
import com.example.dairy.adapter.ImageAdapter;
import com.example.dairy.adapter.PrintTestAdapter;
import com.example.dairy.databinding.ActivityMainBinding;
import com.example.dairy.databinding.ActivityPrintTestBinding;
import com.example.dairy.modal.CommonResponse;
import com.example.dairy.modal.Result;
import com.example.dairy.utils.AlertMassage;
import com.example.dairy.utils.CategoriesInterface;
import com.example.dairy.utils.Constants;
import com.example.dairy.utils.Debug;
import com.example.dairy.utils.PDFOperation;
import com.example.dairy.utils.PreferenceManager;
import com.example.dairy.utils.PrintTextInterface;
import com.example.dairy.utils.ThermalOperation;
import com.example.dairy.utils.Utils;
import com.example.dairy.webService.APIController;
import com.example.dairy.webService.ResponseInterface;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Response;

public class PrintTestActivity extends MyAppBaseActivity implements PrintTextInterface {

    ActivityPrintTestBinding binding;
    private PrintTestAdapter printTestAdapter;
    private static final String TAG = "PrintTestActivity";
    private APIController apiController;
    private List<Constants.PRINT_TYPE> data = new ArrayList<Constants.PRINT_TYPE>();
    ThermalOperation thermalOperation;
    private PDFOperation pdfOperation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_print_test);
        getSupportActionBar().setTitle("Print Slip");

        thermalOperation = new ThermalOperation(this);
        pdfOperation = new PDFOperation(this);

        data.add(Constants.PRINT_TYPE.MILK_BUY);
        data.add(Constants.PRINT_TYPE.MILK_SELL);
        data.add(Constants.PRINT_TYPE.ADVANCE_PAYMENT);

        data.add(Constants.PRINT_TYPE.PURCHASE_LIST);
        data.add(Constants.PRINT_TYPE.SELL_LIST);
        data.add(Constants.PRINT_TYPE.MASTER_LIST);
        data.add(Constants.PRINT_TYPE.PRODUCER_LIST);
        data.add(Constants.PRINT_TYPE.LEDGER_LIST);
        data.add(Constants.PRINT_TYPE.ADVANCE_PAYMENT_LIST);

//      MEMBER_LIST_PDF,
//      PURCHASE_LIST_PDF,
//      LAGER_LIST_PDF,
//      LOCAL_SELL_LIST_PDF,
//      ADVANCE_PAYMENT_LIST_PDF,
        data.add(Constants.PRINT_TYPE.MEMBER_LIST_PDF);
        data.add(Constants.PRINT_TYPE.PURCHASE_LIST_PDF);
        data.add(Constants.PRINT_TYPE.LAGER_LIST_PDF);
        data.add(Constants.PRINT_TYPE.LOCAL_SELL_LIST_PDF);
        data.add(Constants.PRINT_TYPE.ADVANCE_PAYMENT_LIST_PDF);



        GridLayoutManager manager = new GridLayoutManager(this, 3, GridLayoutManager.VERTICAL, false);
        binding.optionView.setLayoutManager(manager);
        printTestAdapter = new PrintTestAdapter(this, this);
        binding.optionView.setAdapter(printTestAdapter);
        printTestAdapter.addData(data);

    }

    @Override
    protected void onResume() {
        super.onResume();


    }

    private Bitmap createBitmapFromView(Context context, RelativeLayout view) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        view.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT));

        view.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.buildDrawingCache();
        Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);

        return bitmap;
    }

    private Bitmap getBitmapFromView(View view) {
        //Define a bitmap with the same size as the view
        Bitmap returnedBitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(),Bitmap.Config.ARGB_8888);
        //Bind a canvas to it
        Canvas canvas = new Canvas(returnedBitmap);
        //Get the view's background
        Drawable bgDrawable =view.getBackground();
        if (bgDrawable!=null) {
            //has background drawable, then draw it on the canvas
            bgDrawable.draw(canvas);
        }   else{
            //does not have background drawable, then draw white background on the canvas
            canvas.drawColor(Color.WHITE);
        }
        // draw the view on the canvas
        view.draw(canvas);
        //return the bitmap
        return returnedBitmap;
    }


    @Override
    public void printTextClick(Constants.PRINT_TYPE type) {

        Result resultData = new Result();
        List<Result> resultList = new ArrayList<>();
        resultList.add(resultData);
        resultList.add(resultData);
        resultList.add(resultData);
        resultList.add(resultData);
        resultList.add(resultData);
        resultList.add(resultData);
        resultList.add(resultData);
        resultList.add(resultData);

        switch (type) {
            case MILK_BUY:

               Bitmap b = getBitmapFromView(binding.rlMain);
               binding.ivBitmap.setImageBitmap(b);
                thermalOperation.printBitmap(b);
                break;

            case MILK_SELL:
                thermalOperation.printSingleSell(resultData);
                break;

            case ADVANCE_PAYMENT:
                thermalOperation.printSingleAdvancePayment(resultData);
                break;

            case MASTER_LIST:

            case PRODUCER_LIST:
                thermalOperation.printProducerList(resultList);
                break;


            case LEDGER_LIST:
                thermalOperation.printLedgerList(resultList,"","");
                break;

            case PURCHASE_LIST:
                thermalOperation.printPurchaseList(resultList,"","");
                break;

            case ADVANCE_PAYMENT_LIST:
                thermalOperation.printAdvancePaymentList(resultList);
                break;

            case SELL_LIST:
                thermalOperation.printSellList(resultList, "", "");
                break;




            case MEMBER_LIST_PDF:
                pdfOperation.createPdfMemberList(true, resultList);
                break;

            case PURCHASE_LIST_PDF:
                pdfOperation.createPdfForPurchaseList(true, resultList);
                break;

            case LAGER_LIST_PDF:
                pdfOperation.createPdfForLagerList(true, resultList);
                break;

            case LOCAL_SELL_LIST_PDF:
                pdfOperation.createPdfForLocalSellList(true, resultList);
                break;

            case ADVANCE_PAYMENT_LIST_PDF:
                pdfOperation.createPdfForAdvancePaymentList(true, resultList);
                break;

        }
    }
}