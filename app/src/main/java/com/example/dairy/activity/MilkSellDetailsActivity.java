package com.example.dairy.activity;

import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;

import androidx.databinding.DataBindingUtil;

import com.example.dairy.R;
import com.example.dairy.databinding.ActivityMilkSellDetailsBindingImpl;
import com.example.dairy.modal.Result;

public class MilkSellDetailsActivity extends MyAppBaseActivity {

    private ActivityMilkSellDetailsBindingImpl binding;
    private Result result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this,R.layout.activity_milk_sell_details);
        getSupportActionBar().setTitle(R.string.sellerdetails);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        result = (Result) getIntent().getSerializableExtra("data") ;
        Log.d("data",result.toString());
        init();
    }

    private void init() {
        binding.tvcustomerId.setText(result.getCustomerId().toString());
        binding.tvcustomerName.setText(result.getCustomer().getCustomerName());
        binding.tvanimaltype.setText(result.getAnimalType());
        binding.tvamount.setText(result.getTotalAmount());
        binding.tvLiter.setText(result.getLiter());
        binding.tvRate.setText(result.getRate());
        binding.tvsaledate.setText(result.getSaleDate());

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;


            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }

}