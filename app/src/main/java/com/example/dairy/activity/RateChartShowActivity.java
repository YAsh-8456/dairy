package com.example.dairy.activity;

import android.os.Bundle;
import android.view.MenuItem;

import androidx.databinding.DataBindingUtil;
import androidx.viewpager.widget.ViewPager;

import com.example.dairy.R;
import com.example.dairy.adapter.ViewPagerAdapter;
import com.example.dairy.databinding.ActivityRateChartBinding;
import com.example.dairy.fragment.RateChartFragment;
import com.example.dairy.utils.Constants;
import com.example.dairy.utils.Debug;
import com.example.dairy.utils.PreferenceManager;
import com.example.dairy.utils.Utils;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class RateChartShowActivity extends MyAppBaseActivity {

    private static final String TAG = RateChartShowActivity.class.getSimpleName();
    ActivityRateChartBinding binding;
    ViewPager viewPager;
    ViewPagerAdapter viewPagerAdapter;
    TabLayout tabLayout;
    Boolean isBuffalo = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_rate_chart);
        getSupportActionBar().setTitle(R.string.categories_milk_rate_chart);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
        viewPager = (ViewPager) findViewById(R.id.viewPager);
        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());

        List<Map<Integer, Object>> readExcelNew = new ArrayList<>();
        if(getIntent() != null){
            isBuffalo = getIntent().getExtras().getBoolean("isBuffalo", false);
        }

        if (isBuffalo) {
            readExcelNew = Utils.convertStringToChartList(PreferenceManager.getValue(Constants.BUFFALO_CHART_DATA));
        } else {
            readExcelNew = Utils.convertStringToChartList(PreferenceManager.getValue(Constants.COW_CHART_DATA));
        }

        Debug.e(TAG, "readExcelNew: " + readExcelNew);
        if (readExcelNew != null && readExcelNew.size() > 0) {
            Map<Integer, Object> data = readExcelNew.get(0);
            List<Map<Integer, Object>> newList = readExcelNew;
            if (newList.size() > 0) {
                newList.remove(0);
            }
            for (Map.Entry<Integer, Object> entry : data.entrySet()) {
                int key = entry.getKey();
                String value = String.valueOf(entry.getValue());
                // remove header line create new list --------->


                if (Utils.isNotEmpty(value)) {
                    Debug.e(TAG, "If: " + key + newList);
                    viewPagerAdapter.addFrag(RateChartFragment.newInstance(key, newList), String.valueOf(value));
                } else {
                    Debug.e(TAG, "Else: " + newList);
                }
            }

        }
        viewPager.setAdapter(viewPagerAdapter);
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }
}