package com.example.dairy.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.databinding.DataBindingUtil;

import com.example.dairy.R;
import com.example.dairy.databinding.ActivityMasterEntryBinding;
import com.example.dairy.modal.CommonResponse;
import com.example.dairy.modal.Result;
import com.example.dairy.utils.AlertMassage;
import com.example.dairy.utils.Constants;
import com.example.dairy.utils.Debug;
import com.example.dairy.utils.PreferenceManager;
import com.example.dairy.utils.Utils;
import com.example.dairy.webService.APIController;
import com.example.dairy.webService.ResponseInterface;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Response;

public class MasterEntryActivity extends MyAppBaseActivity implements View.OnClickListener, ResponseInterface {

    ActivityMasterEntryBinding binding;
    private final static String TAG = "MasterEntry";
    private APIController apiController;
    private boolean isEditable = false;
    private int editableUserID = -1;
    private String customerType = "buyer";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_master_entry);
        getSupportActionBar().setTitle(R.string.categories_master);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        init();
    }

    @SuppressLint("SetTextI18n")
    private void init() {

        binding.tvSubmit.setOnClickListener(this);

        binding.llBuyer.setOnClickListener(this);
        binding.llSeller.setOnClickListener(this);
        binding.llAll.setOnClickListener(this);

        apiController = new APIController(this);
        selectedPet(Constants.ALL);
        binding.etUserId.setText("" + getNextUserNumber());
        checkIsEditable();
    }

    private int getNextUserNumber() {
        List<Result> userList = Utils.getMemberList();
        int largestUerId = 0;
        for (int i = 0; i < userList.size(); i++) {
            Result userData = userList.get(i);
            if (largestUerId < Integer.parseInt(userData.getMemberCode())) {
                largestUerId = Integer.parseInt(userData.getMemberCode());
            }
        }

        largestUerId += 1;
        return largestUerId;
    }

    private void selectedPet(String petType) {
        customerType = petType;
        if (petType.equals(Constants.BUYER)) {
            binding.llBuyer.setBackgroundColor(getResources().getColor(R.color.lightGreen));
            binding.llSeller.setBackgroundColor(getResources().getColor(R.color.btn_color));
            binding.llAll.setBackgroundColor(getResources().getColor(R.color.btn_color));
            binding.llSaleLayout.setVisibility(View.GONE);

        } else if (petType.equals(Constants.SELLER)) {
            binding.llBuyer.setBackgroundColor(getResources().getColor(R.color.btn_color));
            binding.llSeller.setBackgroundColor(getResources().getColor(R.color.lightGreen));
            binding.llAll.setBackgroundColor(getResources().getColor(R.color.btn_color));
            binding.llSaleLayout.setVisibility(View.VISIBLE);
        } else if (petType.equals(Constants.ALL)) {
            binding.llBuyer.setBackgroundColor(getResources().getColor(R.color.btn_color));
            binding.llSeller.setBackgroundColor(getResources().getColor(R.color.btn_color));
            binding.llAll.setBackgroundColor(getResources().getColor(R.color.lightGreen));
            binding.llSaleLayout.setVisibility(View.VISIBLE);

        }
    }


    private void checkIsEditable() {

        if (getIntent() != null) {
            isEditable = getIntent().getBooleanExtra("isEditable", false);
        }

        Result customerData = (Result) getIntent().getSerializableExtra("memberDetails");
        if (isEditable) {
            editableUserID = customerData.getId();
            binding.etUserId.setText(customerData.getMemberCode());
            binding.etAccountNumber.setText(customerData.getAccountNumber());
            binding.etBankName.setText(customerData.getBankName());
            binding.etFatherName.setText(customerData.getFatherName());
            binding.etCustomerAddress.setText(customerData.getAddress());
            binding.etIFSCCode.setText(customerData.getiFSCCode());
            binding.etMobileNumber.setText(customerData.getPhoneNumber());
            binding.etUser.setText(customerData.getCustomerName());
            binding.etUserHindi.setText(customerData.getCustomer_name_hindi());
            binding.etVillage.setText(customerData.getVillageName());

            binding.etBuffaloSellRate.setText(customerData.getLocalMilkSaleRateForBuffalo());
            binding.etCowSellRate.setText(customerData.getLocalMilkSaleRateForCow());

            selectedPet(customerData.getMemberType().toUpperCase());
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvSubmit:
                checkValidation();
                break;

            case R.id.llBuyer:
                selectedPet(Constants.BUYER);
                break;

            case R.id.llSeller:
                selectedPet(Constants.SELLER);
                break;

            case R.id.llAll:
                selectedPet(Constants.ALL);
                break;

        }
    }

    private void checkValidation() {

        String userName = binding.etUser.getText().toString().trim();
        String mobileNumber = binding.etMobileNumber.getText().toString().trim();
        String userNameHindi = binding.etUserHindi.getText().toString().trim();
        String fatherName = binding.etFatherName.getText().toString().trim();
        String customerAddress = binding.etCustomerAddress.getText().toString().trim();
        String village = binding.etVillage.getText().toString().trim();
        String accountNumber = binding.etAccountNumber.getText().toString().trim();
        String bankName = binding.etBankName.getText().toString().trim();
        String IFSCCode = binding.etIFSCCode.getText().toString().trim();
        String memberCode = binding.etUserId.getText().toString().trim();
        String cowMilkLocalRate = binding.etCowSellRate.getText().toString().trim();
        String buffaloMilkLocalRate = binding.etBuffaloSellRate.getText().toString().trim();

        if (Utils.isEmpty(memberCode)) {
            Utils.showSnackbar(this, binding.llMainLayout, getResources().getString(R.string.message_enter_memberCode));
            return;
        }


        if (Utils.isEmpty(userName)) {
            Utils.showSnackbar(this, binding.llMainLayout, getResources().getString(R.string.message_enter_username));
            return;
        }

        if (Utils.isEmpty(userNameHindi)) {
            Utils.showSnackbar(this, binding.llMainLayout, getResources().getString(R.string.message_enter_username));
            return;
        }

//        if (Utils.isEmpty(mobileNumber) || mobileNumber.length() < 10) {
        if (Utils.isEmpty(mobileNumber)) {
            Utils.showSnackbar(this, binding.llMainLayout, getResources().getString(R.string.message_enter_mobilenumber));
            return;
        }

        if (customerType.equals(Constants.SELLER) || customerType.equals(Constants.ALL)) {
            if (Utils.isEmpty(cowMilkLocalRate) || cowMilkLocalRate.equals("0")) {
                Utils.showSnackbar(this, binding.llMainLayout, getResources().getString(R.string.message_enter_cow_milk_local_sale_rate));
                return;
            }

            if (Utils.isEmpty(buffaloMilkLocalRate) || buffaloMilkLocalRate.equals("0")) {
                Utils.showSnackbar(this, binding.llMainLayout, getResources().getString(R.string.message_enter_buffalo_milk_local_sale_rate));
                return;
            }
        }

//        if (Utils.isEmpty(fatherName)) {
//            Utils.showSnackbar(this, binding.llMainLayout, getResources().getString(R.string.message_enter_dairyaddress));
//            return;
//        }
//
//        if (Utils.isEmpty(accountNumber)) {
//            Utils.showSnackbar(this, binding.llMainLayout, getResources().getString(R.string.message_enter_account_number));
//            return;
//        }
//
//        if (Utils.isEmpty(bankName)) {
//            Utils.showSnackbar(this, binding.llMainLayout, getResources().getString(R.string.message_enter_bank_name));
//            return;
//        }
//
//        if (Utils.isEmpty(IFSCCode)) {
//            Utils.showSnackbar(this, binding.llMainLayout, getResources().getString(R.string.message_enter_ifsc_code));
//            return;
//        }
//
//        if (Utils.isEmpty(village)) {
//            Utils.showSnackbar(this, binding.llMainLayout, getResources().getString(R.string.message_enter_village));
//            return;
//        }
//
//        if (Utils.isEmpty(customerAddress)) {
//            Utils.showSnackbar(this, binding.llMainLayout, getResources().getString(R.string.message_enter_dairyaddress));
//            return;
//        }

        addMasterApiCall();

    }


    private void addMasterApiCall() {

        String dairyId = String.valueOf(PreferenceManager.getValueInt(Constants.DAIRY_ID));

        try {
            String userName = binding.etUser.getText().toString().trim();
            String mobileNumber = binding.etMobileNumber.getText().toString().trim();
            String fatherName = binding.etFatherName.getText().toString().trim();
            String userNameHindi = binding.etUserHindi.getText().toString().trim();
            String customerAddress = binding.etCustomerAddress.getText().toString().trim();
            String village = binding.etVillage.getText().toString().trim();
            String accountNumber = binding.etAccountNumber.getText().toString().trim();
            String bankName = binding.etBankName.getText().toString().trim();
            String IFSCCode = binding.etIFSCCode.getText().toString().trim();
            String memberCode = binding.etUserId.getText().toString().trim();
            String cowMilkLocalRate = binding.etCowSellRate.getText().toString().trim();
            String buffaloMilkLocalRate = binding.etBuffaloSellRate.getText().toString().trim();

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("DairyId", dairyId);
            jsonObject.put("memberCode", memberCode);
            jsonObject.put("customerName", userName);
            jsonObject.put("customer_name_hindi", userNameHindi);
            jsonObject.put("phoneNumber", mobileNumber);
            jsonObject.put("fatherName", fatherName);
            jsonObject.put("memberType", customerType.equals(Constants.BUYER) ? "buyer" : customerType.equals(Constants.SELLER) ? "seller" : "all");
            jsonObject.put("accountNumber", accountNumber);
            jsonObject.put("IFSCCode", IFSCCode);
            jsonObject.put("bankName", bankName);
            jsonObject.put("villageName", village);
            jsonObject.put("address", customerAddress);
            jsonObject.put("local_milk_sale_rate_for_cow", Utils.isNotEmpty(cowMilkLocalRate) ? cowMilkLocalRate : "");
            jsonObject.put("local_milk_sale_rate_for_buffalo", Utils.isNotEmpty(buffaloMilkLocalRate) ? buffaloMilkLocalRate : "");


            if (!Utils.isInternetConnected(this)) {
                AlertMassage.MassageAlert(this, getResources().getString(R.string.message_intent_conection));
                return;
            }

            binding.llProgressBar.setVisibility(View.VISIBLE);
            Utils.hideSoftKeyword(this);
            RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), jsonObject.toString());

            if (!isEditable) {
                apiController.addMasterAPI(Constants.API_TYPE.ADD_MASTER, body);
            } else {
                String userID = String.valueOf(editableUserID);
                if (Utils.isEmpty(userID)) {
                    Toast.makeText(this, "UserID is Missing", Toast.LENGTH_SHORT).show();
                    return;
                }
                apiController.editMaster(Constants.API_TYPE.EDIT_MASTER, userID, body);
            }


        } catch (Exception e) {
            Debug.e(TAG, "Error" + e.getMessage());
            binding.llProgressBar.setVisibility(View.GONE);
        }

    }


    @Override
    public void successResponse(Constants.API_TYPE type, Object response, int code) {
        binding.llProgressBar.setVisibility(View.GONE);
        switch (type) {
            case ADD_MASTER:
                try {
                    if (code == Constants.RESULT_OK) {
                        CommonResponse addMasterResponse = ((Response<CommonResponse>) response).body();
                        if (addMasterResponse != null && addMasterResponse.getStatus()) {
                            Intent intent = getIntent();
                            intent.putExtra("customer", (Serializable) addMasterResponse.getResult().get(0));
                            setResult(RESULT_OK, intent);
                            finish();

                        } else {
                            Utils.showSnackbar(this, binding.llMainLayout, addMasterResponse.getMessage());
                        }

                    } else {
                        Utils.showSnackbar(this, binding.llMainLayout, "API Error");

                    }
                } catch (Exception e) {
                    Debug.e(TAG, "Login Exception" + e.getMessage());
                }
                break;

            case EDIT_MASTER:
                try {
                    if (code == Constants.RESULT_OK) {
                        CommonResponse editMasterResponse = ((Response<CommonResponse>) response).body();
                        if (editMasterResponse != null && editMasterResponse.getStatus()) {
//                            Utils.showSnackbar(this, binding.llMainLayout, editMasterResponse.getMessage());
                            Intent intent = getIntent();
                            intent.putExtra("customer", editMasterResponse.getResult().get(0));
                            setResult(RESULT_OK, intent);
                            finish();
                        } else {
                            Utils.showSnackbar(this, binding.llMainLayout, editMasterResponse.getMessage());
                        }

                    } else {
                        Utils.showSnackbar(this, binding.llMainLayout, "API Error");

                    }
                } catch (Exception e) {
                    Debug.e(TAG, "Login Exception" + e.getMessage());
                }
                break;
        }

    }

    @Override
    public void failureResponse(Constants.API_TYPE type, String error) {
        binding.llProgressBar.setVisibility(View.GONE);
        Debug.e(TAG, "Fail Error =----->" + error);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }

}