package com.example.dairy.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.databinding.DataBindingUtil;

import com.example.dairy.R;
import com.example.dairy.databinding.ActivitySignUpBinding;
import com.example.dairy.modal.CommonResponse;
import com.example.dairy.utils.AlertMassage;
import com.example.dairy.utils.Constants;
import com.example.dairy.utils.CreateChartInterface;
import com.example.dairy.utils.Debug;
import com.example.dairy.utils.PreferenceManager;
import com.example.dairy.utils.Utils;
import com.example.dairy.webService.APIController;
import com.example.dairy.webService.ResponseInterface;

import org.json.JSONObject;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Response;

public class SignUpActivity extends MyAppBaseActivity implements View.OnClickListener, ResponseInterface, CreateChartInterface {

    private final static String TAG = "SignUp";
    ActivitySignUpBinding binding;
    private APIController apiController;
    private boolean isEditable = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_sign_up);
        init();
    }

    private void init() {

        binding.tvLoginClick.setOnClickListener(this);
        binding.tvSignUpClick.setOnClickListener(this);

        apiController = new APIController(this);

        isEditable = getIntent().getBooleanExtra("isEditable", false);
        if (isEditable) {
            setDairyData();

            binding.tvLoginClick.setVisibility(View.GONE);
            binding.tvSignUpClick.setText(R.string.update);
        }
    }

    private void setDairyData() {

        binding.etUser.setText(PreferenceManager.getValue(Constants.USER_NAME));
        binding.etMobileNumber.setText(PreferenceManager.getValue(Constants.USER_MO_NUMBER));
        binding.etDairyName.setText(PreferenceManager.getValue(Constants.DAIRY_NAME));
        binding.etDairyAddress.setText(PreferenceManager.getValue(Constants.DAIRY_ADDRESS));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvLoginClick:
                onBackPressed();
                break;

            case R.id.tvSignUpClick:
                checkValidation();
                break;
        }
    }

    private void checkValidation() {

        String userName = binding.etUser.getText().toString().trim();
        String dairyName = binding.etDairyName.getText().toString().trim();
        String dairyAddress = binding.etDairyAddress.getText().toString().trim();
        String mobileNumber = binding.etMobileNumber.getText().toString().trim();
        String password = binding.etPassword.getText().toString().trim();

        if (Utils.isEmpty(userName)) {
            Utils.showSnackbar(this, binding.llMainLayout, getResources().getString(R.string.message_enter_username));
            return;
        }

        if (Utils.isEmpty(dairyName)) {
            Utils.showSnackbar(this, binding.llMainLayout, getResources().getString(R.string.message_enter_dairyname));
            return;
        }

        if (Utils.isEmpty(mobileNumber) || mobileNumber.length() < 10) {
            Utils.showSnackbar(this, binding.llMainLayout, getResources().getString(R.string.message_enter_mobilenumber));
            return;
        }

        if (Utils.isEmpty(dairyAddress)) {
            Utils.showSnackbar(this, binding.llMainLayout, getResources().getString(R.string.message_enter_dairyaddress));
            return;
        }

        if (Utils.isEmpty(password)) {
            Utils.showSnackbar(this, binding.llMainLayout, getResources().getString(R.string.message_enter_password));
            return;
        }

        if (isEditable) {
            callEditDairyApi();
        } else {
            callSignUpApi();

        }
    }

    private void callSignUpApi() {

        try {
            String userName = binding.etUser.getText().toString().trim();
            String dairyName = binding.etDairyName.getText().toString().trim();
            String dairyAddress = binding.etDairyAddress.getText().toString().trim();
            String mobileNumber = binding.etMobileNumber.getText().toString().trim();
            String password = binding.etPassword.getText().toString().trim();

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("userName", userName);
            jsonObject.put("password", password);
            jsonObject.put("centerName", dairyName);
            jsonObject.put("dairyName", dairyName);
            jsonObject.put("dairyAddress", dairyAddress);
            jsonObject.put("phoneNumber", mobileNumber);


            if (!Utils.isInternetConnected(SignUpActivity.this)) {
                AlertMassage.MassageAlert(this, getResources().getString(R.string.message_intent_conection));
                return;
            }

            binding.llProgressBar.setVisibility(View.VISIBLE);
            Utils.hideSoftKeyword(SignUpActivity.this);
            RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), jsonObject.toString());
            apiController.dairyRegister(Constants.API_TYPE.DAIRY_REGISTER, body);

        } catch (Exception e) {
            Debug.e(TAG, "Error" + e.getMessage());
        }

    }

    private void callEditDairyApi() {

        try {
            String userName = binding.etUser.getText().toString().trim();
            String dairyName = binding.etDairyName.getText().toString().trim();
            String dairyAddress = binding.etDairyAddress.getText().toString().trim();
            String mobileNumber = binding.etMobileNumber.getText().toString().trim();
            String password = binding.etPassword.getText().toString().trim();

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("userName", userName);
            jsonObject.put("password", password);
            jsonObject.put("centerName", dairyName);
            jsonObject.put("dairyName", dairyName);
            jsonObject.put("dairyAddress", dairyAddress);
            jsonObject.put("phoneNumber", mobileNumber);


            if (!Utils.isInternetConnected(SignUpActivity.this)) {
                AlertMassage.MassageAlert(this, getResources().getString(R.string.message_intent_conection));
                return;
            }

            binding.llProgressBar.setVisibility(View.VISIBLE);
            Utils.hideSoftKeyword(SignUpActivity.this);
            RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), jsonObject.toString());
            apiController.dairyEdit(Constants.API_TYPE.DAIRY_EDIT, body);

        } catch (Exception e) {
            Debug.e(TAG, "Error" + e.getMessage());
        }

    }


    @Override
    public void successResponse(Constants.API_TYPE type, Object response, int code) {
        binding.llProgressBar.setVisibility(View.GONE);
        switch (type) {
            case DAIRY_REGISTER:
                try {
                    if (code == Constants.RESULT_OK) {
                        CommonResponse registerResponse = ((Response<CommonResponse>) response).body();
                        if (registerResponse != null && registerResponse.getStatus()) {
                            Utils.saveDairyDetails(registerResponse.get_token(), registerResponse.getResult().get(0));
                            Utils.setDefaultData(this, this);
                        } else {
                            Utils.showSnackbar(this, binding.llMainLayout, registerResponse.getMessage());
                        }

                    } else {
                        String error = null;
                        try {
                            error = ((retrofit2.Response<String>) response).errorBody().string();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        CommonResponse errorResponse = Utils.convertStringToList(error);
                        Utils.showSnackbar(this, binding.llMainLayout, errorResponse.getSqlMessage());

                    }
                } catch (Exception e) {
                    Debug.e(TAG, "Sign up Exception" + e.getMessage());
                }
                break;

            case DAIRY_EDIT:
                try {
                    if (code == Constants.RESULT_OK) {
                        CommonResponse registerResponse = ((Response<CommonResponse>) response).body();
                        if (registerResponse != null && registerResponse.getStatus()) {
//                            Utils.saveDairyDetails(registerResponse.get_token(), registerResponse.getResult().get(0));
//                            Intent intent = getIntent();
//                            setResult(RESULT_OK, intent);
//                            finish();
                            PreferenceManager.clearData();
                            Intent loginScreen = new Intent(SignUpActivity.this, SignInActivity.class);
                            startActivity(loginScreen);
                            finishAffinity();
                        } else {
                            Utils.showSnackbar(this, binding.llMainLayout, registerResponse.getMessage());
                        }

                    } else {
                        String error = null;
                        try {
                            error = ((retrofit2.Response<String>) response).errorBody().string();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        CommonResponse errorResponse = Utils.convertStringToList(error);
                        Utils.showSnackbar(this, binding.llMainLayout, errorResponse.getSqlMessage());

                    }
                } catch (Exception e) {
                    Debug.e(TAG, "Sign up Exception" + e.getMessage());
                }
                break;
        }

    }

    @Override
    public void failureResponse(Constants.API_TYPE type, String error) {
        binding.llProgressBar.setVisibility(View.GONE);
        Debug.e(TAG, "Fail Error =----->" + error);
    }

    @Override
    public void createdChart(boolean isBuffalo, List<Map<Integer, Object>> data) {


        if (isBuffalo) {
            PreferenceManager.putValue(Constants.BUFFALO_CHART_DATA, Utils.convertChartListToString(data));
            Debug.e(TAG, "Get Buffalo data =--->: " + Utils.convertChartListToString(data));
            Utils.createChartData(this, this, Constants.cowStartFAT, Constants.cowEndFAT, Constants.cowStartSNF, Constants.cowEndSNF, Constants.fatRateData, Constants.snfRateData, Constants.commissionData, Constants.dedicationData, false);
        } else {
            PreferenceManager.putValue(Constants.COW_CHART_DATA, Utils.convertChartListToString(data));
            Debug.e(TAG, "Get Cow data =--->: " + Utils.convertChartListToString(data));
        }

        if (Utils.isNotEmpty(PreferenceManager.getValue(Constants.BUFFALO_CHART_DATA)) && Utils.isNotEmpty(PreferenceManager.getValue(Constants.COW_CHART_DATA))) {
            Debug.e(TAG, "Create Chart =--->" + isBuffalo);
            Intent openMainScreen = new Intent(SignUpActivity.this, MainActivity.class);
            startActivity(openMainScreen);
            finish();
        }
    }

    @Override
    public void createdCowChart(boolean isBuffalo, List<Map<Integer, Object>> data) {

    }

    @Override
    public void createdBuffaloChart(boolean isBuffalo, List<Map<Integer, Object>> data) {

    }
}