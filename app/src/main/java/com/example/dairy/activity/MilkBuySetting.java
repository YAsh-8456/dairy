package com.example.dairy.activity;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;

import androidx.databinding.DataBindingUtil;

import com.example.dairy.R;
import com.example.dairy.databinding.ActivitySubSettingBinding;
import com.example.dairy.utils.Constants;
import com.example.dairy.utils.Debug;
import com.example.dairy.utils.PreferenceManager;
import com.example.dairy.utils.Utils;

public class MilkBuySetting extends MyAppBaseActivity {

    ActivitySubSettingBinding binding;
    private final String TAG = "MilkBuySetting";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_sub_setting);
        getSupportActionBar().setTitle(R.string.milk_buy_setting);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        init();
    }

    private void init() {

        binding.tvCowFat.setText(String.valueOf(PreferenceManager.getFloatValue(Constants.COW_FAT_LIMIT)));

        binding.printEnable.setChecked(PreferenceManager.getValueBoolean(Constants.PRINT_ENABLE));
        binding.smsEnable.setChecked(PreferenceManager.getValueBoolean(Constants.SMS_ENABLE));
        binding.calculateByFAT.setChecked(PreferenceManager.getValueBoolean(Constants.CALCULATE_BY_FAT));
        manageVisibilityCowLimit();

        binding.printEnable.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                PreferenceManager.putValueBoolean(Constants.PRINT_ENABLE, isChecked);

            }
        });

        binding.smsEnable.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                PreferenceManager.putValueBoolean(Constants.SMS_ENABLE, isChecked);

            }
        });


        binding.calculateByFAT.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                PreferenceManager.putValueBoolean(Constants.CALCULATE_BY_FAT, isChecked);
                manageVisibilityCowLimit();

            }
        });

        binding.tvCowFat.addTextChangedListener(setValueInCowFAT);
    }

    private void manageVisibilityCowLimit() {
        if(binding.calculateByFAT.isChecked()){
            binding.llLimitSetLayout.setVisibility(View.VISIBLE);
        }else{
            binding.llLimitSetLayout.setVisibility(View.GONE);
        }
    }

    TextWatcher setValueInCowFAT = new TextWatcher() {
        @Override
        public void afterTextChanged(Editable s) {

            if(s.toString().length() > 0){

                String tempValue = s.toString();
                if (Utils.isStringInteger(tempValue)) {
                    Debug.e(TAG, "FAT Value after convert float =--->" + String.valueOf((float) Integer.parseInt(tempValue)));
                    PreferenceManager.putFloatValue(Constants.COW_FAT_LIMIT, (float) Integer.parseInt(tempValue));
                }else {
                    PreferenceManager.putFloatValue(Constants.COW_FAT_LIMIT, Float.valueOf(tempValue));
                }

            }else {
                binding.tvCowFat.setText("5.0");
                PreferenceManager.putFloatValue(Constants.COW_FAT_LIMIT, 5.0f);
            }
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }


    };

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }

}